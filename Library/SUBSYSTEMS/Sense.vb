﻿''' <summary>Defines a SCPI Sense Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class SenseSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="controller">Reference to an open <see cref="Scpi.IDevice">SCPI instrument</see>.</param>
    Public Sub New(ByVal controller As Scpi.IDevice)

        ' instantiate the base class
        MyBase.New("SENS", controller)

        ' create a new function collection
        Me._functionCollection = New ResettableKeyableCollection(Of ScpiFunction)

        Me._averageCount = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._averageCount)
        Me._averageState = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._averageState)

        Me._deltaAperture = New ResettableDouble
        MyBase.ResettableValues.Add(Me._deltaAperture)
        Me._deltaVoltageAutoRange = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._deltaVoltageAutoRange)
        Me._deltaVoltageRange = New ResettableDouble
        MyBase.ResettableValues.Add(Me._deltaVoltageRange)
        Me._functionMode = New ResettableValue(Of SenseFunctionModes)
        MyBase.ResettableValues.Add(Me._functionMode)
        Me._functionsDisabled = New ResettableValue(Of SenseFunctionModes)
        MyBase.ResettableValues.Add(Me._functionsDisabled)
        Me._functionsEnabled = New ResettableValue(Of SenseFunctionModes)
        MyBase.ResettableValues.Add(Me._functionsEnabled)
        Me._isFunctionConcurrent = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._isFunctionConcurrent)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then


                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        MyBase.ClearExecutionState()
        Return Me._functionCollection.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        MyBase.PresetKnownState()
        Return Me._functionCollection.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        MyBase.ResetKnownState()
        Return Me._functionCollection.ResetKnownState()
    End Function

#End Region

#Region " PARSERS AND BUILDERS "

    ''' <summary>
    ''' Returns the string function corresponding to the enumerated value.
    ''' Unlike SelectFunctionMode, this method can return a list of functions.
    ''' </summary>
    ''' <param name="value">specifies the function.</param>
    Public Shared Function BuildFunctionModesRecord(ByVal value As Nullable(Of Scpi.SenseFunctionModes)) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return BuildFunctionModesRecord(value.Value)
        Else
            Return BuildFunctionModesRecord(SenseFunctionModes.None)
        End If
    End Function

    ''' <summary>
    ''' Returns the string function corresponding to the enumerated value.
    ''' Unlike SelectFunctionMode, this method can return a list of functions.
    ''' </summary>
    ''' <param name="value">specifies the function.</param>
    Public Shared Function BuildFunctionModesRecord(ByVal value As Scpi.SenseFunctionModes) As String
        If value = SenseFunctionModes.None Then
            Return String.Empty
        Else
            Dim builder As New System.Text.StringBuilder
            For Each element As SenseFunctionModes In [Enum].GetValues(GetType(SenseFunctionModes))
                If (value And element) <> 0 Then
                    Syntax.AddWord(builder, Syntax.ExtractScpi(element))
                End If
            Next
            Return builder.ToString
        End If
    End Function

#End Region

#Region " DATA "

    ''' <summary>Returns the latest post-processed reading stored in the sample buffer.</summary>
    Public Function FetchLatestData() As String

        Me._lastFetch = MyBase.Controller.QueryTrimEnd(":SENSE:DATA:LAT?")
        Return Me._lastFetch

    End Function

    ''' <summary>Returns the latest post-processed reading stored in the sample buffer.</summary>
    Public Function FetchData() As String

        Me._lastFetch = MyBase.Controller.QueryTrimEnd(":FETC?")
        Return Me._lastFetch

    End Function

    Private _lastFetch As String
    ''' <summary>Gets or sets the last fetch data string.</summary>
    Public ReadOnly Property LastFetch() As String
        Get
            Return Me._lastFetch
        End Get
    End Property

    ''' <summary>Returns the most recent reading.</summary>
    Public ReadOnly Property LatestData() As String
        Get
            Me._lastFetch = MyBase.Controller.QueryTrimEnd(Syntax.BuildQuery(Me.Name, "DATA"))
            ' Me._lastFetch = MyBase.Controller.QueryTrimEnd(":DATA:LAT?") ' this does not work with the 2700.
            Return Me._lastFetch
        End Get
    End Property

    ''' <summary>Take a measurement using the Read query.</summary>
    Public Function Read() As String
        Me._lastFetch = MyBase.Controller.QueryTrimEnd(":READ?")
        Return Me._lastFetch
    End Function

#End Region

#Region " AVERAGE "

    Private _averageCount As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the average count.
    ''' </summary>
    ''' <value>The average count.</value>
    Public ReadOnly Property AverageCount() As ResettableValue(Of Integer)
        Get
            Return Me._averageCount
        End Get
    End Property

    ''' <summary>Gets or sets the average count.  This is the total number of 
    ''' elements used in the filter.
    ''' </summary>
    Public Property AverageCount(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(Syntax.AverageModalityName, Syntax.CountCommand, Me._averageCount, access)
            Return Me._averageCount.Value.Value
        End Get
        Set(ByVal value As Integer)
            Me._averageCount = MyBase.Setter(Syntax.AverageModalityName, Syntax.CountCommand, Me._averageCount, value, access)
        End Set
    End Property

    Private _averageState As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the average state.
    ''' </summary>
    ''' <value>The average state.</value>
    Public ReadOnly Property AverageState() As ResettableValue(Of Boolean)
        Get
            Return Me._averageState
        End Get
    End Property

    ''' <summary>Gets or sets the condition for averaging.</summary>
    Public Property AverageState(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.AverageModalityName, Syntax.StateCommand, Me._averageState, access)
            Return Me._averageState.Value.Value
        End Get
        Set(ByVal value As Boolean)
            Me._averageState = MyBase.Setter(Syntax.AverageModalityName, Syntax.StateCommand, Me._averageState, value, BooleanDataFormat.OnOff, access)
        End Set
    End Property

#End Region

#Region " DELTA "

    Private Const serialCommTimeoutInterval As Integer = 200
    Private Const serialCommPollDelay As Integer = 10

    ''' <summary>
    ''' Returns a double or no value.
    ''' </summary>
    Private Shared Function parseDouble(ByVal value As String) As Nullable(Of Double)

        Dim numericValue As Double
        If String.IsNullOrWhiteSpace(value) Then
            Return New Nullable(Of Double)
        ElseIf Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture, numericValue) Then
            Return numericValue
        Else
            Return New Nullable(Of Double)
        End If

    End Function

    Private _deltaAperture As ResettableDouble
    ''' <summary>
    ''' Gets the delta aperture.
    ''' </summary>
    ''' <value>The delta aperture.</value>
    Public ReadOnly Property DeltaAperture() As ResettableDouble
        Get
            Return Me._deltaAperture
        End Get
    End Property

    ''' <summary>Gets or sets the sense current integration period in NPLC.</summary>
    Public Property DeltaAperture(ByVal access As ResourceAccessLevels) As Double
        Get
            If Subsystem.IsDeviceAccess(access) OrElse Not Me._deltaAperture.Value.HasValue Then
                ' send the serial command.
                Dim scpiCommand As String = Syntax.BuildQuery(Me.Name, Syntax.VoltageModalityName, Syntax.IntegrationPeriodCommand)
                Dim reply As String = SystemSubsystem.SerialQuery(MyBase.Controller, scpiCommand, serialCommTimeoutInterval)
                If String.IsNullOrWhiteSpace(reply) Then
                    Throw New TimeoutException("Timeout fetching Delta Integration period using the command '" & scpiCommand & "'")
                Else
                    Me._deltaAperture.Value = parseDouble(reply)
                    Me._deltaAperture.ActualValue = Me._deltaAperture.Value
                End If
            End If
            Return Me._deltaAperture.Value.Value
        End Get
        Set(ByVal value As Double)
            If Not Subsystem.IsCacheAccess(access) AndAlso (Subsystem.IsDeviceAccess(access) OrElse (value <> deltaAperture)) Then
                Dim scpiCommand As String = Syntax.BuildCommand(Me.Name, Syntax.VoltageModalityName,
                                                                Syntax.IntegrationPeriodCommand,
                                                                value)
                SystemSubsystem.SerialSend(MyBase.Controller, scpiCommand)
            End If
            Me._deltaAperture.Value = value
        End Set
    End Property

    Private _deltaVoltageRange As ResettableDouble
    ''' <summary>
    ''' Gets the delta voltage range.
    ''' </summary>
    ''' <value>The delta voltage range.</value>
    Public ReadOnly Property DeltaVoltageRange() As ResettableDouble
        Get
            Return Me._deltaVoltageRange
        End Get
    End Property

    ''' <summary>Gets or sets the delta sense voltage Range.   Set to
    ''' <see cref="Scpi.Syntax.Infinity">infinity</see> to set to maximum or to 
    ''' <see cref="Scpi.Syntax.Infinity">negative infinity</see> for minimum.</summary>
    Public Property DeltaVoltageRange(ByVal access As ResourceAccessLevels) As Double
        Get
            If Subsystem.IsDeviceAccess(access) OrElse Not Me._deltaVoltageRange.Value.HasValue Then
                Dim scpiCommand As String = ":SENS:VOLT:RANG?"
                Dim reply As String = SystemSubsystem.SerialQuery(MyBase.Controller, scpiCommand, serialCommTimeoutInterval)
                If String.IsNullOrWhiteSpace(reply) Then
                    Throw New TimeoutException("Timeout fetching Delta Voltage Range using the command '" & scpiCommand & "'")
                Else
                    Me._deltaVoltageRange.Value = parseDouble(reply)
                    Me._deltaVoltageRange.ActualValue = Me._deltaVoltageRange.Value
                End If
            End If
            Return Me._deltaVoltageRange.Value.Value
        End Get
        Set(ByVal value As Double)
            If Not Subsystem.IsCacheAccess(access) AndAlso
                (Subsystem.IsDeviceAccess(access) OrElse (value <> deltaVoltageRange)) Then
                If value >= (Scpi.Syntax.Infinity - 1) Then
                    SystemSubsystem.SerialSend(MyBase.Controller, ":SENS:VOLT:RANG MAX")
                ElseIf value <= (Scpi.Syntax.NegativeInfinity + 1) Then
                    SystemSubsystem.SerialSend(MyBase.Controller, ":SENS:VOLT:RANG MIN")
                Else
                    SystemSubsystem.SerialSend(MyBase.Controller, ":SENS:VOLT:RANG " & value.ToString(Globalization.CultureInfo.CurrentCulture))
                End If
            End If
            Me._deltaVoltageRange.Value = value
        End Set
    End Property

    Private _deltaVoltageAutoRange As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the delta voltage auto range.
    ''' </summary>
    ''' <value>The delta voltage auto range.</value>
    Public ReadOnly Property DeltaVoltageAutoRange() As ResettableValue(Of Boolean)
        Get
            Return Me._deltaVoltageAutoRange
        End Get
    End Property

    ''' <summary>Gets or sets the delta sense voltage Range.   Set to
    ''' <see cref="Scpi.Syntax.Infinity">infinity</see> to set to maximum or to 
    ''' <see cref="Scpi.Syntax.Infinity">negative infinity</see> for minimum.</summary>
    Public Property DeltaVoltageAutoRange(ByVal access As ResourceAccessLevels) As Boolean
        Get
            If Subsystem.IsDeviceAccess(access) OrElse Not Me._deltaVoltageAutoRange.Value.HasValue Then
                Dim scpiCommand As String = ":SENS:VOLT:RANG:AUTO?"
                Dim reply As String = SystemSubsystem.SerialQuery(MyBase.Controller, scpiCommand, serialCommTimeoutInterval)
                If String.IsNullOrWhiteSpace(reply) Then
                    Throw New TimeoutException("Timeout fetching Delta Voltage Auto Range using the command '" & scpiCommand & "'")
                Else
                    Me._deltaVoltageAutoRange.Value = reply.Equals("1", StringComparison.OrdinalIgnoreCase)
                    Me._deltaVoltageAutoRange.ActualValue = Me._deltaVoltageAutoRange.Value
                End If
            End If
            Return Me._deltaVoltageAutoRange.Value.Value
        End Get
        Set(ByVal value As Boolean)
            If Not Subsystem.IsCacheAccess(access) AndAlso
                (Subsystem.IsDeviceAccess(access) OrElse (value <> deltaVoltageAutoRange)) Then
                SystemSubsystem.SerialSend(MyBase.Controller, ":SENS:VOLT:RANG:AUTO " &
                                           isr.Scpi.Syntax.BuildBooleanValue(value, BooleanDataFormat.OneZero))
            End If
            Me._deltaVoltageAutoRange.Value = value
        End Set
    End Property

#End Region

#Region " FUNCTIONS "

    Private _activeFunction As ScpiFunction
    ''' <summary>
    ''' Gets or sets reference to the selected function.
    ''' </summary>
    Public Property ActiveFunction() As ScpiFunction
        Get
            Return Me._activeFunction
        End Get
        Set(ByVal value As ScpiFunction)
            Me._activeFunction = value
        End Set
    End Property

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal value As ScpiFunction, ByVal access As ResourceAccessLevels)
        Me._activeFunction = value
        If value IsNot Nothing AndAlso Not Me._supportsMultiFunctions Then
            Me.FunctionMode(access) = Me._activeFunction.SenseModality
        End If
    End Sub

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal value As SenseFunctionModes, ByVal access As ResourceAccessLevels)
        If value <> SenseFunctionModes.None Then
            For Each scpiFunction As ScpiFunction In Me._functionCollection
                If scpiFunction.SenseModality = value Then
                    Me.ActivateFunction(scpiFunction, access)
                    Return
                End If
            Next
            Throw New ScpiFunctionNotDefinedException(value)
        End If
    End Sub

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal access As ResourceAccessLevels)
        Me.ActivateFunction(Me.FunctionMode(access), access)
    End Sub

    ''' <summary>
    ''' Adds a sense function to the collection of sense functions.
    ''' Makes the function the 
    ''' <see cref="ActiveFunction">active function.</see>
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the function header that is used when addressing 
    ''' this instrument function using the SCPI commands.</param>
    ''' <param name="modality">Defines the <see cref="SenseFunctionModes">SCPI function or element</see>.</param>
    Public Function AddFunction(ByVal syntaxHeader As String, ByVal modality As SenseFunctionModes) As ScpiFunction

        Me._activeFunction = Me._functionCollection.Add(New ScpiFunction(syntaxHeader,
                                                                  MyBase.Controller.SystemSubsystem.LineFrequency,
                                                                  modality))
        Return Me._activeFunction

    End Function

    Private _functionCollection As ResettableKeyableCollection(Of ScpiFunction)
    ''' <summary>
    ''' Gets reference to the collection of sense functions
    ''' </summary>
    Public ReadOnly Property FunctionCollection() As ResettableKeyableCollection(Of ScpiFunction)
        Get
            Return Me._functionCollection
        End Get
    End Property

    Private _functionMode As ResettableValue(Of SenseFunctionModes)
    ''' <summary>
    ''' Gets the function mode.
    ''' </summary>
    ''' <value>The function mode.</value>
    Public ReadOnly Property FunctionMode() As ResettableValue(Of SenseFunctionModes)
        Get
            Return Me._functionMode
        End Get
    End Property

    ''' <summary>Gets or sets the <see cref="senseFunctionModes">sense function mode</see>.</summary>
    ''' <remarks>This method is useful with instruments having a single sense function.</remarks>
    Public Property FunctionMode(ByVal access As ResourceAccessLevels) As SenseFunctionModes
        Get
            Me._functionMode.Value = Syntax.ParseSenseFunctionMode(MyBase.Getter(Syntax.FunctionCommand,
                                           SenseSubsystem.BuildFunctionModesRecord(Me._functionMode.Value), access))
            Me._functionMode.ActualValue = Me._functionMode.Value.Value
            Return Me._functionMode.Value.Value
        End Get
        Set(ByVal value As SenseFunctionModes)
            Me._functionMode.Value = Syntax.ParseSenseFunctionMode(MyBase.Setter(Syntax.FunctionCommand,
                                             Syntax.ExtractScpi(Me._functionMode.Value),
                                            Syntax.ExtractScpi(value), access))
        End Set
    End Property

    Private _functionsDisabled As ResettableValue(Of SenseFunctionModes)
    ''' <summary>
    ''' Gets the functions disabled.
    ''' </summary>
    ''' <value>The functions disabled.</value>
    Public ReadOnly Property FunctionsDisabled() As ResettableValue(Of SenseFunctionModes)
        Get
            Return Me._functionsDisabled
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the 'off' functions.
    ''' Unlike <see cref="FunctionMode">function mode</see>  this property is capable of turning 
    ''' on multiple sense functions.
    ''' </summary>
    Public Property FunctionsDisabled(ByVal access As ResourceAccessLevels) As SenseFunctionModes
        Get
            Me._functionsDisabled.Value = Syntax.ParseSenseFunctionMode(MyBase.Getter(Syntax.FunctionCommand,
                                            Syntax.Off,
                                            SenseSubsystem.BuildFunctionModesRecord(Me._functionsDisabled.Value),
                                            access))
            Me._functionsDisabled.ActualValue = Me._functionsDisabled.Value.Value
            Return Me._functionsDisabled.Value.Value
        End Get
        Set(ByVal Value As SenseFunctionModes)
            If Value = SenseFunctionModes.None Then
                Me._functionsDisabled.Value = Value
            Else
                Me._functionsDisabled.Value = Syntax.ParseSenseFunctionMode(MyBase.Setter(Syntax.FunctionCommand,
                                                Syntax.Off,
                                                SenseSubsystem.BuildFunctionModesRecord(Me._functionsDisabled.Value),
                                                SenseSubsystem.BuildFunctionModesRecord(Value), access))
            End If
        End Set
    End Property

    Private _functionsEnabled As ResettableValue(Of SenseFunctionModes)
    ''' <summary>
    ''' Gets the functions enabled.
    ''' </summary>
    ''' <value>The functions enabled.</value>
    Public ReadOnly Property FunctionsEnabled() As ResettableValue(Of SenseFunctionModes)
        Get
            Return Me._functionsEnabled
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the 'on' functions.
    ''' Unlike <see cref="FunctionMode">function mode</see>  this property is capable of turning 
    ''' on multiple sense functions.
    ''' </summary>
    Public Property FunctionsEnabled(ByVal access As ResourceAccessLevels) As SenseFunctionModes
        Get
            Me._functionsEnabled.Value = Syntax.ParseSenseFunctionMode(MyBase.Getter(Syntax.FunctionCommand,
                                           SenseSubsystem.BuildFunctionModesRecord(Me._functionsEnabled.Value), access))
            Me._functionsEnabled.ActualValue = Me._functionsEnabled.Value.Value
            Return Me._functionsEnabled.Value.Value
        End Get
        Set(ByVal Value As SenseFunctionModes)
            If Value = SenseFunctionModes.None Then
                Me._functionsEnabled.Value = Value
            Else
                Me._functionsEnabled.Value = Syntax.ParseSenseFunctionMode(MyBase.Setter(Syntax.FunctionCommand,
                                                 SenseSubsystem.BuildFunctionModesRecord(Me._functionsEnabled.Value),
                                                SenseSubsystem.BuildFunctionModesRecord(Value), access))
            End If
        End Set
    End Property

    Private _isFunctionConcurrent As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the is function concurrent.
    ''' </summary>
    ''' <value>The is function concurrent.</value>
    Public ReadOnly Property IsFunctionConcurrent() As ResettableValue(Of Boolean)
        Get
            Return Me._isFunctionConcurrent
        End Get
    End Property

    ''' <summary>
    ''' Gets or set the control of concurrent measurements.
    ''' </summary>
    Public Property IsFunctionConcurrent(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter("FUNC:CONC", Me._isFunctionConcurrent, access)
            Return Me.IsFunctionConcurrent.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            Me._isFunctionConcurrent = MyBase.Setter("FUNC:CONC", Me._isFunctionConcurrent, Value, BooleanDataFormat.OneZero, access)
        End Set
    End Property

    Private _supportsMultiFunctions As Boolean
    ''' <summary>
    ''' Gets or sets the condition telling if the instrument supports multi-functions.
    ''' For example, the 2400 source-measure instrument support measuring voltage, current, and resistance concurrently
    ''' whereas the 2700 supports a single function at a time.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
    Public Property SupportsMultiFunctions() As Boolean
        Get
            Return Me._supportsMultiFunctions
        End Get
        Set(ByVal value As Boolean)
            Me._supportsMultiFunctions = value
        End Set
    End Property

    ''' <summary>
    ''' Sets integration periods on all functions.
    ''' </summary>
    Public Sub UpdateIntegrationPeriods(ByVal value As Double, ByVal access As ResourceAccessLevels)
        For Each scpiFunction As ScpiFunction In Me._functionCollection
            Me.IntegrationPeriod(scpiFunction, access) = value
        Next
    End Sub

    ''' <summary>
    ''' Sets auto range on all functions.
    ''' </summary>
    Public Sub UpdateAutoRanges(ByVal value As Boolean, ByVal access As ResourceAccessLevels)
        For Each scpiFunction As ScpiFunction In Me._functionCollection
            Me.AutoRange(scpiFunction, access) = value
        Next
    End Sub

#End Region

#Region " ANY FUNCTION "

    ''' <summary>
    ''' Gets or sets the condition for auto current range.
    ''' </summary>
    ''' <value><c>True</c> if [auto range]; otherwise, <c>False</c>.</value>
    ''' <exception cref="System.ArgumentNullException">scpiFunction</exception>
    Public Property AutoRange(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Boolean
        Get
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            MyBase.Getter(scpiFunction.SyntaxHeader, Syntax.AutoRangeCommand, scpiFunction.AutoRange, access)
            Return scpiFunction.AutoRange.Value.Value
        End Get
        Set(ByVal value As Boolean)
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            scpiFunction.AutoRange = MyBase.Setter(scpiFunction.SyntaxHeader, Syntax.AutoRangeCommand,
                                                   scpiFunction.AutoRange, value, BooleanDataFormat.OnOff, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the integration period of the specified function in seconds.
    ''' </summary>
    ''' <value>The integration period.</value>
    ''' <exception cref="System.ArgumentNullException">scpiFunction</exception>
    ''' <param name="scpiFunction"></param>
    '''   <param name="access"></param>
    '''   <returns></returns>
    Public Property IntegrationPeriod(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            MyBase.Getter(scpiFunction.SyntaxHeader, "NPLC", scpiFunction.PowerLineCycles, access)
            Return scpiFunction.IntegrationPeriod.Value.Value
        End Get
        Set(ByVal Value As Double)
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            scpiFunction.PowerLineCycles = MyBase.Setter(scpiFunction.SyntaxHeader, "NPLC",
                                                         scpiFunction.PowerLineCycles, Value * scpiFunction.LineFrequency,
                                                         access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the sense protection (compliance) of the active sense function.
    ''' </summary>
    ''' <value>The protection level.</value>
    ''' <exception cref="System.ArgumentNullException">scpiFunction</exception>
    '''   <returns></returns>
    ''' <remarks>Specifies the sense protection level (compliance) for the active sense function.
    ''' A current sense compliance specifies the current limit of a voltage source and conversely,
    ''' the voltage sense compliance specifies the voltage limit on a current source.</remarks>
    Public Property ProtectionLevel(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            MyBase.Getter(scpiFunction.SyntaxHeader, "PROT", scpiFunction.ProtectionLevel, access)
            Return scpiFunction.ProtectionLevel.Value.Value
        End Get
        Set(ByVal Value As Double)
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            scpiFunction.ProtectionLevel = MyBase.Setter(scpiFunction.SyntaxHeader, "PROT",
                                                         scpiFunction.ProtectionLevel, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the upper range of the active sense function.
    ''' </summary>
    ''' <value>The range.</value>
    ''' <exception cref="System.ArgumentNullException">scpiFunction</exception>
    '''   <returns></returns>
    Public Property Range(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            MyBase.Getter(scpiFunction.SyntaxHeader, "RANG", scpiFunction.Range, access)
            Return scpiFunction.Range.Value.Value
        End Get
        Set(ByVal Value As Double)
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            scpiFunction.Range = MyBase.Setter(scpiFunction.SyntaxHeader, "RANG",
                                               scpiFunction.Range, Value, access)
        End Set
    End Property


#End Region

#Region " ACTIVE FUNCTION "

    ''' <summary>Gets or sets the condition for auto current range.</summary>
    Public Property AutoRange(ByVal access As ResourceAccessLevels) As Boolean
        Get
            Return Me.AutoRange(Me._activeFunction, access)
        End Get
        Set(ByVal value As Boolean)
            Me.AutoRange(Me._activeFunction, access) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the integration period of the active function in seconds.
    ''' </summary>
    Public Property IntegrationPeriod(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.IntegrationPeriod(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.IntegrationPeriod(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the sense protection (compliance) of the active sense function.
    ''' </summary>
    ''' <remarks>
    ''' Specifies the sense protection level (compliance) for the active sense function. 
    ''' A current sense compliance specifies the current limit of a voltage source and conversely, 
    ''' the voltage sense compliance specifies the voltage limit on a current source.
    ''' </remarks>
    Public Property ProtectionLevel(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.ProtectionLevel(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.ProtectionLevel(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the upper range of the active sense function.
    ''' </summary>
    Public Property Range(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.Range(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.Range(Me._activeFunction, access) = Value
        End Set
    End Property

#End Region

#Region " MEASUREMENTS "

    ''' <summary>
    ''' Configures the sense system for measurement.
    ''' </summary>
    ''' <param name="functions">Specifies the function mode.</param>
    ''' <param name="range">Specifies the measurement range.</param>
    ''' <param name="digits">Specifies the display digits</param>
    ''' <remarks>
    ''' Use this method to configure a measurement using the SCPI single-oriented measurement commands.
    ''' </remarks>
    Public Sub Configure(ByVal functions As SenseFunctionModes, ByVal range As String, ByVal digits As String)

        ' get the function based on the enumerated value
        Dim queryCommand As String = Syntax.ExtractScpi(functions)

        If Not String.IsNullOrWhiteSpace(queryCommand) Then

            ' set the configure command.
            queryCommand = ":CONF:" & queryCommand

            ' if we have a command check if we need to add the range
            If Not String.IsNullOrWhiteSpace(range) Then
                queryCommand = queryCommand & " " & range
            End If

            If Not String.IsNullOrWhiteSpace(digits) Then
                queryCommand = queryCommand & " ," & digits
            End If

            ' configure
            MyBase.Controller.WriteLine(queryCommand)

        End If

    End Sub

#End Region

End Class

