''' <summary>Defines a SCPI Trigger Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class TriggerSubsystem
    Inherits Subsystem

#Region " SHARED "

    Private Shared _scpiName As String = "TRIG"

    ''' <summary>Initiates operation.</summary>
    ''' <param name="controller">Reference to a <see cref="Scpi.IDevice">SCPI controller</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub Initiate(ByVal controller As Scpi.IDevice)

        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If
        controller.WriteLine(":INIT")

    End Sub

    ''' <summary>Initiates operation.</summary>
    ''' <param name="controller">Reference to a <see cref="Scpi.IDevice">SCPI controller</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub Abort(ByVal controller As Scpi.IDevice)

        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If
        controller.WriteLine(":ABOR")

    End Sub

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="controller">Reference to an open <see cref="Scpi.IDevice">SCPI instrument</see>.</param>
    Public Sub New(ByVal controller As Scpi.IDevice)

        ' instantiate the base class
        MyBase.New(TriggerSubsystem._scpiName, controller)

        Me._autoDelay = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._autoDelay)
        Me._count = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._count)
        Me._delay = New ResettableDouble
        MyBase.ResettableValues.Add(Me._delay)
        Me._bypass = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._bypass)
        Me._inputEvent = New ResettableValue(Of TriggerEvent)
        MyBase.ResettableValues.Add(Me._inputEvent)
        Me._inputLineNumber = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._inputLineNumber)
        Me._outputEvent = New ResettableValue(Of TriggerEvent)
        MyBase.ResettableValues.Add(Me._outputEvent)
        Me._outputLineNumber = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._outputLineNumber)
        Me._source = New ResettableValue(Of ArmSource)
        MyBase.ResettableValues.Add(Me._source)
        Me._timerSeconds = New ResettableDouble
        MyBase.ResettableValues.Add(Me._timerSeconds)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called


                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " PROPERTIES "

    Private _autoDelay As ResettableValue(Of Boolean)
    ''' <summary>Gets the auto Delay cached value.
    ''' </summary>
    Public ReadOnly Property AutoDelay() As ResettableValue(Of Boolean)
        Get
            Return Me._autoDelay
        End Get
    End Property

    ''' <summary>Gets or sets the condition for auto current Delay.
    ''' </summary>
    Public Property AutoDelay(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.AutoDelayCommand, Me._autoDelay, access)
            Return Me._autoDelay.Value.Value
        End Get
        Set(ByVal value As Boolean)
            Me._autoDelay = MyBase.Setter(Syntax.AutoDelayCommand, Me._autoDelay, value, BooleanDataFormat.OnOff, access)
        End Set
    End Property

    Private _count As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the count.
    ''' </summary>
    ''' <value>The count.</value>
    Public ReadOnly Property Count() As ResettableValue(Of Integer)
        Get
            Return Me._count
        End Get
    End Property

    ''' <summary>Gets or sets the condition for auto current Count.</summary>
    Public Property Count(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(Syntax.CountCommand, Me._count, access)
            Return Me._count.Value.Value
        End Get
        Set(ByVal value As Integer)
            Me._count = MyBase.Setter(Syntax.CountCommand, Me._count, value, access)
        End Set
    End Property

    Private _delay As ResettableDouble
    ''' <summary>
    ''' Gets the delay.
    ''' </summary>
    ''' <value>The delay.</value>
    Public ReadOnly Property Delay() As ResettableDouble
        Get
            Return Me._delay
        End Get
    End Property

    ''' <summary>Gets or sets the condition for auto current Delay.</summary>
    Public Property Delay(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(Syntax.DelayCommand, Me._delay, access)
            Return Me._delay.Value.Value
        End Get
        Set(ByVal value As Double)
            Me._delay = MyBase.Setter(Syntax.DelayCommand, Me._delay, value, access)
        End Set
    End Property

    Private _bypass As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the bypass.
    ''' </summary>
    ''' <value>The bypass.</value>
    Public ReadOnly Property Bypass() As ResettableValue(Of Boolean)
        Get
            Return Me._bypass
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the bypass (Acceptor) or Enable (source) of the arm layer.
    ''' </summary>
    Public Property Bypass(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.Direction, Me._bypass, access)
            Return Me._bypass.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            Me._bypass = MyBase.Setter(Syntax.Direction, Me._bypass, Value, BooleanDataFormat.AcceptorSource, access)
        End Set
    End Property

    Private _inputEvent As ResettableValue(Of TriggerEvent)
    ''' <summary>
    ''' Gets the input event.
    ''' </summary>
    ''' <value>The input event.</value>
    Public ReadOnly Property InputEvent() As ResettableValue(Of TriggerEvent)
        Get
            Return Me._inputEvent
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the InputEvent e.g., SOURce, DELay, SENSe, NONE.
    ''' </summary>
    Public Property InputEvent(ByVal access As ResourceAccessLevels) As TriggerEvent
        Get
            Me.InputEvent.Value = Syntax.ParseTriggerEvent(MyBase.Getter(Syntax.Input,
                                                              Syntax.ExtractScpi(Me._inputEvent.Value),
                                                              access))
            Me.InputEvent.ActualValue = Me.InputEvent.Value.Value
            Return Me.InputEvent.Value.Value
        End Get
        Set(ByVal Value As TriggerEvent)
            Me._inputEvent.Value = Syntax.ParseTriggerEvent(MyBase.Setter(Syntax.Input,
                                                Syntax.ExtractScpi(Me._inputEvent.Value),
                                                 Syntax.ExtractScpi(Value),
                                                 access))
        End Set
    End Property

    Private _inputLineNumber As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the input line number.
    ''' </summary>
    ''' <value>The input line number.</value>
    Public ReadOnly Property InputLineNumber() As ResettableValue(Of Integer)
        Get
            Return Me._inputLineNumber
        End Get
    End Property
    ''' <summary>
    ''' Gets or sets the input line number of the active layer.
    ''' </summary>
    Public Property InputLineNumber(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(Syntax.InputLine, Me._inputLineNumber, access)
            Return Me._inputLineNumber.Value.Value
        End Get
        Set(ByVal Value As Integer)
            Me._inputLineNumber = MyBase.Setter(Syntax.InputLine, Me._inputLineNumber, Value, access)
        End Set
    End Property

    Private _outputEvent As ResettableValue(Of TriggerEvent)
    ''' <summary>
    ''' Gets the output event.
    ''' </summary>
    ''' <value>The output event.</value>
    Public ReadOnly Property OutputEvent() As ResettableValue(Of TriggerEvent)
        Get
            Return Me._outputEvent
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the OutputEvent e.g., SOURce, DELay, SENSe, NONE.
    ''' </summary>
    Public Property OutputEvent(ByVal access As ResourceAccessLevels) As TriggerEvent
        Get
            Me.OutputEvent.Value = Syntax.ParseTriggerEvent(MyBase.Getter(Syntax.Output,
                                                                Syntax.ExtractScpi(Me.OutputEvent.Value), access))
            Me.OutputEvent.ActualValue = Me.OutputEvent.Value.Value
            Return Me.OutputEvent.Value.Value
        End Get
        Set(ByVal Value As TriggerEvent)
            Me.OutputEvent.Value = Syntax.ParseTriggerEvent(MyBase.Setter(Syntax.Output,
                                                               Syntax.ExtractScpi(Me.OutputEvent.Value),
                                                               Syntax.ExtractScpi(Value), access))
        End Set
    End Property

    Private _outputLineNumber As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the output line number.
    ''' </summary>
    ''' <value>The output line number.</value>
    Public ReadOnly Property OutputLineNumber() As ResettableValue(Of Integer)
        Get
            Return Me._outputLineNumber
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the output line number of the active layer.
    ''' </summary>
    Public Property OutputLineNumber(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(Syntax.OutputLine, Me.OutputLineNumber, access)
            Return Me.OutputLineNumber.Value.Value
        End Get
        Set(ByVal Value As Integer)
            Me._outputLineNumber = MyBase.Setter(Syntax.OutputLine, Me.OutputLineNumber, Value, access)
        End Set
    End Property

    Private _source As ResettableValue(Of ArmSource)
    ''' <summary>
    ''' Gets the source.
    ''' </summary>
    ''' <value>The source.</value>
    Public ReadOnly Property Source() As ResettableValue(Of ArmSource)
        Get
            Return Me._source
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the source immediate, manual, bus, trigger link, 
    ''' start test or stop test ior timer depending on the instrument..
    ''' </summary>
    Public Property Source(ByVal access As ResourceAccessLevels) As ArmSource
        Get
            Me.Source.Value = Syntax.ParseArmSource(MyBase.Getter(Syntax.Source,
                                                                      Syntax.ExtractScpi(Me.Source.Value), access))
            Me.Source.ActualValue = Me.Source.Value.Value
            Return Me.Source.Value.Value
        End Get
        Set(ByVal Value As ArmSource)
            Me.Source.Value = Syntax.ParseArmSource(MyBase.Setter(Syntax.Source,
                                                                      Syntax.ExtractScpi(Me.Source.Value),
                                                                      Syntax.ExtractScpi(Value), access))
        End Set
    End Property

    Private _timerSeconds As ResettableDouble
    ''' <summary>
    ''' Gets the timer seconds.
    ''' </summary>
    ''' <value>The timer seconds.</value>
    Public ReadOnly Property TimerSeconds() As ResettableDouble
        Get
            Return Me._timerSeconds
        End Get
    End Property
    ''' <summary>
    ''' Requests a programmed timer interval.
    ''' </summary>
    Public Property TimerSeconds(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(Syntax.Timer, Me._timerSeconds, access)
            Return Me._timerSeconds.Value.Value
        End Get
        Set(ByVal value As Double)
            Me._timerSeconds = MyBase.Setter(Syntax.Timer, Me._timerSeconds, value, access)
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>Initiates operation.</summary>
    ''' <returns>True if success or false if error.</returns>
    Public Function Abort() As Boolean

        TriggerSubsystem.Abort(MyBase.Controller)
        Return Not MyBase.Controller.HadError

    End Function

    ''' <summary>Clears any pending triggers.</summary>
    ''' <returns>True if success or false if error.</returns>
    Public Function ClearTriggers() As Boolean

        MyBase.Execute(Syntax.ClearCommand)
        Return Not MyBase.Controller.HadError

    End Function

    ''' <summary>Initiates operation.</summary>
    ''' <returns>True if success or false if error.</returns>
    Public Function Initiate() As Boolean

        TriggerSubsystem.Initiate(MyBase.Controller)
        Return Not MyBase.Controller.HadError

    End Function

#End Region

End Class
