''' <summary>Defines a SCPI Base Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public MustInherit Class Subsystem

    Implements IDisposable, IResettable, ISubsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructor for a sub system.
    ''' </summary>
    ''' <param name="name">Specifies the system name or syntax header.</param>
    ''' <param name="instrument">Reference to the interface to the instrument.</param>
    Protected Sub New(ByVal name As String, ByVal instrument As Scpi.IDevice)

        ' instantiate the base class
        MyBase.New()

        Me._name = name
        Me._controller = instrument
        Me._resettableValues = New Scpi.ResettableCollection(Of Scpi.IResettable)

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._controller = Nothing

                End If
            End If

            ' Free shared unmanaged resources

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " ISUBSYSTEM "

    Private _controller As IDevice
    ''' <summary>
    ''' Gets or sets reference to the SCPI controller for accessing the actual instrument.
    ''' </summary>
    Public ReadOnly Property Controller() As IDevice Implements ISubsystem.Controller
        Get
            Return Me._controller
        End Get
    End Property

    Private _name As String
    ''' <summary>
    ''' Gets the system name or syntax header.
    ''' </summary>
    Public ReadOnly Property Name() As String Implements ISubsystem.Name
        Get
            Return Me._name
        End Get
    End Property

#End Region

#Region " IRESETTABLE "

    Private _resettableValues As Scpi.ResettableCollection(Of Scpi.IResettable)
    ''' <summary>
    ''' Holds all resettable values for this subsystem.
    ''' </summary>
    Protected ReadOnly Property ResettableValues() As Scpi.ResettableCollection(Of Scpi.IResettable)
        Get
            Return Me._resettableValues
        End Get
    End Property

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the CLS default values.
    ''' </summary>
    Public Overridable Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        Return Me._resettableValues.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to its default system preset values.
    ''' </summary>
    Public Overridable Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        Return Me._resettableValues.PresetKnownState()
    End Function

    ''' <summary>Returns the subsystem to its default known state by setting properties
    '''   to default values.</summary>
    Public Overridable Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        Return Me._resettableValues.ResetKnownState()
    End Function

#End Region

#Region " ACCESS LEVEL "

    ''' <summary>
    ''' Returns true if cache only access level is requested.
    ''' </summary>
    Public Shared Function IsCacheAccess(ByVal access As ResourceAccessLevels) As Boolean
        Return (access And ResourceAccessLevels.Cache) <> 0
    End Function

    ''' <summary>
    ''' Returns true if device access level is requested.
    ''' </summary>
    Public Shared Function IsDeviceAccess(ByVal access As ResourceAccessLevels) As Boolean
        Return (access And ResourceAccessLevels.Device) <> 0
    End Function

    ''' <summary>
    ''' Returns true if device verification access level is requested.
    ''' </summary>
    Public Shared Function IsVerifyAccess(ByVal access As ResourceAccessLevels) As Boolean
        Return (access And ResourceAccessLevels.Verify) <> 0
    End Function

    ''' <summary>
    ''' Gets or sets reference to the level of <see cref="ResourceAccessLevels">resource access.</see>
    ''' </summary>
    Public Property AccessLevel() As ResourceAccessLevels
        Get
            Return Me._controller.AccessLevel
        End Get
        Set(ByVal value As ResourceAccessLevels)
            Me._controller.AccessLevel = value
        End Set
    End Property

    ''' <summary>
    ''' Save the access level.
    ''' </summary>
    Public Sub StoreResourceAccessMode()
        Me._controller.StoreResourceAccessLevel()
    End Sub

    ''' <summary>
    ''' Restores the access level.
    ''' </summary>
    Public Sub RestoreResourceAccessMode()
        Me._controller.RestoreResourceAccessLevel()
    End Sub

#End Region

#Region " EXECUTE "

    ''' <summary>
    ''' Executes a command based on the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Sub Execute(ByVal subHeader As String)
        Me.Controller.WriteLine(Syntax.BuildExecute(Me.Name, subHeader))
    End Sub

    ''' <summary>
    ''' Executes a command based on the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Sub Execute(ByVal modalityName As String, ByVal subHeader As String)
        Me.Controller.WriteLine(Syntax.BuildExecute(Me.Name, modalityName, subHeader))
    End Sub

    Public Sub Execute(ByVal modalityName As String, ByVal subHeader As String, ByVal isVerifyOperationComplete As Boolean, ByVal isRaiseDeviceErrors As Boolean)
        Me.Controller.WriteLine(Syntax.BuildExecute(Me.Name, modalityName, subHeader), isVerifyOperationComplete, isRaiseDeviceErrors)
    End Sub

#End Region

#Region " QUERIES: BOOLEAN "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryBoolean(ByVal subHeader As String) As Nullable(Of Boolean)
        Return Me.Controller.QueryBoolean(Syntax.BuildQuery(Me.Name, subHeader))
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the third item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryBoolean(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Boolean)
        Return Me.Controller.QueryBoolean(Syntax.BuildQuery(Me.Name, modalityName, subHeader))
    End Function

#End Region

#Region " QUERRIES: DOUBLE "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryDouble(ByVal subHeader As String) As Nullable(Of Double)
        Return Me.Controller.QueryDouble(Syntax.BuildQuery(Me.Name, subHeader))
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the third item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryDouble(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Double)
        Return Me.Controller.QueryDouble(Syntax.BuildQuery(Me.Name, modalityName, subHeader))
    End Function

#End Region

#Region " QUERRIES: INTEGER "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryInteger(ByVal subHeader As String) As Nullable(Of Integer)
        Return Me.Controller.QueryInteger(Syntax.BuildQuery(Me.Name, subHeader))
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the third item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryInteger(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Integer)
        Return Me.Controller.QueryInteger(Syntax.BuildQuery(Me.Name, modalityName, subHeader))
    End Function

#End Region

#Region " QUERRIES: INTEGER - INFINITY "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryInfInteger(ByVal subHeader As String) As Nullable(Of Integer)
        Return Me.Controller.QueryInfInteger(Syntax.BuildQuery(Me.Name, subHeader))
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the third item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryInfInteger(ByVal modalityName As String, ByVal subHeader As String) As Nullable(Of Integer)
        Return Me.Controller.QueryInfInteger(Syntax.BuildQuery(Me.Name, modalityName, subHeader))
    End Function

#End Region

#Region " QUERRIES: STRING "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryTrimEnd(ByVal subHeader As String) As String
        Return Me.Controller.QueryTrimEnd(Syntax.BuildQuery(Me.Name, subHeader))
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the third item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Function QueryTrimEnd(ByVal modalityName As String, ByVal subHeader As String) As String
        Return Me.Controller.QueryTrimEnd(Syntax.BuildQuery(Me.Name, modalityName, subHeader))
    End Function

#End Region

#Region " SETTERS: BOOLEAN "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Setter(ByVal subHeader As String, ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat, ByVal verify As Boolean) As Boolean
        Dim scpiMessage As String = Syntax.BuildCommand(Me.Name, subHeader, value, boolFormat)
        Me.Controller.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Boolean) = Me.QueryBoolean(scpiMessage)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal boolFormat As BooleanDataFormat, ByVal value As Boolean, ByVal verify As Boolean) As Boolean
        Dim scpiMessage As String = Syntax.BuildCommand(Me.Name, modalityName, subHeader, value, boolFormat)
        Me.Controller.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Boolean) = Me.QueryBoolean(modalityName, subHeader)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

#End Region

#Region " SETTERS: DOUBLE "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Setter(ByVal subHeader As String, ByVal value As Double, ByVal verify As Boolean) As Boolean
        Dim scpiMessage As String = Syntax.BuildCommand(Me.Name, subHeader, value)
        Me.Controller.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Double) = Me.QueryDouble(scpiMessage)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Double, ByVal verify As Boolean) As Boolean
        Dim scpiMessage As String = Syntax.BuildCommand(Me.Name, modalityName, subHeader, value)
        Me.Controller.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Double) = Me.QueryDouble(modalityName, subHeader)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

#End Region

#Region " SETTERS: INTEGER "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Setter(ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean
        Dim scpiMessage As String = Syntax.BuildCommand(Me.Name, subHeader, value)
        Me.Controller.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Integer) = Me.QueryInteger(scpiMessage)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean
        Dim scpiMessage As String = Syntax.BuildCommand(Me.Name, modalityName, subHeader, value)
        Me.Controller.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Integer) = Me.QueryInteger(modalityName, subHeader)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function SetterInfinity(ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean
        Dim scpiMessage As String = Syntax.BuildCommand(Me.Name, subHeader, value)
        Me.Controller.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Integer) = Me.QueryInteger(scpiMessage)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function SetterInfinity(ByVal modalityName As String, ByVal subHeader As String, ByVal value As Integer, ByVal verify As Boolean) As Boolean
        Dim scpiMessage As String = Syntax.BuildCommand(Me.Name, modalityName, subHeader, value)
        Me.Controller.WriteLine(scpiMessage)
        If verify Then
            Dim actual As Nullable(Of Integer) = Me.QueryInteger(modalityName, subHeader)
            Return actual.HasValue AndAlso (value = actual.Value)
        Else
            Return True
        End If
    End Function

#End Region

#Region " SETTERS: STRING "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Setter(ByVal subHeader As String, ByVal value As String, ByVal verify As Boolean) As Boolean
        Dim scpiMessage As String = Syntax.BuildCommand(Me.Name, subHeader, value)
        Me.Controller.WriteLine(scpiMessage)
        If verify Then
            Dim actual As String = Me.QueryTrimEnd(subHeader)
            Return Not String.IsNullOrWhiteSpace(actual) AndAlso (value = actual)
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As String, ByVal verify As Boolean) As Boolean
        Dim scpiMessage As String = Syntax.BuildCommand(Me.Name, modalityName, subHeader, value)
        Me.Controller.WriteLine(scpiMessage)
        If verify Then
            Dim actual As String = Me.QueryTrimEnd(modalityName, subHeader)
            Return Not String.IsNullOrWhiteSpace(actual) AndAlso (value = actual)
        Else
            Return True
        End If
    End Function

#End Region

#Region " PROPERTY GETTERS: STRING "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    ''' <param name="access">Specifies the desired access level to the instrument.</param>
    Public Function Getter(ByVal subHeader As String, ByVal value As String, ByVal access As ResourceAccessLevels) As String
        If Subsystem.IsDeviceAccess(access) OrElse String.IsNullOrWhiteSpace(value) Then
            value = Me.QueryTrimEnd(subHeader)
        End If
        Return value
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and 
    ''' the third item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String, ByVal value As String, ByVal access As ResourceAccessLevels) As String
        If Subsystem.IsDeviceAccess(access) OrElse String.IsNullOrWhiteSpace(value) Then
            value = Me.QueryTrimEnd(modalityName, subHeader)
        End If
        Return value
    End Function

#End Region

#Region " PROPERTY GETTERS: BOOLEAN "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Getter(ByVal subHeader As String, ByVal nullableValue As Nullable(Of Boolean),
                           ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
            nullableValue = Me.QueryBoolean(subHeader)
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Function Getter(ByVal subHeader As String, ByVal nullableValue As ResettableValue(Of Boolean),
                           ByVal access As ResourceAccessLevels) As ResettableValue(Of Boolean)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.Value.HasValue Then
            nullableValue.Value = Me.QueryBoolean(subHeader)
            nullableValue.ActualValue = nullableValue.Value
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As Nullable(Of Boolean),
                           ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
            nullableValue = Me.QueryBoolean(modalityName, subHeader)
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="2")>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As ResettableValue(Of Boolean),
                           ByVal access As ResourceAccessLevels) As ResettableValue(Of Boolean)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.Value.HasValue Then
            nullableValue.Value = Me.QueryBoolean(modalityName, subHeader)
            nullableValue.ActualValue = nullableValue.Value
        End If
        Return nullableValue
    End Function

#End Region

#Region " PROPERTY GETTERS: INTEGER "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Getter(ByVal subHeader As String, ByVal nullableValue As Nullable(Of Integer), ByVal access As ResourceAccessLevels) As Nullable(Of Int32)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
            nullableValue = Me.QueryInteger(subHeader)
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Function Getter(ByVal subHeader As String,
                          ByVal nullableValue As ResettableValue(Of Integer), ByVal access As ResourceAccessLevels) As ResettableValue(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.Value.HasValue Then
            nullableValue.Value = Me.QueryInteger(subHeader)
            nullableValue.ActualValue = nullableValue.Value
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As Nullable(Of Integer),
                           ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
            nullableValue = Me.QueryInteger(modalityName, subHeader)
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="2")>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As ResettableValue(Of Integer),
                           ByVal access As ResourceAccessLevels) As ResettableValue(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.Value.HasValue Then
            nullableValue.Value = Me.QueryInteger(modalityName, subHeader)
            nullableValue.ActualValue = nullableValue.Value
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function GetterInfinity(ByVal subHeader As String, ByVal nullableValue As Nullable(Of Integer),
                                   ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
            nullableValue = Me.QueryInfInteger(subHeader)
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Function GetterInfinity(ByVal subHeader As String, ByVal nullableValue As ResettableValue(Of Integer),
                                   ByVal access As ResourceAccessLevels) As ResettableValue(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.Value.HasValue Then
            nullableValue.Value = Me.QueryInfInteger(subHeader)
            nullableValue.ActualValue = nullableValue.Value
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function GetterInfinity(ByVal modalityName As String, ByVal subHeader As String,
                                   ByVal nullableValue As Nullable(Of Integer),
                                   ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
            nullableValue = Me.QueryInfInteger(modalityName, subHeader)
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="2")>
    Public Function GetterInfinity(ByVal modalityName As String, ByVal subHeader As String,
                                   ByVal nullableValue As ResettableValue(Of Integer),
                                   ByVal access As ResourceAccessLevels) As ResettableValue(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.Value.HasValue Then
            nullableValue.Value = Me.QueryInfInteger(modalityName, subHeader)
            nullableValue.ActualValue = nullableValue.Value
        End If
        Return nullableValue
    End Function

#End Region

#Region " PROPERTY GETTERS: DOUBLE "

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Getter(ByVal subHeader As String, ByVal nullableValue As Nullable(Of Double),
                           ByVal access As ResourceAccessLevels) As Nullable(Of Double)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
            nullableValue = Me.QueryDouble(subHeader)
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> and 
    ''' the second item is a <paramref name="command">command</paramref>
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Function Getter(ByVal subHeader As String, ByVal nullableValue As ResettableDouble,
                           ByVal access As ResourceAccessLevels) As ResettableDouble
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.Value.HasValue Then
            nullableValue.Value = Me.QueryDouble(subHeader)
            nullableValue.ActualValue = nullableValue.Value
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As Nullable(Of Double),
                           ByVal access As ResourceAccessLevels) As Nullable(Of Double)
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.HasValue Then
            nullableValue = Me.QueryDouble(modalityName, subHeader)
        End If
        Return nullableValue
    End Function

    ''' <summary>
    ''' Gets a value from the instrument using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref> and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="2")>
    Public Function Getter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As ResettableDouble,
                           ByVal access As ResourceAccessLevels) As ResettableDouble
        If Subsystem.IsDeviceAccess(access) OrElse Not nullableValue.Value.HasValue Then
            nullableValue.Value = Me.QueryDouble(modalityName, subHeader)
            nullableValue.ActualValue = nullableValue.Value
        End If
        Return nullableValue
    End Function

#End Region

#Region " PROPERTIY SETTERS: STRING "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="nullableValue">value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Setter(ByVal subHeader As String, ByVal nullableValue As String,
                           ByVal value As String, ByVal access As ResourceAccessLevels) As String
        If Subsystem.IsDeviceAccess(access) OrElse Not String.Equals(value, nullableValue, StringComparison.OrdinalIgnoreCase) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As String = String.Empty
            actual = Me.Getter(subHeader, actual, access)
            If Not String.Equals(value, actual, StringComparison.OrdinalIgnoreCase) Then
                Throw New VerificationException(
                        Syntax.BuildCommand(Me.Name, subHeader, value), value, actual)
            End If
        End If
        Return value
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String, ByVal nullableValue As String,
                           ByVal value As String, ByVal access As ResourceAccessLevels) As String
        If Subsystem.IsDeviceAccess(access) OrElse Not String.Equals(value, nullableValue, StringComparison.OrdinalIgnoreCase) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, modalityName, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As String = String.Empty
            actual = Me.Getter(modalityName, subHeader, actual, access)
            If Not String.Equals(value, actual, StringComparison.OrdinalIgnoreCase) Then
                Throw New VerificationException(
                        Syntax.BuildCommand(Me.Name, modalityName, subHeader, value), value, actual)
            End If
        End If
        Return value
    End Function

#End Region

#Region " PROPERTIY SETTERS: BOOLEAN "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="nullableValue">value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Setter(ByVal subHeader As String, ByVal nullableValue As Nullable(Of Boolean),
                           ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat, ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, subHeader, value, boolFormat))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As Nullable(Of Boolean) = Me.Getter(subHeader, actual, access)
            If value <> actual.Value Then
                Throw New VerificationException(
                        Syntax.BuildCommand(Me.Name, subHeader, value, boolFormat), value, actual.Value)
            End If
        End If
        Return New Nullable(Of Boolean)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="nullableValue">value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Function Setter(ByVal subHeader As String, ByVal nullableValue As ResettableValue(Of Boolean),
                           ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat,
                           ByVal access As ResourceAccessLevels) As ResettableValue(Of Boolean)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, subHeader, value, boolFormat))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As New ResettableValue(Of Boolean)
            actual = Me.Getter(subHeader, actual, access)
            If value <> actual.Value Then
                If actual.Value.HasValue Then
                    Throw New VerificationException(
                            Syntax.BuildCommand(Me.Name, subHeader, value, boolFormat), value, actual.Value.Value)
                Else
                    Throw New VerificationException("Getter failed reading.")
                End If
            End If
        End If
        nullableValue.Value = value
        Return nullableValue
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As Nullable(Of Boolean), ByVal value As Boolean,
                           ByVal boolFormat As BooleanDataFormat, ByVal access As ResourceAccessLevels) As Nullable(Of Boolean)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, modalityName, subHeader, value, boolFormat))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As Nullable(Of Boolean) = Me.Getter(modalityName, subHeader, actual, access).Value
            If value <> actual Then
                Throw New VerificationException(
                        Syntax.BuildCommand(Me.Name, modalityName, subHeader, value, boolFormat), value, actual.Value)
            End If
        End If
        Return New Nullable(Of Boolean)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="2")>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As ResettableValue(Of Boolean), ByVal value As Boolean,
                           ByVal boolFormat As BooleanDataFormat, ByVal access As ResourceAccessLevels) As ResettableValue(Of Boolean)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, modalityName, subHeader, value, boolFormat))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As New ResettableValue(Of Boolean)
            actual = Me.Getter(modalityName, subHeader, actual, access).Value
            If value <> actual Then

                If actual.Value.HasValue Then
                    Throw New VerificationException(
                            Syntax.BuildCommand(Me.Name, modalityName, subHeader, value, boolFormat), value, actual.Value.Value)
                Else
                    Throw New VerificationException("Getter failed reading.")
                End If
            End If
        End If
        nullableValue.Value = value
        Return nullableValue
    End Function

#End Region

#Region " PROPERTIY SETTERS: INTEGER "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="nullableValue">value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Setter(ByVal subHeader As String, ByVal nullableValue As Nullable(Of Integer),
                           ByVal value As Integer, ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As Nullable(Of Integer) = Me.Getter(subHeader, actual, access)
            If value <> actual Then
                Throw New VerificationException(
                        Syntax.BuildCommand(Me.Name, subHeader, value), value, actual.Value)
            End If
        End If
        Return New Nullable(Of Integer)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="nullableValue">value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Function Setter(ByVal subHeader As String, ByVal nullableValue As ResettableValue(Of Integer),
                           ByVal value As Integer, ByVal access As ResourceAccessLevels) As ResettableValue(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As New ResettableValue(Of Integer)
            actual = Me.Getter(subHeader, actual, access)
            If value <> actual Then

                If actual.Value.HasValue Then
                    Throw New VerificationException(
                            Syntax.BuildCommand(Me.Name, subHeader, value), value, actual.Value.Value)
                Else
                    Throw New VerificationException("Getter failed reading.")
                End If

            End If


        End If
        nullableValue.Value = value
        Return nullableValue
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="nullableValue">value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function SetterInfinity(ByVal subHeader As String, ByVal nullableValue As Nullable(Of Integer),
                                   ByVal value As Integer,
                                   ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As Nullable(Of Integer) = Me.GetterInfinity(subHeader, actual, access)
            If value <> actual Then
                Throw New VerificationException(
                        Syntax.BuildCommand(Me.Name, subHeader, value), value, actual.Value)
            End If
        End If
        Return New Nullable(Of Integer)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="nullableValue">value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Function SetterInfinity(ByVal subHeader As String, ByVal nullableValue As ResettableValue(Of Integer),
                                   ByVal value As Integer,
                                   ByVal access As ResourceAccessLevels) As ResettableValue(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As New ResettableValue(Of Integer)
            actual = Me.GetterInfinity(subHeader, actual, access)
            If value <> actual Then
                If actual.Value.HasValue Then
                    Throw New VerificationException(
                            Syntax.BuildCommand(Me.Name, subHeader, value), value, actual.Value.Value)
                Else
                    Throw New VerificationException("Getter failed reading.")
                End If
            End If
        End If
        nullableValue.Value = value
        Return nullableValue
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As Nullable(Of Integer),
                           ByVal value As Integer, ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, modalityName, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As Nullable(Of Integer) = Me.Getter(modalityName, subHeader, actual, access)
            If value <> actual Then
                Throw New VerificationException(
                        Syntax.BuildCommand(Me.Name, modalityName, subHeader, value), value, actual.Value)
            End If
        End If
        Return New Nullable(Of Integer)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="2")>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As ResettableValue(Of Integer),
                           ByVal value As Integer, ByVal access As ResourceAccessLevels) As ResettableValue(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, modalityName, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As New ResettableValue(Of Integer)
            actual = Me.Getter(modalityName, subHeader, actual, access)
            If value <> actual Then

                If actual.Value.HasValue Then
                    Throw New VerificationException(
                            Syntax.BuildCommand(Me.Name, modalityName, subHeader, value), value, actual.Value.Value)
                Else
                    Throw New VerificationException("Getter failed reading.")
                End If

            End If
        End If
        nullableValue.Value = value
        Return nullableValue
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function SetterInfinity(ByVal modalityName As String, ByVal subHeader As String,
                                   ByVal nullableValue As Nullable(Of Integer),
                                   ByVal value As Integer, ByVal access As ResourceAccessLevels) As Nullable(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, modalityName, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As Nullable(Of Integer) = Me.GetterInfinity(modalityName, subHeader, actual, access)
            If value <> actual Then
                Throw New VerificationException(
                        Syntax.BuildCommand(Me.Name, modalityName, subHeader, value), value, actual.Value)
            End If
        End If
        Return New Nullable(Of Integer)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="2")>
    Public Function SetterInfinity(ByVal modalityName As String, ByVal subHeader As String,
                                   ByVal nullableValue As ResettableValue(Of Integer),
                                   ByVal value As Integer, ByVal access As ResourceAccessLevels) As ResettableValue(Of Integer)
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, modalityName, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As New ResettableValue(Of Integer)
            actual = Me.GetterInfinity(modalityName, subHeader, actual, access)
            If value <> actual Then

                If actual.Value.HasValue Then
                    Throw New VerificationException(
                            Syntax.BuildCommand(Me.Name, modalityName, subHeader, value), value, actual.Value.Value)
                Else
                    Throw New VerificationException("Getter failed reading.")
                End If

            End If
        End If
        nullableValue.Value = value
        Return nullableValue
    End Function

#End Region

#Region " PROPERTIY SETTERS: DOUBLE "

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="nullableValue">value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Setter(ByVal subHeader As String, ByVal nullableValue As Nullable(Of Double),
                           ByVal value As Double, ByVal access As ResourceAccessLevels) As Nullable(Of Double)
        If Subsystem.IsDeviceAccess(access) OrElse ResettableDouble.AreDifferent(value, nullableValue, Single.Epsilon) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As Nullable(Of Double) = Me.Getter(subHeader, actual, access)
            If ResettableDouble.AreDifferent(value, actual.Value, Single.Epsilon) Then
                Throw New VerificationException(
                        Syntax.BuildCommand(Me.Name, subHeader, value), value, actual.Value)
            End If
        End If
        Return New Nullable(Of Double)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see> 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item is the <paramref name="nullableValue">value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="1")>
    Public Function Setter(ByVal subHeader As String, ByVal nullableValue As ResettableDouble,
                           ByVal value As Double, ByVal access As ResourceAccessLevels) As ResettableDouble
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, subHeader, nullableValue.BoundValue(value)))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As New ResettableDouble
            actual = Me.Getter(subHeader, actual, access)
            If value <> actual Then

                If actual.Value.HasValue Then
                    Throw New VerificationException(
                            Syntax.BuildCommand(Me.Name, subHeader, value), value, actual.Value.Value)
                Else
                    Throw New VerificationException("Getter failed reading.")
                End If

            End If
        End If
        nullableValue.Value = value
        Return nullableValue
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As Nullable(Of Double),
                           ByVal value As Double, ByVal access As ResourceAccessLevels) As Nullable(Of Double)
        If Subsystem.IsDeviceAccess(access) OrElse ResettableDouble.AreDifferent(value, nullableValue, Single.Epsilon) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, modalityName, subHeader, value))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As Nullable(Of Double) = Me.Getter(modalityName, subHeader, actual, access)
            If ResettableDouble.AreDifferent(value, actual.Value, Single.Epsilon) Then
                Throw New VerificationException(
                        Syntax.BuildCommand(Me.Name, modalityName, subHeader, value), value, actual.Value)
            End If
        End If
        Return New Nullable(Of Double)(value)
    End Function

    ''' <summary>
    ''' Sets an instrument value using the <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <see cref="Name">SCPI system name</see>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and
    ''' the fourth item represents a <paramref name="nullableValue">numeric value</paramref>.
    ''' </param>
    ''' <param name="nullableValue">Specifies the command value.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId:="2")>
    Public Function Setter(ByVal modalityName As String, ByVal subHeader As String,
                           ByVal nullableValue As ResettableDouble,
                           ByVal value As Double, ByVal access As ResourceAccessLevels) As ResettableDouble
        If Subsystem.IsDeviceAccess(access) OrElse (value <> nullableValue) Then
            Me.Controller.WriteLine(Syntax.BuildCommand(Me.Name, modalityName, subHeader, nullableValue.BoundValue(value)))
        End If
        If Subsystem.IsVerifyAccess(access) Then
            Dim actual As New ResettableDouble
            actual = Me.Getter(modalityName, subHeader, actual, access)
            If value <> actual Then

                If actual.Value.HasValue Then
                    Throw New VerificationException(
                            Syntax.BuildCommand(Me.Name, modalityName, subHeader, value), value, actual.Value.Value)
                Else
                    Throw New VerificationException("Getter failed reading.")
                End If
            End If
        ElseIf Subsystem.IsCacheAccess(access) Then
            nullableValue.ActualValue = value
        End If
        nullableValue.Value = value

        Return nullableValue
    End Function

#End Region

End Class
