''' <summary>Defines a SCPI ARM Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class ArmSubsystem

    Inherits Subsystem

#Region " SHARED "

    Private Shared _scpiName As String = "ARM"

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="controller">Reference to an open <see cref="Scpi.IDevice">SCPI instrument</see>.</param>
    Public Sub New(ByVal controller As Scpi.IDevice)

        ' instantiate the base class
        MyBase.New(ArmSubsystem._scpiName, controller)

        ' create a new instance of the limit collection
        Me._layers = New ResettableKeyableCollection(Of ArmLayer)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        MyBase.ClearExecutionState()
        Return Me._layers.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        MyBase.PresetKnownState()
        Return Me._layers.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        MyBase.ResetKnownState()
        Return Me._layers.ResetKnownState()
    End Function

#End Region

#Region " LAYERS "

    ''' <summary>
    ''' Gets or sets reference to the 
    ''' <see cref="Armlayer">layer</see>.
    ''' </summary>
    Public Property ActiveLayer() As ArmLayer

    ''' <summary>
    ''' Adds a <see cref="Armlayer">layer</see> to the collection of layers.
    ''' Makes the layers the <see cref="Activelayer">active layer.</see>
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the header that is used when addressing 
    ''' the instrument using the SCPI commands.</param>
    Public Function AddLayer(ByVal syntaxHeader As String) As ArmLayer

        Me.ActiveLayer = Me.Layers.Add(New ArmLayer(syntaxHeader))
        Return Me.ActiveLayer

    End Function

    Private _layers As ResettableKeyableCollection(Of ArmLayer)
    ''' <summary>
    ''' Gets reference to the collection of calculation layers
    ''' </summary>
    Public ReadOnly Property Layers() As ResettableKeyableCollection(Of ArmLayer)
        Get
            Return Me._layers
        End Get
    End Property

#End Region

#Region " ACTIVE LAYER "

    ''' <summary>Gets or sets the condition for auto current Delay.
    ''' </summary>
    Public Property AutoDelay(ByVal access As ResourceAccessLevels) As Boolean
        Get
            Return MyBase.Getter(Me.ActiveLayer.SyntaxHeader, Syntax.AutoDelayCommand, Me.ActiveLayer.AutoDelay, access).Value.Value
        End Get
        Set(ByVal value As Boolean)
            Me._activeLayer.AutoDelay = MyBase.Setter(Me._activeLayer.SyntaxHeader, Syntax.AutoDelayCommand,
                                       Me._activeLayer.AutoDelay, value, BooleanDataFormat.OnOff, access)
        End Set
    End Property

    ''' <summary>Gets or sets the condition for auto current Count.</summary>
    Public Property Count(ByVal access As ResourceAccessLevels) As Integer
        Get
            Me._activeLayer.Count = MyBase.GetterInfinity(Me._activeLayer.SyntaxHeader, Syntax.CountCommand, Me._activeLayer.Count, access)
            Return Me._activeLayer.Count.Value.Value
        End Get
        Set(ByVal value As Integer)
            Me._activeLayer.Count = MyBase.SetterInfinity(Me._activeLayer.SyntaxHeader, Syntax.CountCommand, Me._activeLayer.Count, value, access)
        End Set
    End Property

    ''' <summary>Gets or sets the condition for auto current Delay.</summary>
    Public Property Delay(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(Me._activeLayer.SyntaxHeader, Syntax.DelayCommand, Me._activeLayer.Delay, access)
            Return Me._activeLayer.Delay.Value.Value
        End Get
        Set(ByVal value As Double)
            Me._activeLayer.Delay = MyBase.Setter(Me._activeLayer.SyntaxHeader, Syntax.DelayCommand, Me._activeLayer.Delay, value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the bypass (Acceptor) or Enable (source) of the arm layer.
    ''' </summary>
    Public Property Bypass(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Me._activeLayer.SyntaxHeader, Syntax.Direction, Me._activeLayer.Bypass, access)
            Return Me._activeLayer.Bypass.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            Me._activeLayer.Bypass = MyBase.Setter(Me._activeLayer.SyntaxHeader, Syntax.Direction, Me._activeLayer.Bypass, Value, BooleanDataFormat.AcceptorSource, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the input line number of the active layer.
    ''' </summary>
    Public Property InputLineNumber(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(Me._activeLayer.SyntaxHeader, Syntax.InputLine, Me._activeLayer.InputLineNumber, access)
            Return Me._activeLayer.InputLineNumber.Value.Value
        End Get
        Set(ByVal Value As Integer)
            Me._activeLayer.InputLineNumber = MyBase.Setter(Me._activeLayer.SyntaxHeader, Syntax.InputLine, Me._activeLayer.InputLineNumber, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the output line number of the active layer.
    ''' </summary>
    Public Property OutputLineNumber(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(Me._activeLayer.SyntaxHeader, Syntax.OutputLine, Me._activeLayer.OutputLineNumber, access)
            Return Me._activeLayer.OutputLineNumber.Value.Value
        End Get
        Set(ByVal Value As Integer)
            Me._activeLayer.OutputLineNumber = MyBase.Setter(Me._activeLayer.SyntaxHeader, Syntax.OutputLine, Me._activeLayer.OutputLineNumber, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source immediate, manual, bus, trigger link, 
    ''' start test or stop test ior timer depending on the instrument..
    ''' </summary>
    Public Property Source(ByVal access As ResourceAccessLevels) As ArmSource
        Get
            Me.ActiveLayer.Source.Value = Syntax.ParseArmSource(MyBase.Getter(Me._activeLayer.SyntaxHeader, Syntax.Source,
                                                                      Syntax.ExtractScpi(Me._activeLayer.Source.Value), access))
            Me.ActiveLayer.Source.ActualValue = Me._activeLayer.Source.Value.Value
            Return Me.ActiveLayer.Source.Value.Value
        End Get
        Set(ByVal Value As ArmSource)
            Me._activeLayer.Source.Value = Syntax.ParseArmSource(MyBase.Setter(Me._activeLayer.SyntaxHeader, Syntax.Source,
                                                                      Syntax.ExtractScpi(Me._activeLayer.Source.Value),
                                                                      Syntax.ExtractScpi(Value), access))
        End Set
    End Property

    ''' <summary>
    ''' Requests a programmed timer interval.
    ''' </summary>
    Public Property TimerSeconds(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(Me._activeLayer.SyntaxHeader, Syntax.Timer, Me._activeLayer.TimerSeconds, access)
            Return Me._activeLayer.TimerSeconds.Value.Value
        End Get
        Set(ByVal value As Double)
            Me._activeLayer.TimerSeconds = MyBase.Setter(Me._activeLayer.SyntaxHeader, Syntax.Timer, Me._activeLayer.TimerSeconds, value, access)
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>Initiates operation.</summary>
    ''' <returns>True if success or false if error.</returns>
    Public Function Abort() As Boolean

        TriggerSubsystem.Abort(MyBase.Controller)
        Return Not MyBase.Controller.HadError

    End Function

    ''' <summary>Initiates operation.</summary>
    ''' <returns>True if success or false if error.</returns>
    Public Function Initiate() As Boolean

        TriggerSubsystem.Initiate(MyBase.Controller)
        Return Not MyBase.Controller.HadError

    End Function

#End Region

End Class
