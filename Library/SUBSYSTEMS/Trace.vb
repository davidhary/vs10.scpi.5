''' <summary>Defines a SCPI Trace Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class TraceSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="controller">Reference to an open <see cref="Scpi.IDevice">SCPI instrument</see>.</param>
    Public Sub New(ByVal controller As Scpi.IDevice)

        ' instantiate the base class
        MyBase.New("TRAC", controller)

        Me._feedSource = New ResettableValue(Of FeedSource)
        MyBase.ResettableValues.Add(Me._feedSource)
        Me._nextFeedControl = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._nextFeedControl)
        Me._notifyCount = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._notifyCount)
        Me._pointsCount = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._pointsCount)

        Me._defaultTraceSize = 1024

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called


                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " READINGS "

    ''' <summary>
    ''' Requests all readings from the buffer.
    ''' </summary>
    Public Sub QueryReadings()
        MyBase.Controller.QueryTrimEnd(Syntax.BuildQuery(MyBase.Name, Syntax.DataCommand))
    End Sub

    ''' <summary>
    ''' Requests a range of readings.
    ''' </summary>
    ''' <param name="firstPoint"></param>
    ''' <param name="count"></param>
    Public Sub QueryReadings(ByVal firstPoint As Integer, ByVal count As Integer)
        MyBase.Controller.QueryTrimEnd(Syntax.BuildQuery(MyBase.Name, Syntax.DataCommand, firstPoint, count))
    End Sub

    ''' <summary>
    ''' Requests all readings from the buffer.
    ''' </summary>
    Public Sub RequestReadings()
        MyBase.Controller.WriteLine(Syntax.BuildQuery(MyBase.Name, Syntax.DataCommand))
    End Sub

    ''' <summary>
    ''' Requests a range of readings.
    ''' </summary>
    ''' <param name="firstPoint"></param>
    ''' <param name="count"></param>
    Public Sub RequestReadings(ByVal firstPoint As Integer, ByVal count As Integer)
        MyBase.Controller.WriteLine(Syntax.BuildQuery(MyBase.Name, Syntax.DataCommand, firstPoint, count))
    End Sub

#End Region

#Region " BUFFER MANAGEMENT "

    Private _actualPointsCount As Integer?
    ''' <summary>
    ''' Gets the actual number of trace points.
    ''' </summary>
    Public ReadOnly Property ActualPointsCount(ByVal access As ResourceAccessLevels) As Integer
        Get
            Me._actualPointsCount = MyBase.Getter(Syntax.PointsCommand, Syntax.Actual, Me._actualPointsCount, access)
            Return Me._actualPointsCount.Value
        End Get
    End Property

    Private _feedSource As ResettableValue(Of FeedSource)
    Public ReadOnly Property FeedSource() As ResettableValue(Of FeedSource)
        Get
            Return Me._feedSource
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the source of readings such as 
    ''' <see cref="Syntax.Sense">sense</see> or 
    ''' <see cref="Syntax.Calculate1">calculate 1</see> or <see cref="Syntax.Calculate2">calculate2</see>
    ''' </summary>
    Public Property FeedSource(ByVal access As ResourceAccessLevels) As FeedSource
        Get
            Me._feedSource = Syntax.ParseFeedSource(MyBase.Getter(Syntax.Feed,
                                                                     Syntax.ExtractScpi(Me._feedSource.Value),
                                                                     access))
            Return Me._feedSource.Value.Value
        End Get
        Set(ByVal Value As FeedSource)
            Me._feedSource = Syntax.ParseFeedSource(MyBase.Setter(Syntax.Feed,
                                        Syntax.ExtractScpi(Me._feedSource.Value),
                                        Syntax.ExtractScpi(Value), access))
        End Set
    End Property

    Private _nextFeedControl As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the next feed control.
    ''' </summary>
    ''' <value>The next feed control.</value>
    Public ReadOnly Property NextFeedControl() As ResettableValue(Of Boolean)
        Get
            Return Me._nextFeedControl
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the buffer control mode such as 
    ''' <see cref="Syntax.Next">fills buffer and stops</see> or
    ''' <see cref="Syntax.Never">disables buffer storage</see>
    ''' </summary>
    Public Property NextFeedControl(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.Feed, Syntax.Control, Me._nextFeedControl, access)
            Return Me._nextFeedControl.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            Me._nextFeedControl = MyBase.Setter(Syntax.Feed, Syntax.Control, Me._nextFeedControl, Value, BooleanDataFormat.NextNever, access)
        End Set
    End Property

    Private _pointsCount As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the points count.
    ''' </summary>
    ''' <value>The points count.</value>
    Public ReadOnly Property PointsCount() As ResettableValue(Of Integer)
        Get
            Return Me._pointsCount
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the number of Trace Points
    ''' </summary>
    Public Property PointsCount(ByVal access As ResourceAccessLevels) As Integer
        Get
            Return MyBase.Getter(Syntax.PointsCommand, Me.PointsCount, access).Value.Value
        End Get
        Set(ByVal Value As Integer)
            If Subsystem.IsCacheAccess(access) Then
                Me.PointsCount.Value = Value
                Me.PointsCount.ActualValue = Value
            Else
                Dim feedControlEnabled As Boolean = Me.NextFeedControl(access)
                If feedControlEnabled Then
                    Me.NextFeedControl(access) = False
                End If
                If Subsystem.IsDeviceAccess(access) OrElse (Value <> Me.PointsCount) Then
                    MyBase.Setter(Syntax.PointsCommand, Me.PointsCount, Value, access)
                End If
                If isr.Scpi.Subsystem.IsVerifyAccess(access) Then
                    ' set the actual value
                    MyBase.Getter(Syntax.PointsCommand, Me.PointsCount, access)
                    Me.PointsCount.Value = Value
                End If
                ' check if we need to restore feed control.
                If feedControlEnabled Then
                    Me.NextFeedControl(access) = True
                End If
            End If
            Me._notifyCount.ResetValue = Me.PointsCount.Value.Value \ 2
        End Set
    End Property

    ''' <summary>
    ''' Returns True if periodic notifications are enabled.
    ''' </summary>
    Public ReadOnly Property PeriodicNotifyEnabled() As Boolean
        Get
            Return Me._notifyInterval > 0
        End Get
    End Property

    Private _notifyInterval As Integer
    ''' <summary>
    ''' Gets or sets the interval for notifications.  Set to 0 to
    ''' disable periodic notifications.
    ''' </summary>
    Public Property NotifyInterval() As Integer
        Get
            Return Me._notifyInterval
        End Get
        Set(ByVal value As Integer)
            Me._notifyInterval = value
        End Set
    End Property

    Private _notifyCount As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets or sets the number of Trace Points
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="access")>
    Public Property NotifyCount(ByVal access As ResourceAccessLevels) As Integer
        Get
            If Me._notifyCount.Value.HasValue Then
                Return Me._notifyCount.Value.Value
            Else
                Return -1
            End If
        End Get
        Set(ByVal Value As Integer)
            If Value < Me.PointsCount(access) Then
                Me._notifyCount = MyBase.Setter(Syntax.Notify, Me._notifyCount, Value, access)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Update the notification to notify on the next interval.
    ''' </summary>
    Public Sub NotifyNext(ByVal access As ResourceAccessLevels)
        Me.NotifyCount(access) = Me.NotifyCount(ResourceAccessLevels.Cache) + Me._notifyInterval
    End Sub

#End Region

#Region " CONTROL METHODS "

    ''' <summary>
    ''' Clears the buffer of readings.
    ''' </summary>
    Public Sub ClearBuffer()

        Me._actualPointsCount = 0
        MyBase.Execute(Syntax.ClearCommand)

    End Sub

    ''' <summary>
    ''' Gets or sets the default trace size for reading ASCII data.
    ''' </summary>
    Public Property DefaultTraceSize() As Integer

    ''' <summary>
    ''' Addresses the instrument to talk and send all the readings stored in the data store (buffer) to 
    ''' the computer.
    ''' </summary>
    Public Function ReadBuffer() As String

        Dim defaultStringSize As Integer = MyBase.Controller.DefaultStringSize
        If Me._defaultTraceSize > defaultStringSize Then
            MyBase.Controller.DefaultStringSize = Me._defaultTraceSize
        End If


        Dim text As New System.Text.StringBuilder
        MyBase.Execute("DATA?")
        Do
            text.Append(MyBase.Controller.ReadLine())
        Loop Until (MyBase.Controller.SerialPoll And ServiceRequests.MessageAvailable) = 0

        ' restore the default string size.
        If Me._defaultTraceSize > defaultStringSize Then
            MyBase.Controller.DefaultStringSize = defaultStringSize
        End If

        Return text.ToString

    End Function


#End Region

End Class
