''' <summary>
''' Defines the CALC2 SCPI subsystem.
''' </summary>
''' <license>
''' (c) 2001 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/08/2001" by="David" revision="1.0.646.x">
''' Created
''' </history>
''' <history date="03/25/2008" by="David" revision="5.0.3004.x">
'''  Port to new SCPI library.
''' </history>
Public Class Calculate2Subsystem

    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="controller">Reference to an open <see cref="Scpi.IDevice">SCPI instrument</see>.</param>
    Public Sub New(ByVal controller As Scpi.IDevice)

        MyBase.new("CALC2", controller)

        ' create a new instance of the limit collection
        Me._limits = New ResettableKeyableCollection(Of CalculateLimit)

        Me._compositeLimitsAutoClearEnabled = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._compositeLimitsAutoClearEnabled)
        Me._compositeLimitsFailureBits = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._compositeLimitsFailureBits)
        Me._feedSource = New ResettableValue(Of FeedSource)
        MyBase.ResettableValues.Add(Me._feedSource)
        Me._gradingControlMode = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._gradingControlMode)
        Me._immediateBinning = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._immediateBinning)

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        MyBase.ClearExecutionState()
        Return Me._limits.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        MyBase.PresetKnownState()
        Return Me._limits.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Preset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        MyBase.ResetKnownState()
        Return Me._limits.ResetKnownState()
    End Function

#End Region

#Region " LIMITS "

    Private _activeLimit As CalculateLimit
    ''' <summary>
    ''' Gets or sets reference to the 
    ''' <see cref="CalculateLimit">limit</see>.
    ''' </summary>
    Public Property ActiveLimit() As CalculateLimit
        Get
            Return Me._activeLimit
        End Get
        Set(ByVal value As CalculateLimit)
            Me._activeLimit = value
        End Set
    End Property

    ''' <summary>
    ''' Adds a <see cref="CalculateLimit">limit</see> to the collection of limits.
    ''' Makes the limits the <see cref="ActiveLimit">active limit.</see>
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the header that is used when addressing 
    ''' the instrument using the SCPI commands.</param>
    Public Function AddLimit(ByVal syntaxHeader As String) As CalculateLimit

        Me._activeLimit = Me._limits.Add(New CalculateLimit(syntaxHeader))
        Return Me._activeLimit

    End Function

    Private _limits As ResettableKeyableCollection(Of CalculateLimit)
    ''' <summary>
    ''' Gets reference to the collection of calculation limits
    ''' </summary>
    Public ReadOnly Property Limits() As ResettableKeyableCollection(Of CalculateLimit)
        Get
            Return Me._limits
        End Get
    End Property

#End Region

#Region " ACTIVE LIMIT "

    ''' <summary>
    ''' Gets or sets the compliance limit fail output pattern.
    ''' Make the compliance limit the active limit before setting or getting this property.
    ''' </summary>
    Public Property ComplianceFailureBits(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(ActiveLimit.SyntaxHeader, Syntax.Source2Command, ActiveLimit.ComplianceBits, access)
            Return ActiveLimit.ComplianceBits.Value.Value
        End Get
        Set(ByVal Value As Integer)
            ActiveLimit.ComplianceBits = MyBase.Setter(ActiveLimit.SyntaxHeader, Syntax.Source2Command, ActiveLimit.ComplianceBits, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the compliance failure condition as IN or OUT.
    ''' Make the compliance limit the active limit before setting or getting this property.
    ''' </summary>
    Public Property IncomplianceFailureCondition(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(ActiveLimit.SyntaxHeader, Syntax.FailCommand, ActiveLimit.IncomplianceFailCondition, access)
            Return ActiveLimit.IncomplianceFailCondition.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            ActiveLimit.IncomplianceFailCondition = MyBase.Setter(ActiveLimit.SyntaxHeader, Syntax.FailCommand,
                                                                  ActiveLimit.IncomplianceFailCondition, Value,
                                                                  BooleanDataFormat.InOut, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets the failure state of the active limit.
    ''' </summary>
    Public ReadOnly Property LimitFailed(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(ActiveLimit.SyntaxHeader, Syntax.FailCommand, ActiveLimit.LimitFailed, access)
            Return ActiveLimit.LimitFailed.Value.Value
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the pass output pattern of the active limit.
    ''' </summary>
    Public Property LimitPassBits(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(ActiveLimit.SyntaxHeader, "PASS:SOUR2", ActiveLimit.PassBits, access)
            Return ActiveLimit.PassBits.Value.Value
        End Get
        Set(ByVal Value As Integer)
            ActiveLimit.PassBits = MyBase.Setter(ActiveLimit.SyntaxHeader, "PASS:SOUR2", ActiveLimit.PassBits, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the on/off state of the active limit.
    ''' </summary>
    Public Property LimitState(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(ActiveLimit.SyntaxHeader, "STAT", ActiveLimit.State, access)
            Return ActiveLimit.State.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            ActiveLimit.State = MyBase.Setter(ActiveLimit.SyntaxHeader, "STAT", ActiveLimit.State, Value, BooleanDataFormat.OneZero, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the lower level of the active limit.
    ''' </summary>
    Public Property LowerLimit(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(ActiveLimit.SyntaxHeader, "LOW", ActiveLimit.LowerLimit, access)
            Return ActiveLimit.LowerLimit.Value.Value
        End Get
        Set(ByVal Value As Double)
            ActiveLimit.LowerLimit = MyBase.Setter(ActiveLimit.SyntaxHeader, "LOW", ActiveLimit.LowerLimit, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the lower limit fail output pattern of of the active limit.
    ''' </summary>
    Public Property LowerLevelFailureBits(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(ActiveLimit.SyntaxHeader, "LOW:SOUR2", ActiveLimit.LowerLimitFailureBits, access)
            Return ActiveLimit.LowerLimitFailureBits.Value.Value
        End Get
        Set(ByVal Value As Integer)
            ActiveLimit.LowerLimitFailureBits = MyBase.Setter(ActiveLimit.SyntaxHeader, "LOW:SOUR2", ActiveLimit.LowerLimitFailureBits, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the upper limit fail output pattern of the active limit.
    ''' </summary>
    Public Property UpperLevelFailureBits(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(ActiveLimit.SyntaxHeader, "UPP:SOUR2", ActiveLimit.UpperLimitFailureBits, access)
            Return ActiveLimit.UpperLimitFailureBits.Value.Value
        End Get
        Set(ByVal Value As Integer)
            ActiveLimit.UpperLimitFailureBits = MyBase.Setter(ActiveLimit.SyntaxHeader, "UPP:SOUR2", ActiveLimit.UpperLimitFailureBits, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the active limit upper level.
    ''' </summary>
    Public Property UpperLimit(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(ActiveLimit.SyntaxHeader, "UPP", ActiveLimit.UpperLimit, access)
            Return ActiveLimit.UpperLimit.Value.Value
        End Get
        Set(ByVal Value As Double)
            ActiveLimit.UpperLimit = MyBase.Setter(ActiveLimit.SyntaxHeader, "UPP", ActiveLimit.UpperLimit, Value, access)
        End Set
    End Property

#End Region

#Region " CALC 2 "

    Private _feedSource As ResettableValue(Of FeedSource)
    ''' <summary>
    ''' Gets the feed source.
    ''' </summary>
    ''' <value>The feed source.</value>
    Public ReadOnly Property FeedSource() As ResettableValue(Of FeedSource)
        Get
            Return Me._feedSource
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the source of readings such as 
    ''' <see cref="Syntax.Sense">sense</see> or 
    ''' <see cref="Syntax.Calculate1">calculate 1</see> or <see cref="Syntax.Calculate2">calculate2</see>
    ''' </summary>
    Public Property FeedSource(ByVal access As ResourceAccessLevels) As FeedSource
        Get
            Me._feedSource.Value = Syntax.ParseFeedSource(MyBase.Getter(Syntax.Feed,
                                                                     Syntax.ExtractScpi(Me._feedSource.Value),
                                                                     access))
            Me.FeedSource.ActualValue = Me.FeedSource.Value.Value
            Return Me.FeedSource.Value.Value
        End Get
        Set(ByVal Value As FeedSource)
            Me._feedSource.Value = Syntax.ParseFeedSource(MyBase.Setter(Syntax.Feed,
                                                       Syntax.ExtractScpi(Me._feedSource.Value),
                                                       Syntax.ExtractScpi(Value), access))
        End Set
    End Property

    ''' <summary>
    ''' Return the average of the buffer contents.
    ''' </summary>
    Public Function CalculateBufferAverage() As Double

        ' select average
        MyBase.Setter(Syntax.Format, Syntax.Mean, False)

        ' turn status on.
        MyBase.Setter(Syntax.Status, Syntax.On, False)

        ' do the calculation.
        MyBase.Execute(Syntax.Immediate)

        ' get the result
        Dim value As Nullable(Of Double) = MyBase.QueryDouble(Syntax.DataCommand)
        If value IsNot Nothing AndAlso value.HasValue Then
            Return value.Value
        Else
            Return 0
        End If

    End Function

#End Region

#Region " COMPOSITE LIMITS "

    Private _compositeLimitsAutoClearEnabled As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the composite limits auto clear enabled.
    ''' </summary>
    ''' <value>The composite limits auto clear enabled.</value>
    Public ReadOnly Property CompositeLimitsAutoClearEnabled() As ResettableValue(Of Boolean)
        Get
            Return Me._compositeLimitsAutoClearEnabled
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the enabled condition for automatically clearing the limit.
    ''' </summary>
    ''' <param name="access">Specifies if the property updates the device, uses a stored value, 
    ''' and/or verified by reading back the device value.</param>
    Public Property CompositeLimitsAutoClearEnabled(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter("CLIM", "CLE:AUTO", Me._compositeLimitsAutoClearEnabled, access)
        End Get
        Set(ByVal Value As Boolean)
            Me._compositeLimitsAutoClearEnabled = MyBase.Setter("CLIM", "CLE:AUTO", Me._compositeLimitsAutoClearEnabled, Value, BooleanDataFormat.OneZero, access)
        End Set
    End Property

    Private _compositeLimitsFailureBits As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the composite limits failure bits.
    ''' </summary>
    ''' <value>The composite limits failure bits.</value>
    Public ReadOnly Property CompositeLimitsFailureBits() As ResettableValue(Of Integer)
        Get
            Return Me._compositeLimitsFailureBits
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the fail output pattern of the active limit.
    ''' </summary>
    Public Property CompositeLimitsFailureBits(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter("CLIM", "FAIL:SOUR2", Me._compositeLimitsFailureBits, access)
        End Get
        Set(ByVal Value As Integer)
            Me._compositeLimitsFailureBits = MyBase.Setter("CLIM", "FAIL:SOUR2", Me._compositeLimitsFailureBits, Value, access)
        End Set
    End Property

    Private _immediateBinning As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the immediate binning.
    ''' </summary>
    ''' <value>The immediate binning.</value>
    Public ReadOnly Property ImmediateBinning() As ResettableValue(Of Boolean)
        Get
            Return Me._immediateBinning
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the binning control.
    ''' Determines when the binning information is sent to the handler. 
    ''' A limit test is performed (IMMediate) or after a sweep, list, or memory sequence (END)
    ''' </summary>
    Public Property ImmediateBinning(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter("CLIM", "BCON", Me._immediateBinning, access)
            Return Me._immediateBinning.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            Me._immediateBinning = MyBase.Setter("CLIM", "BCON", Me._immediateBinning, Value, BooleanDataFormat.ImmediateEnd, access)
        End Set
    End Property

    Private _gradingControlMode As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the grading control mode.
    ''' </summary>
    ''' <value>The grading control mode.</value>
    Public ReadOnly Property GradingControlMode() As ResettableValue(Of Boolean)
        Get
            Return Me._gradingControlMode
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the limit control mode.
    ''' Sets how limit results control digital I/O lines: GRADing or SORTing.
    ''' </summary>
    Public Property GradingControlMode(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter("CLIM", "MODE", Me._gradingControlMode, access)
            Return Me._gradingControlMode.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            Me._gradingControlMode = MyBase.Setter("CLIM", "MODE", Me._gradingControlMode, Value, BooleanDataFormat.GradingSorting, access)
        End Set
    End Property

    ''' <summary>
    ''' Clears composite limits.
    ''' Returns the instrument output to the TTL settings per SOURC2:TTL
    ''' </summary>
    Public Sub ClearCompositeLimits()

        MyBase.Controller.WriteLine(":" & MyBase.Name & ":CLIM:CLE", True, True)

    End Sub

#End Region

End Class

