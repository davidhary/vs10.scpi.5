''' <summary>Defines a SCPI System Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class SystemSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="controller">Reference to an open <see cref="Scpi.IDevice">SCPI instrument</see>.</param>
    Public Sub New(ByVal controller As Scpi.IDevice)

        ' instantiate the base class
        MyBase.New(SystemSubsystem._scpiName, controller)

        Me._autoZero = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._autoZero)
        Me._beeperEnabled = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._beeperEnabled)
        Me._cableGuard = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._cableGuard)
        Me._contactCheckEnabled = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._contactCheckEnabled)
        Me._contactCheckResistance = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._contactCheckResistance)
        Me._isRemote = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._isRemote)
        Me._remoteSenseMode = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._remoteSenseMode)

        Me._isRemote.ResetValue = True
        Me._isRemote.ClearValue = True
        Me._isRemote.PresetValue = True

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " SHARED "

    Private Shared _scpiName As String = "SYST"
    ''' <summary>Clears the messages from the error queue.</summary>
    ''' <param name="controller">Reference to a <see cref="Scpi.IDevice">SCPI controller</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub ClearErrorQueue(ByVal controller As Scpi.IDevice)

        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If
        controller.WriteLine(Syntax.BuildExecute(SystemSubsystem._scpiName, Syntax.ClearCommand))

    End Sub

    ''' <summary>Gets or sets the status of the front (True) rear (switch)</summary>
    ''' <param name="controller">Reference to a <see cref="Scpi.IDevice">SCPI controller</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared ReadOnly Property FrontSwitched(ByVal controller As Scpi.IDevice) As Boolean
        Get
            If controller Is Nothing Then
                Throw New ArgumentNullException("controller")
            End If
            Return controller.QueryBoolean(Syntax.BuildQuery(SystemSubsystem._scpiName, Syntax.FrontSwitchedCommand)).Value
        End Get
    End Property

    ''' <summary>Initializes battery backed RAM. This initializes trace, source list, 
    '''   user-defined math, source-memory locations, standard save setups, and all 
    '''   call math expressions.</summary>
    ''' <param name="controller">Reference to a <see cref="Scpi.IDevice">SCPI controller</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub InitializeMemory(ByVal controller As Scpi.IDevice)

        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If
        controller.WriteLine(Syntax.BuildExecute(SystemSubsystem._scpiName, "MEM", Syntax.InitCommand))

    End Sub

    ''' <summary>Returns the instrument to states optimized for front panel operations.</summary>
    ''' <param name="controller">Reference to a <see cref="Scpi.IDevice">SCPI controller</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Sub Preset(ByVal controller As Scpi.IDevice)

        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If
        controller.WriteLine(Syntax.BuildExecute(SystemSubsystem._scpiName, Syntax.PresetCommand))

    End Sub

    ''' <summary>Returns the version level of the SCPI standard implemented by the instrument..</summary>
    ''' <param name="controller">Reference to a <see cref="Scpi.IDevice">SCPI controller</see></param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Shared Function ReadScpiRevision(ByVal controller As Scpi.IDevice) As Double
        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If
        Return controller.QueryDouble(Syntax.BuildQuery(SystemSubsystem._scpiName, Syntax.VersionCommand)).Value
    End Function

#End Region

#Region " SERIAL COMM "

    ''' <summary>
    ''' Returns a command string to send to the 2182 instrument.
    ''' </summary>
    ''' <param name="remoteCommand">The command which to send to the remote instrument.</param>
    Private Shared Function buildSerialSendQueryCommand(ByVal remoteCommand As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0} ""{1}""", SerialSendCommand, remoteCommand)
    End Function

    Public Const SerialSendCommand As String = ":SYST:COMM:SER:SEND"
    Public Const SerialEnterCommand As String = ":SYST:COMM:SER:ENT?"

    ''' <summary>
    ''' Wait for serial operation to complete and return true if '1' was received.
    ''' </summary>
    Public Function SerialQueryOperationCompleted() As Boolean

        Return Me.SerialQuery(Scpi.Syntax.OperationCompletedQueryCommand, 200, 2) = "1"

    End Function

    Private _serialCommTimedOut As Boolean
    ''' <summary>
    ''' Get the time out condition of the last query.
    ''' </summary>
    Public ReadOnly Property SerialCommTimedOut() As Boolean
        Get
            Return Me._serialCommTimedOut
        End Get
    End Property

    Private _lastSerialMessageReceived As String
    ''' <summary>
    ''' Last message received from the remote instrument.
    ''' </summary>
    Public ReadOnly Property LastSerialMessageReceived() As String
        Get
            Return Me._lastSerialMessageReceived
        End Get
    End Property

    Private _lastSerialMessageSent As String
    ''' <summary>
    ''' Last message send from the remote instrument.
    ''' </summary>
    Public ReadOnly Property LastSerialMessageSent() As String
        Get
            Return Me._lastSerialMessageSent
        End Get
    End Property

    ''' <summary>
    ''' Queries the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="remoteCommand">The command which to send to the remote instrument.</param>
    ''' <param name="delayMilliseconds">The time to wait from sending the command until
    ''' expecting it to be present on the local instrument bus.</param>
    ''' <param name="timeoutSeconds">Maximum time to look for a reply from the instrument.</param>
    Public Function SerialQuery(ByVal remoteCommand As String,
                                       ByVal delayMilliseconds As Integer,
                                       ByVal timeoutSeconds As Double) As String

        ' Send query to the remote instrument.
        SystemSubsystem.SerialSend(Controller, remoteCommand)

        ' receive the reply from the instrument.
        Return Me.SerialReceive(delayMilliseconds, timeoutSeconds)

    End Function

    ''' <summary>
    ''' Queries the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="controller"></param>
    ''' <param name="remoteCommand">The command which to send to the remote instrument.</param>
    ''' <param name="delayMilliseconds">The time to wait from sending the command until
    ''' expecting it to be present on the local instrument bus.</param>
    Public Shared Function SerialQuery(ByVal controller As Scpi.IDevice,
                                       ByVal remoteCommand As String,
                                       ByVal delayMilliseconds As Integer) As String
        SystemSubsystem.SerialSend(controller, remoteCommand)
        Return SystemSubsystem.SerialRequestLineTrimEnd(controller, delayMilliseconds)
    End Function

    ''' <summary>
    ''' Reads all the messages left on the serial buffer.
    ''' </summary>
    ''' <param name="controller">The controller.</param>
    ''' <returns>System.String.</returns>
    ''' <exception cref="System.ArgumentNullException">controller</exception>
    Public Shared Function SerialFlush(ByVal controller As Scpi.IDevice) As String

        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If

        Dim receivedMessage As String = ""
        Dim buffer As New System.Text.StringBuilder

        SystemSubsystem.SerialSend(controller, "*OPC?")
        Threading.Thread.Sleep(200)
        SystemSubsystem.SerialRequestBuffer(controller)
        Dim endTime As Date = DateTime.Now.AddMilliseconds(20000)
        Do
            receivedMessage = controller.ReadLineTrimEnd()
            buffer.AppendLine(receivedMessage)
        Loop Until receivedMessage = "1" OrElse DateTime.Now > endTime
        Try
            controller.StoreTimeout(100)
            ' Keithley 6221:
            ' Serial communication leaves a LF character on the bus after responding to a request for data.
            ' The Message Available Bits are not set.
            Dim lf As String = ""
            endTime = DateTime.Now.AddMilliseconds(100)
            Do Until Not String.IsNullOrWhiteSpace(lf) OrElse endTime < DateTime.Now
                lf = controller.ReadLine
                System.Windows.Forms.Application.DoEvents()
            Loop
        Catch
        Finally
            controller.RestoreTimeout()
        End Try

        Return buffer.ToString

    End Function

    ''' <summary>
    ''' Reads a message from the remote instrument by way of serial communication.
    ''' An enter command must be send prior to sending this message.
    ''' </summary>
    ''' <param name="controller">The controller.</param>
    ''' <returns>System.String.</returns>
    ''' <exception cref="System.ArgumentNullException">controller</exception>
    Public Shared Function SerialReadLineTrimEnd(ByVal controller As Scpi.IDevice) As String

        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If

        Dim receivedMessage As String = ""
        If (controller.SerialPoll And ServiceRequests.MessageAvailable) <> 0 Then

            ' get the message.
            receivedMessage = controller.ReadLineTrimEnd()

        End If

        Try
            controller.StoreTimeout(100)
            ' Keithley 6221:
            ' Serial communication leaves a LF character on the bus after responding to a request for data.
            ' The Message Available Bits are not set.
            Dim lf As String = ""
            Dim endTime As DateTime = DateTime.Now.AddMilliseconds(100)
            Do Until Not String.IsNullOrWhiteSpace(lf) OrElse endTime < DateTime.Now
                lf = controller.ReadLine
                System.Windows.Forms.Application.DoEvents()
            Loop
        Catch
        Finally
            controller.RestoreTimeout()
        End Try


        Return receivedMessage

    End Function

    ''' <summary>
    ''' Receives a message.
    ''' </summary>
    ''' <param name="delayMilliseconds">The time to wait between requests of data.</param>
    ''' <remarks>
    ''' Waits till timeout or data received.  If data received, accumulate data until no data is available.
    ''' </remarks>
    Public Function SerialReceive(ByVal delayMilliseconds As Integer, ByVal timeoutSeconds As Double) As String

        ' initialize monitoring values
        Me._lastSerialMessageReceived = String.Empty
        Me._serialCommTimedOut = False

        ' mark time to wait for timeout
        Dim endTime As Date = DateTime.Now.AddSeconds(timeoutSeconds)

        ' loop until we have a value or timeout
        Do

            ' wait some time for the remote instrument to prepare a reply.
            Threading.Thread.Sleep(delayMilliseconds)

            ' read any reply from the remote instrument.
            Me._lastSerialMessageReceived = SystemSubsystem.SerialRequestLineTrimEnd(Controller, delayMilliseconds)

            Me._serialCommTimedOut = String.IsNullOrWhiteSpace(Me._lastSerialMessageReceived) AndAlso DateTime.Now > endTime

        Loop Until Not String.IsNullOrWhiteSpace(Me._lastSerialMessageReceived) OrElse Me._serialCommTimedOut

        If Me._serialCommTimedOut Then
            Return Me._lastSerialMessageReceived
        End If

        Dim receivedBuffer As New System.Text.StringBuilder
        receivedBuffer.Append(Me._lastSerialMessageReceived)

        ' loop until we no longer get a value from the device
        Do

            ' wait some time for the remote instrument to prepare a reply.
            Threading.Thread.Sleep(delayMilliseconds)

            ' read any reply from the remote instrument.
            Me._lastSerialMessageReceived = SystemSubsystem.SerialRequestLineTrimEnd(Controller, delayMilliseconds)

            If Not String.IsNullOrWhiteSpace(Me._lastSerialMessageReceived) Then
                receivedBuffer.Append(Me._lastSerialMessageReceived)
            End If

            ' exit if the message is empty.
        Loop Until String.IsNullOrWhiteSpace(Me._lastSerialMessageReceived)

        ' save the entire message.
        Me._lastSerialMessageReceived = receivedBuffer.ToString

        Return Me._lastSerialMessageReceived

    End Function

    ''' <summary>
    ''' Request data from the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="controller">The controller.</param>
    ''' <exception cref="System.ArgumentNullException">controller</exception>
    Public Shared Sub SerialRequestBuffer(ByVal controller As Scpi.IDevice)

        ' command the serial instrument to send the message.
        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If
        controller.WriteLine(SerialEnterCommand)

    End Sub

    ''' <summary>
    ''' Request and read data in the buffer queue of the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="controller"></param>
    Public Shared Function SerialRequestLineTrimEnd(ByVal controller As Scpi.IDevice) As String

        ' issue a request for data already in the buffer queue.
        SystemSubsystem.SerialRequestBuffer(controller)

        ' read any reply from the remote instrument.
        Return SystemSubsystem.SerialReadLineTrimEnd(controller)

    End Function

    ''' <summary>
    ''' Request and read data in the buffer queue of the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="controller"></param>
    ''' <param name="delayMilliseconds">The time to wait between requests of data.</param>
    Public Shared Function SerialRequestLineTrimEnd(ByVal controller As Scpi.IDevice, ByVal delayMilliseconds As Integer) As String

        ' issue a request for data already in the buffer queue.
        SystemSubsystem.SerialRequestBuffer(controller)

        ' wait some time for the request to reach destination and the remote instrument prepare the reply.
        Threading.Thread.Sleep(delayMilliseconds)

        ' read any reply from the remote instrument.
        Return SystemSubsystem.SerialReadLineTrimEnd(controller)

    End Function

    ''' <summary>
    ''' Sends a message to the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="controller">The controller.</param>
    ''' <param name="remoteCommand">The command which to send to the remote instrument.</param>
    ''' <exception cref="System.ArgumentNullException">controller</exception>
    Public Shared Sub SerialSend(ByVal controller As Scpi.IDevice, ByVal remoteCommand As String)
        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If
        Dim queryCommand As String = SystemSubsystem.buildSerialSendQueryCommand(remoteCommand)
        controller.WriteLine(queryCommand)
    End Sub

    ''' <summary>
    ''' Sends a message to the remote instrument by way of serial communication.
    ''' </summary>
    ''' <param name="remoteCommand">The command which to send to the remote instrument.</param>
    Public Sub SerialSend(ByVal remoteCommand As String)
        Me._lastSerialMessageSent = remoteCommand
        SystemSubsystem.SerialSend(MyBase.Controller, remoteCommand)
    End Sub

#End Region

#Region " SHARED ERROR "

    ''' <summary>
    ''' Returns the last error from the instrument.
    ''' </summary>
    ''' <param name="controller">Reference to a <see cref="Scpi.IDevice">SCPI controller</see></param>
    ''' <returns>System.String.</returns>
    ''' <exception cref="System.ArgumentNullException">controller</exception>
    ''' <exception cref="ArgumentNullException"></exception>
    Public Shared Function ReadLastError(ByVal controller As Scpi.IDevice) As String

        If controller Is Nothing Then
            Throw New ArgumentNullException("controller")
        End If
        Return controller.QueryTrimEnd(":SYST:ERR?")
    End Function

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        Return MyBase.ClearExecutionState()
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        Return MyBase.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        Return MyBase.ResetKnownState()
    End Function

#End Region

#Region " PASSWORD "

    Private _passwordLocked As Nullable(Of Boolean)
    ''' <summary>Gets or sets the condition for auto zero is enabled.</summary>
    Public ReadOnly Property PasswordLocked(ByVal access As ResourceAccessLevels) As Boolean
        Get
            Me._passwordLocked = MyBase.Getter(Syntax.PasswordModalityName, Syntax.LockCommand, Me._passwordLocked, access)
            Return Me._passwordLocked.Value
        End Get
    End Property

    ''' <summary>Unlocks the instrument using the provided password.</summary>
    ''' <param name="password"></param>
    ''' <returns>True if unlocked.</returns>
    Public Function UnlockPassword(ByVal password As String, ByVal access As ResourceAccessLevels) As Boolean
        MyBase.Controller.WriteLine(Syntax.BuildCommand(
                                SystemSubsystem._scpiName, Syntax.PasswordModalityName, Syntax.UnlockCommand,
                                String.Format(Globalization.CultureInfo.CurrentCulture, """{0}""", password)))
        Me._passwordLocked = MyBase.Getter(Syntax.PasswordModalityName, Syntax.LockCommand, New Nullable(Of Boolean), access)
        Return Not Me._passwordLocked.Value
    End Function

#End Region

#Region " BEEPER "

    Private _beeperEnabled As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the beeper enabled.
    ''' </summary>
    ''' <value>The beeper enabled.</value>
    Public ReadOnly Property BeeperEnabled() As ResettableValue(Of Boolean)
        Get
            Return Me._beeperEnabled
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the Beeper enabled state.
    ''' </summary>
    Public Property BeeperEnabled(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter("BEEP:STAT", Me._beeperEnabled, access)
            Return Me._beeperEnabled.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            Me._beeperEnabled = MyBase.Setter("BEEP:STAT", Me._beeperEnabled, Value, BooleanDataFormat.OneZero, access)
        End Set
    End Property

    ''' <summary>
    ''' Commands the instrument to issue a Beep on the instrument.
    ''' </summary>
    ''' <param name="frequency">Specifies the frequency of the beep</param>
    ''' <param name="duration">Specifies the duration of the beep.</param>
    Public Sub BeepImmediately(ByVal frequency As Integer, ByVal duration As Single)

        MyBase.Controller.WriteLine("{0}:BEEP:IMM {1}, {2}", MyBase.Name, frequency, duration)

    End Sub

#End Region

#Region " CARDS "

    ''' <summary>
    ''' Gets a list of cards.
    ''' </summary>
    Public Function EnumerateCards() As String()
        Dim cardList As String = MyBase.Controller.QueryTrimEnd("*OPT?")
        If String.IsNullOrWhiteSpace(cardList) Then
            Return New String() {}
        Else
            Return cardList.Split(","c)
        End If
    End Function

    ''' <summary>
    ''' Return the number of analog outputs on the specified slot.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardAnalogOuts(ByVal slotNumber As Integer) As Integer
        Dim analogOuts As String = MyBase.Controller.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture,
              "SYST:CARD{0}:AOUT?", slotNumber))
        Dim cardAnalogOuts As Integer = 0
        If Not String.IsNullOrWhiteSpace(analogOuts) AndAlso Not Integer.TryParse(analogOuts, Globalization.NumberStyles.Integer,
                                Globalization.CultureInfo.InvariantCulture, cardAnalogOuts) Then
            Return 0
        End If
        Return cardAnalogOuts
    End Function

    ''' <summary>
    ''' Returns the firmware version of the card located in the 
    ''' specified <paramref name="slotNumber">slot</paramref>.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardFirmwareVersion(ByVal slotNumber As Integer) As String
        Return MyBase.Controller.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture,
              "SYST:CARD{0}:SWR?", slotNumber))
    End Function

    ''' <summary>
    ''' Gets the card calibration count.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardCalCount(ByVal slotNumber As Integer) As Nullable(Of Int32)
        Return MyBase.Controller.QueryInteger(String.Format(Globalization.CultureInfo.CurrentCulture,
            "CAL:PROT:CARD{0}:COUNT?", slotNumber))
    End Function

    ''' <summary>
    ''' Gets the card calibration date.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardCalDate(ByVal slotNumber As Integer) As String
        Return MyBase.Controller.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture,
            "CAL:PROT:CARD{0}:DATE?", slotNumber)).Replace(",", "/")
    End Function

    ''' <summary>
    ''' Returns the serial number of the card located in the 
    ''' specified <paramref name="slotNumber">slot</paramref>.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardSerialNumber(ByVal slotNumber As Integer) As String
        Return MyBase.Controller.QueryTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture,
              "SYST:CARD{0}:SNUM?", slotNumber))
    End Function

    ''' <summary>
    ''' Returns true if card has thermocouple compensation.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    Public Function ReadCardTempCompensated(ByVal slotNumber As Integer) As Boolean
        Return MyBase.Controller.QueryBoolean(String.Format(Globalization.CultureInfo.CurrentCulture,
              "SYST:CARD{0}:TCOM?", slotNumber)).Value
    End Function

    ''' <summary>
    ''' Sets a pseudo card for the specific slot.
    ''' </summary>
    ''' <param name="slotNumber"></param>
    ''' <param name="cardName"></param>
    Public Sub WritePseudoCard(ByVal slotNumber As Integer, ByVal cardName As String)
        MyBase.Controller.WriteLine(String.Format(Globalization.CultureInfo.CurrentCulture,
              "SYST:PCAR{0} {1}", slotNumber, cardName))
    End Sub

#End Region

#Region " CONTACT CHECK "

    Private _contactCheckEnabled As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the contact check enabled.
    ''' </summary>
    ''' <value>The contact check enabled.</value>
    Public ReadOnly Property ContactCheckEnabled() As ResettableValue(Of Boolean)
        Get
            Return Me._contactCheckEnabled
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the Contact check enabled state.
    ''' </summary>
    Public Property ContactCheckEnabled(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.ContactCheck, Me._contactCheckEnabled, access)
            Return Me._contactCheckEnabled.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            Me._contactCheckEnabled = MyBase.Setter(Syntax.ContactCheck, Me._contactCheckEnabled, Value, BooleanDataFormat.OneZero, access)
        End Set
    End Property

    Private _contactCheckResistance As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the contact check resistance.
    ''' </summary>
    ''' <value>The contact check resistance.</value>
    Public ReadOnly Property ContactCheckResistance() As ResettableValue(Of Integer)
        Get
            Return Me._contactCheckResistance
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the contact check resistance.
    ''' </summary>
    Public Property ContactCheckResistance(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(Syntax.ContactCheck, Syntax.Resistance, Me._contactCheckResistance, access)
            Return Me._contactCheckResistance.Value.Value
        End Get
        Set(ByVal Value As Integer)
            Me._contactCheckResistance = MyBase.Setter(Syntax.ContactCheck, Syntax.Resistance, Me._contactCheckResistance, Value, access)
        End Set
    End Property

#End Region

#Region " CONTROL "

    Private _isRemote As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Places or takes out the instrument of remote. Non-GPIB Only.
    ''' </summary>
    Public Property Remote() As Boolean
        Get
            Return Me._isRemote.Value.Value
        End Get
        Set(ByVal value As Boolean)
            Me._isRemote.Value = value
            If value Then
                MyBase.Controller.WriteLine(":SYST:REM")
            Else
                MyBase.Controller.WriteLine(":SYST:LOC")
            End If
        End Set
    End Property

    ''' <summary>
    ''' Lock out the front panel.  Non-GPIB.
    ''' </summary>
    Public Sub LocalLockout()
        MyBase.Controller.WriteLine(":SYST:RWL")
    End Sub

#End Region

#Region " ERROR MANAGEMENT "

    ''' <summary>Clears the messages from the error queue.</summary>
    Public Sub ClearErrorQueue()
        SystemSubsystem.ClearErrorQueue(Controller)
    End Sub

    Private _lastError As String = String.Empty
    ''' <summary>
    ''' Gets the last error (cached).
    ''' </summary>
    Public ReadOnly Property LastError(ByVal access As ResourceAccessLevels) As String
        Get
            If Subsystem.IsDeviceAccess(access) OrElse String.IsNullOrWhiteSpace(Me._lastError) Then
                Me._lastError = Me.ReadLastError()
            End If
            Return Me._lastError
        End Get
    End Property

    ''' <summary>Returns the last error from the instrument.</summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Function ReadLastError() As String
        Return SystemSubsystem.ReadLastError(MyBase.Controller)
    End Function

#End Region

#Region " PROPERTIES "

    Private _autoZero As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the auto zero.
    ''' </summary>
    ''' <value>The auto zero.</value>
    Public ReadOnly Property AutoZero() As ResettableValue(Of Boolean)
        Get
            Return Me._autoZero
        End Get
    End Property

    ''' <summary>Gets or sets the condition for auto zero is enabled.</summary>
    Public Property AutoZero(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.AutoZeroCommand, Me._autoZero, access)
            Return Me._autoZero.Value.Value
        End Get
        Set(ByVal value As Boolean)
            Me._autoZero = MyBase.Setter(Syntax.AutoZeroCommand, Me._autoZero, value, BooleanDataFormat.OnOff, access)
        End Set
    End Property

    Private _cableGuard As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the cable guard.
    ''' </summary>
    ''' <value>The cable guard.</value>
    Public ReadOnly Property CableGuard() As ResettableValue(Of Boolean)
        Get
            Return Me._cableGuard
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the guard type as <see cref="Syntax.Cable">Cable</see> (true)
    ''' or <see cref="Syntax.Ohms">OHMS</see> (false).
    ''' </summary>
    Public Property CableGuard(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.Guard, Me._cableGuard, access)
            Return Me._cableGuard.Value.Value
        End Get
        Set(ByVal Value As Boolean)
            Me._cableGuard = MyBase.Setter(Syntax.Guard, Me._cableGuard, Value, BooleanDataFormat.CableOhms, access)
        End Set
    End Property

    Private _frontSwitchedCache As Nullable(Of Boolean)
    ''' <summary>
    ''' Gets or sets the cached value of the front switched state.
    ''' </summary>
    ''' <value>
    ''' The front switched cached.
    ''' </value>
    Public Property FrontSwitchedCache() As Nullable(Of Boolean)
        Get
            Return Me._frontSwitchedCache
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            Me._frontSwitchedCache = value
        End Set
    End Property

    ''' <summary>Gets or sets the status of the front (true) read (false) switch.</summary>
    Public ReadOnly Property FrontSwitched(ByVal access As ResourceAccessLevels) As Boolean
        Get
            Me.FrontSwitchedCache = MyBase.Getter(Syntax.FrontSwitchedCommand, Me._frontSwitchedCache, access)
            Return Me.FrontSwitchedCache.Value
        End Get
    End Property

    Private Shared _lineFrequency As Nullable(Of Integer)
    ''' <summary>Gets or sets the line frequency for this instrument.
    ''' Setting the stored value is useful to address the case when the instrument does not 
    ''' support the line frequency command.
    ''' </summary>
    Public Overloads Property LineFrequency() As Integer
        Get
            If Controller IsNot Nothing AndAlso Not SystemSubsystem._lineFrequency.HasValue Then
                SystemSubsystem._lineFrequency = MyBase.Controller.QueryInteger(Syntax.BuildQuery(SystemSubsystem._scpiName,
                                                                                                  Syntax.LineFrequencyCommand))
            End If
            If Not SystemSubsystem._lineFrequency.HasValue Then
                SystemSubsystem._lineFrequency = 60
            End If
            Return SystemSubsystem._lineFrequency.Value
        End Get
        Set(ByVal value As Integer)
            SystemSubsystem._lineFrequency = value
        End Set
    End Property

    ''' <summary>
    ''' Returns the line frequency.
    ''' </summary>
    Public Shared Function LineFrequencyGetter() As Integer
        If Not SystemSubsystem._lineFrequency.HasValue Then
            SystemSubsystem._lineFrequency = 60
        End If
        Return SystemSubsystem._lineFrequency.Value
    End Function

    ''' <summary>
    ''' sets the line frequency.
    ''' </summary>
    ''' <param name="value">The value.</param>
    Public Shared Sub LineFrequencySetter(ByVal value As Integer)
        SystemSubsystem._lineFrequency = value
    End Sub

    Private _remoteSenseMode As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the remote sense mode.
    ''' </summary>
    ''' <value>The remote sense mode.</value>
    Public ReadOnly Property RemoteSenseMode() As ResettableValue(Of Boolean)
        Get
            Return Me._remoteSenseMode
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the measurement sense mode as four-wire (remote) or
    ''' Two-Wire</summary>
    Public Property RemoteSenseMode(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.RemoteSenseCommand, Me._remoteSenseMode, access)
            Return Me.RemoteSenseMode.Value.Value
        End Get
        Set(ByVal value As Boolean)
            Me._remoteSenseMode = MyBase.Setter(Syntax.RemoteSenseCommand, Me._remoteSenseMode, value, BooleanDataFormat.OnOff, access)
        End Set
    End Property

    Private _scpiRevision As Nullable(Of Double)
    ''' <summary>Returns the version level of the SCPI standard implemented by the instrument..</summary>
    Public ReadOnly Property ScpiRevision(ByVal access As ResourceAccessLevels) As Double
        Get
            Me._scpiRevision = MyBase.Getter(Syntax.VersionCommand, Me._scpiRevision, access)
            Return Me._scpiRevision.Value
        End Get
    End Property

#End Region

End Class

