''' <summary>Defines a SCPI Source Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class SourceSubsystem
    Inherits Subsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="controller">Reference to an open <see cref="Scpi.IDevice">SCPI instrument</see>.</param>
    Public Sub New(ByVal controller As Scpi.IDevice)

        ' instantiate the base class
        MyBase.New("SOUR", controller)

        ' create a new function collection
        Me._functionCollection = New ResettableCollection(Of ScpiFunction)

        Me._autoClear = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._autoClear)
        Me._autoDelay = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._autoDelay)
        Me._delay = New ResettableDouble
        MyBase.ResettableValues.Add(Me._delay)
        Me._deltaArm = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._deltaArm)
        Me._deltaCount = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._deltaCount)
        Me._deltaDelay = New ResettableDouble
        MyBase.ResettableValues.Add(Me._deltaDelay)
        Me._deltaHighLevel = New ResettableDouble
        MyBase.ResettableValues.Add(Me._deltaHighLevel)
        Me._deltaAbortOnCompliance = New ResettableValue(Of Boolean)
        MyBase.ResettableValues.Add(Me._deltaAbortOnCompliance)
        Me._functionMode = New ResettableValue(Of SourceFunctionMode)
        MyBase.ResettableValues.Add(Me._functionMode)
        Me._memoryPoints = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._memoryPoints)
        Me._sweepPoints = New ResettableValue(Of Integer)
        MyBase.ResettableValues.Add(Me._sweepPoints)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2215:Dispose methods should call base class dispose")>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called


                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Overrides Function ClearExecutionState() As Boolean
        MyBase.ClearExecutionState()
        Return Me._functionCollection.ClearExecutionState
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Overrides Function PresetKnownState() As Boolean
        MyBase.PresetKnownState()
        Return Me._functionCollection.PresetKnownState
    End Function

    ''' <summary>Restore member properties to the following RST or System Reset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Overrides Function ResetKnownState() As Boolean
        MyBase.ResetKnownState()
        Return Me._functionCollection.ResetKnownState
    End Function

#End Region

#Region " FUNCTIONS "

    ''' <summary>
    ''' Adds a sense function to the collection of sense functions.
    ''' Makes the function the 
    ''' <see cref="ActiveFunction">active function.</see>
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the function header that is used when addressing 
    ''' this instrument function using the SCPI commands.</param>
    ''' <param name="modality">Defines the <see cref="SourceFunctionMode">SCPI function or element</see>.</param>
    Public Function AddFunction(ByVal syntaxHeader As String, ByVal modality As SourceFunctionMode) As ScpiFunction

        Me._activeFunction = FunctionCollection.Add(New ScpiFunction(syntaxHeader,
                                                                  MyBase.Controller.SystemSubsystem.LineFrequency,
                                                                  modality))
        Return Me._activeFunction

    End Function

    Private _functionCollection As ResettableCollection(Of ScpiFunction)
    ''' <summary>
    ''' Gets reference to the collection of sense functions
    ''' </summary>
    Public ReadOnly Property FunctionCollection() As ResettableCollection(Of ScpiFunction)
        Get
            Return Me._functionCollection
        End Get
    End Property

    Private _functionMode As ResettableValue(Of SourceFunctionMode)
    Public ReadOnly Property FunctionMode() As ResettableValue(Of SourceFunctionMode)
        Get
            Return Me._functionMode
        End Get
    End Property
    ''' <summary>Gets or sets the <see cref="SourceFunctionMode">source function mode</see>.</summary>
    ''' <remarks>This method must be set to a non-default value.</remarks>
    Public Property FunctionMode(ByVal access As ResourceAccessLevels) As SourceFunctionMode
        Get
            Me._functionMode.Value = Syntax.ParseSourceFunctionMode(MyBase.Getter(Syntax.FunctionCommand,
                                                              Syntax.ExtractScpi(Me._functionMode.Value), access))
            Me._functionMode.ActualValue = Me._functionMode.Value
            Return Me._functionMode.Value.Value
        End Get
        Set(ByVal value As SourceFunctionMode)
            Me._functionMode = Syntax.ParseSourceFunctionMode(MyBase.Setter(Syntax.FunctionCommand,
                                      Syntax.ExtractScpi(Me._functionMode.Value),
                                      Syntax.ExtractScpi(value), access))
        End Set
    End Property

#End Region

#Region " ANY FUNCTION "

    ''' <summary>
    ''' Gets or sets the condition for auto current range.
    ''' </summary>
    ''' <value><c>True</c> if [auto range]; otherwise, <c>False</c>.</value>
    ''' <exception cref="System.ArgumentNullException">scpiFunction</exception>
    Public Property AutoRange(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Boolean
        Get
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            MyBase.Getter(scpiFunction.SyntaxHeader, Syntax.AutoRangeCommand, scpiFunction.AutoRange, access)
            Return scpiFunction.AutoRange.Value.Value
        End Get
        Set(ByVal value As Boolean)
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            scpiFunction.AutoRange = MyBase.Setter(scpiFunction.SyntaxHeader, Syntax.AutoRangeCommand,
                                                   scpiFunction.AutoRange, value, BooleanDataFormat.OnOff, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source function Compliance of specified source function.
    ''' </summary>
    ''' <value>The compliance.</value>
    ''' <exception cref="System.ArgumentNullException">scpiFunction</exception>
    '''   <returns></returns>
    Public Property Compliance(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            MyBase.Getter(scpiFunction.SyntaxHeader, Syntax.ComplianceCommand, scpiFunction.Compliance, access)
            Return scpiFunction.Compliance.Value.Value
        End Get
        Set(ByVal Value As Double)
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            scpiFunction.Compliance = MyBase.Setter(scpiFunction.SyntaxHeader, Syntax.ComplianceCommand,
                                                    scpiFunction.Compliance, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source function immediate amplitude level of the specified source function.
    ''' </summary>
    ''' <value>The level.</value>
    ''' <exception cref="System.ArgumentNullException">scpiFunction</exception>
    '''   <returns></returns>
    Public Property Level(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            MyBase.Getter(scpiFunction.SyntaxHeader, Syntax.LevelCommand, scpiFunction.Level, access)
            Return scpiFunction.Level.Value.Value
        End Get
        Set(ByVal Value As Double)
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            scpiFunction.Level = MyBase.Setter(scpiFunction.SyntaxHeader, Syntax.LevelCommand,
                                               scpiFunction.Level, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the integration period of the specified function in seconds.
    ''' </summary>
    ''' <value>The integration period.</value>
    ''' <exception cref="System.ArgumentNullException">scpiFunction</exception>
    ''' <param name="scpiFunction"></param>
    '''   <param name="access"></param>
    '''   <returns></returns>
    Public Property IntegrationPeriod(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            MyBase.Getter(scpiFunction.SyntaxHeader, "NPLC", scpiFunction.PowerLineCycles, access)
            Return scpiFunction.IntegrationPeriod.Value.Value
        End Get
        Set(ByVal Value As Double)
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            scpiFunction.PowerLineCycles = MyBase.Setter(scpiFunction.SyntaxHeader, "NPLC",
                                                         scpiFunction.PowerLineCycles, Value * scpiFunction.LineFrequency, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the upper range of the active sense function.
    ''' </summary>
    ''' <value>The range.</value>
    ''' <exception cref="System.ArgumentNullException">scpiFunction</exception>
    '''   <returns></returns>
    Public Property Range(ByVal scpiFunction As ScpiFunction, ByVal access As ResourceAccessLevels) As Double
        Get
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            MyBase.Getter(scpiFunction.SyntaxHeader, "RANG", scpiFunction.Range, access)
            Return scpiFunction.Range.Value.Value
        End Get
        Set(ByVal Value As Double)
            If scpiFunction Is Nothing Then
                Throw New ArgumentNullException("scpiFunction")
            End If
            scpiFunction.Range = MyBase.Setter(scpiFunction.SyntaxHeader, "RANG",
                                               scpiFunction.Range, Value, access)
        End Set
    End Property

#End Region

#Region " ACTIVE FUNCTION "

    Private _activeFunction As ScpiFunction
    ''' <summary>
    ''' Gets the current function.
    ''' </summary>
    Public ReadOnly Property ActiveFunction() As ScpiFunction
        Get
            Return Me._activeFunction
        End Get
    End Property

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal value As ScpiFunction, ByVal access As ResourceAccessLevels)
        Me._activeFunction = value
        If value IsNot Nothing Then
            Me.FunctionMode(access) = Me._activeFunction.SourceModality
        End If
    End Sub

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal value As SourceFunctionMode, ByVal access As ResourceAccessLevels)
        If value <> SourceFunctionMode.None Then
            For Each scpiFunction As ScpiFunction In Me._functionCollection
                If scpiFunction.SourceModality = value Then
                    Me.ActivateFunction(scpiFunction, access)
                    Return
                End If
            Next
            Throw New ScpiFunctionNotDefinedException(value)
        End If
    End Sub

    ''' <summary>
    ''' Activate the specified function.
    ''' </summary>
    Public Sub ActivateFunction(ByVal access As ResourceAccessLevels)
        Me.ActivateFunction(Me.FunctionMode(access), access)
    End Sub

    ''' <summary>Gets or sets the condition for auto current range.</summary>
    Public Property AutoRange(ByVal access As ResourceAccessLevels) As Boolean
        Get
            Return Me.AutoRange(Me._activeFunction, access)
        End Get
        Set(ByVal value As Boolean)
            Me.AutoRange(Me._activeFunction, access) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source function Compliance of the active source function.
    ''' </summary>
    Public Property Compliance(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.Compliance(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.Compliance(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source function immediate amplitude level of the active source function.
    ''' </summary>
    Public Property Level(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.Level(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.Level(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the integration period of the active function in seconds.
    ''' </summary>
    Public Property IntegrationPeriod(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.IntegrationPeriod(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.IntegrationPeriod(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the source protection of the active source function.
    ''' </summary>
    Public Property ProtectionLevel(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(Me._activeFunction.SyntaxHeader, Syntax.Protection, Me._activeFunction.ProtectionLevel, access)
            Return Me._activeFunction.ProtectionLevel.Value.Value
        End Get
        Set(ByVal Value As Double)
            Me._activeFunction.ProtectionLevel = MyBase.Setter(Me._activeFunction.SyntaxHeader, Syntax.Protection,
                                                            Me._activeFunction.ProtectionLevel, Value, access)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the upper range of the active sense function.
    ''' </summary>
    Public Property Range(ByVal access As ResourceAccessLevels) As Double
        Get
            Return Me.Range(Me._activeFunction, access)
        End Get
        Set(ByVal Value As Double)
            Me.Range(Me._activeFunction, access) = Value
        End Set
    End Property

    ''' <summary>
    ''' Sets the range and level.
    ''' </summary>
    ''' <param name="range">Specifies the source function range.</param>
    ''' <param name="level">Specifies the source function Level.</param>
    ''' <remarks>
    ''' Setting the source function range and level at once helps ensure that the instrument will 
    ''' not cause a failure if the source level is incompatible with the existing range or conversely if 
    ''' the range is lower then the existing level.
    ''' </remarks>
    Public Sub SetRangeAndLevel(ByVal range As Double, ByVal level As Double)

        Dim commandFormat As String = "{0}{1}:RANG {2}; {0}{1} {3}"
        Dim command As String = String.Format(Globalization.CultureInfo.CurrentCulture,
                                              commandFormat, Me.Name, Me._activeFunction.SyntaxHeader, range, level)
        Me.Controller.WriteLine(command)
        Me._activeFunction.Range.Value = range
        Me._activeFunction.Level.Value = level

    End Sub

    ''' <summary>
    ''' Gets or sets the source function sweep mode e.g., Fixed, Sweep, or List.
    ''' </summary>
    Public Property SweepMode(ByVal access As ResourceAccessLevels) As SweepMode
        Get
            MyBase.Getter(Me._activeFunction.SyntaxHeader, Syntax.ModeCommand, Me._activeFunction.SweepModeCaption, access)
            Return Me._activeFunction.SweepMode.Value.Value
        End Get
        Set(ByVal value As SweepMode)
            Me._activeFunction.SweepModeCaption = MyBase.Setter(Me._activeFunction.SyntaxHeader, Syntax.ModeCommand,
                                                      Me._activeFunction.SweepModeCaption, Syntax.ExtractScpi(value), access)
        End Set
    End Property

    ''' <summary>Gets or sets the source start level.</summary>
    Public Property StartLevel(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(Me._activeFunction.SyntaxHeader, Syntax.StartLevelCommand, Me._activeFunction.StartLevel, access)
            Return Me._activeFunction.StartLevel.Value.Value
        End Get
        Set(ByVal value As Double)
            Me._activeFunction.StartLevel = MyBase.Setter(Me._activeFunction.SyntaxHeader, Syntax.StartLevelCommand,
                                                       Me._activeFunction.StartLevel, value, access)
        End Set
    End Property

    ''' <summary>Gets or sets the source sweep start level.</summary>
    Public Property StopLevel(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(Me._activeFunction.SyntaxHeader, Syntax.StopLevelCommand, Me._activeFunction.StopLevel, access)
            Return Me._activeFunction.StopLevel.Value.Value
        End Get
        Set(ByVal value As Double)
            Me._activeFunction.StopLevel = MyBase.Setter(Me._activeFunction.SyntaxHeader, Syntax.StopLevelCommand,
                                                      Me._activeFunction.StopLevel, value, access)
        End Set
    End Property

#End Region

#Region " DELTA "

    ''' <summary>
    ''' Abort execution of Delta measurements.
    ''' </summary>
    Public Function DeltaAbort() As Boolean

        ' abort the sweep.
        MyBase.Execute(Syntax.Sweep, Syntax.Abort)

        ' turn off the arm state.
        Me._deltaArm = False

        ' abort the waveform
        'MyBase.Execute(Syntax.WaveModalityName, Syntax.Abort)

        Return Not MyBase.Controller.HadError

    End Function

    Private _deltaAbortOnCompliance As ResettableValue(Of Boolean)
    Public ReadOnly Property DeltaAbortOnCompliance() As ResettableValue(Of Boolean)
        Get
            Return Me._deltaAbortOnCompliance
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the condition for aborting the test on the source hitting compliance.
    ''' </summary>
    Public Property DeltaAbortOnCompliance(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.DeltaModalityName, Syntax.ComplianceAbort, Me._deltaAbortOnCompliance, access)
            Return Me._deltaAbortOnCompliance.Value.Value
        End Get
        Set(ByVal value As Boolean)
            Me._deltaAbortOnCompliance = MyBase.Setter(Syntax.DeltaModalityName, Syntax.ComplianceAbort,
                                                    Me._deltaAbortOnCompliance, value, BooleanDataFormat.OnOff, access)
        End Set
    End Property

    Private _deltaArm As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the delta arm.
    ''' </summary>
    ''' <value>The delta arm.</value>
    Public ReadOnly Property DeltaArm() As ResettableValue(Of Boolean)
        Get
            Return Me._deltaArm
        End Get
    End Property

    ''' <summary>Sets the source to auto clear mode.</summary>
    Public Property DeltaArm(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.DeltaModalityName, Syntax.ArmCommand, Me.DeltaArm, access)
            Return Me._deltaArm.Value.Value
        End Get
        Set(ByVal value As Boolean)
            If Subsystem.IsCacheAccess(access) Then
                Me.DeltaArm.Value = value
                Me.DeltaArm.ActualValue = value
            Else
                If Subsystem.IsDeviceAccess(access) OrElse (value <> Me.DeltaArm) Then
                    If value Then
                        MyBase.Execute(Syntax.DeltaModalityName, Syntax.ArmCommand)
                    Else
                        MyBase.Execute(Syntax.SweepModalityName, Syntax.AbortCommand)
                        MyBase.Execute(Syntax.WaveModalityName, Syntax.AbortCommand)
                    End If
                    Me.DeltaArm.Value = value
                End If
                If isr.Scpi.Subsystem.IsVerifyAccess(access) Then
                    ' set the actual value
                    MyBase.Getter(Syntax.DeltaModalityName, Syntax.ArmCommand, Me.DeltaArm, access)
                    ' set the value to the specified value. Now the calling method can check if the value is verified.
                    Me.DeltaArm.Value = value
                End If
            End If
        End Set
    End Property

    Private _deltaHighLevel As ResettableDouble
    ''' <summary>
    ''' Gets the delta high level.
    ''' </summary>
    ''' <value>The delta high level.</value>
    Public ReadOnly Property DeltaHighLevel() As ResettableDouble
        Get
            Return Me._deltaHighLevel
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the source Delta High level.
    ''' </summary>
    ''' <value>The delta high level.</value>
    Public Property DeltaHighLevel(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(Syntax.DeltaModalityName, Syntax.HighCommand, Me._deltaHighLevel, access)
            Return Me._deltaHighLevel.Value.Value
        End Get
        Set(ByVal value As Double)
            Me._deltaHighLevel = MyBase.Setter(Syntax.DeltaModalityName, Syntax.HighCommand, Me._deltaHighLevel, value, access)
        End Set
    End Property

    Private _deltaCount As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the delta count.
    ''' </summary>
    ''' <value>The delta count.</value>
    Public ReadOnly Property DeltaCount() As ResettableValue(Of Integer)
        Get
            Return Me._deltaCount
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the source delta count.  This is the total number of
    ''' valid measurements.  A valid measurement is one which is averaged over the required number
    ''' of average counts.
    ''' </summary>
    ''' <value>The delta count.</value>
    Public Property DeltaCount(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.GetterInfinity(Syntax.DeltaModalityName, Syntax.CountCommand, Me._deltaCount, access)
            Return Me._deltaCount.Value.Value
        End Get
        Set(ByVal value As Integer)
            value = Math.Min(value, 65536)
            Me._deltaCount = MyBase.SetterInfinity(Syntax.DeltaModalityName, Syntax.CountCommand, Me._deltaCount, value, access)
        End Set
    End Property

    Private _deltaDelay As ResettableDouble
    ''' <summary>
    ''' Gets the delta delay.
    ''' </summary>
    ''' <value>The delta delay.</value>
    Public ReadOnly Property DeltaDelay() As ResettableDouble
        Get
            Return Me._deltaDelay
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the source delta delay.
    ''' Allows the current source to settle after changing polarity. Defaults to 2mS for the 6221.
    ''' </summary>
    ''' <value>The delta delay.</value>
    Public Property DeltaDelay(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(Syntax.DeltaModalityName, Syntax.DelayCommand, Me._deltaDelay, access)
            Return Me._deltaDelay.Value.Value
        End Get
        Set(ByVal value As Double)
            Me._deltaDelay = MyBase.Setter(Syntax.DeltaModalityName, Syntax.DelayCommand, Me._deltaDelay, value, access)
        End Set
    End Property

    Private _deltaPresent As Nullable(Of Boolean)

    ''' <summary> Checks if the 6221 is connected to the 2182 for making Delta measurements. </summary>
    ''' <param name="access"> The access. </param>
    ''' <returns> <c>True</c> if [delta present]; otherwise, <c>False</c>. </returns>
    Public Function DeltaPresent(ByVal access As ResourceAccessLevels) As Boolean
        Me._deltaPresent = MyBase.Getter(Syntax.DeltaModalityName, Syntax.PresentCommand, Me._deltaPresent, access)
        If Not Me._deltaPresent.HasValue Then
            Debug.Assert(Not Debugger.IsAttached, "Failed getting serial value")
        End If
        Return Me._deltaPresent.Value
    End Function

#End Region

#Region " SOURCE CONTROL "

    Private _autoClear As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the auto clear.
    ''' </summary>
    ''' <value>The auto clear.</value>
    Public ReadOnly Property AutoClear() As ResettableValue(Of Boolean)
        Get
            Return Me._autoClear
        End Get
    End Property

    ''' <summary>
    ''' Sets the source to auto clear mode.
    ''' </summary>
    ''' <value><c>True</c> if [auto clear]; otherwise, <c>False</c>.</value>
    Public Property AutoClear(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.AutoClearCommand, Me._autoClear, access)
            Return Me._autoClear.Value.Value
        End Get
        Set(ByVal value As Boolean)
            Me._autoClear = MyBase.Setter(Syntax.AutoClearCommand, Me._autoClear, value, BooleanDataFormat.OnOff, access)
        End Set
    End Property

    Private _autoDelay As ResettableValue(Of Boolean)
    ''' <summary>
    ''' Gets the auto delay.
    ''' </summary>
    ''' <value>The auto delay.</value>
    Public ReadOnly Property AutoDelay() As ResettableValue(Of Boolean)
        Get
            Return Me._autoDelay
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the condition for auto current Delay.
    ''' </summary>
    ''' <value><c>True</c> if [auto delay]; otherwise, <c>False</c>.</value>
    Public Property AutoDelay(ByVal access As ResourceAccessLevels) As Boolean
        Get
            MyBase.Getter(Syntax.AutoDelayCommand, Me._autoDelay, access)
            Return Me._autoDelay.Value.Value
        End Get
        Set(ByVal value As Boolean)
            Me._autoDelay = MyBase.Setter(Syntax.AutoDelayCommand, Me._autoDelay, value, BooleanDataFormat.OnOff, access)
        End Set
    End Property

    Private _delay As ResettableDouble
    ''' <summary>
    ''' Gets the delay.
    ''' </summary>
    ''' <value>The delay.</value>
    Public ReadOnly Property Delay() As ResettableDouble
        Get
            Return Me._delay
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the condition for auto current Delay.
    ''' </summary>
    ''' <value>The delay.</value>
    Public Property Delay(ByVal access As ResourceAccessLevels) As Double
        Get
            MyBase.Getter(Syntax.DelayCommand, Me._delay, access)
            Return Me._delay.Value.Value
        End Get
        Set(ByVal value As Double)
            Me._delay = MyBase.Setter(Syntax.DelayCommand, Me._delay, value, access)
            Me._autoDelay = False
        End Set
    End Property

#End Region

#Region " MEMORY "

    Private _memoryPoints As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the memory points.
    ''' </summary>
    ''' <value>The memory points.</value>
    Public ReadOnly Property MemoryPoints() As ResettableValue(Of Integer)
        Get
            Return Me._memoryPoints
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the source memory points.
    ''' </summary>
    ''' <value>The memory points.</value>
    Public Property MemoryPoints(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(Syntax.MemoryModalityName, Syntax.PointsCommand, Me._memoryPoints, access)
            Return Me._memoryPoints.Value.Value
        End Get
        Set(ByVal value As Integer)
            Me._memoryPoints = MyBase.Setter(Syntax.MemoryModalityName, Syntax.PointsCommand, Me._memoryPoints, value, access)
        End Set
    End Property


#End Region

#Region " SWEEP "

    Private _sweepPoints As ResettableValue(Of Integer)
    ''' <summary>
    ''' Gets the sweep points.
    ''' </summary>
    ''' <value>The sweep points.</value>
    Public ReadOnly Property SweepPoints() As ResettableValue(Of Integer)
        Get
            Return Me._sweepPoints
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the source sweep points.
    ''' </summary>
    ''' <value>The sweep points.</value>
    Public Property SweepPoints(ByVal access As ResourceAccessLevels) As Integer
        Get
            MyBase.Getter(Syntax.SweepModalityName, Syntax.PointsCommand, Me._sweepPoints, access)
            Return Me._sweepPoints.Value.Value
        End Get
        Set(ByVal value As Integer)
            Me._sweepPoints = MyBase.Setter(Syntax.SweepModalityName, Syntax.PointsCommand, Me._sweepPoints, value, access)
        End Set
    End Property

#End Region

End Class
