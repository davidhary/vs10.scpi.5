''' <summary>
''' Holds and processes an <see cref="IReading">interface</see> to a single set of instrument readings.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/14/08" by="David" revision="1.0.2935.x">
''' Build to allow a reusable implementation of the K2400 reading.
''' </history>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public MustInherit Class Readings(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <remarks>Use this constructor to instantiate this class
    '''   and set its properties.</remarks>
    Protected Sub New()

        ' instantiate the base class
        MyBase.New()
        Me._readings = New ReadingCollection

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._readings = Nothing

                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " METHODS "

    ''' <summary>Parses the measured data.</summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overridable Sub Parse(ByVal measuredData As String)

        If measuredData Is Nothing Then
            Throw New ArgumentNullException("measuredData")
        ElseIf measuredData.Length = 0 Then
            ' indicate that we do not have a valid value
            Me.Reset()
            Return
        End If
        Me.Parse(measuredData.Split(","c), 0)

    End Sub

    ''' <summary>Parses the measured data.</summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overridable Sub Parse(ByVal measuredData As String())

        Me.Parse(measuredData, 0)

    End Sub

    ''' <summary>Parses the measured data.</summary>
    ''' <param name="firstElementIndex">Zero-based index of first element.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overridable Sub Parse(ByVal measuredData As String(), ByVal firstElementIndex As Integer)

        If measuredData Is Nothing Then
            Throw New ArgumentNullException("measuredData")
        ElseIf measuredData.Length < firstElementIndex + Me._readings.Count Then
            ' indicate that we do not have a valid value
            Me.Reset()
            Return
        End If

        For Each readingItem As IReading In Me._readings
            readingItem.Reading = measuredData(firstElementIndex)
            firstElementIndex += 1
        Next

    End Sub

    ''' <summary>Parses the measured data.</summary>
    ''' <param name="unprocessedReading">Specifies a reading elements set where thresholds were not set.</param>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overridable Sub Parse(ByVal unprocessedReading As Readings(Of T))

        If unprocessedReading Is Nothing Then
            Throw New ArgumentNullException("unprocessedReading")
        End If

        ' indicate that we do not have a valid value
        Me.Reset()

        Dim readings(Me._readings.Count - 1) As String
        Dim index As Integer = 0
        For Each readingItem As IReading In unprocessedReading._readings
            readings(index) = readingItem.Reading
            index += 1
        Next

        Me.Parse(readings, 0)

    End Sub

    ''' <summary>Resets the measured outcomes.</summary>
    Public Overridable Sub Reset()
        For Each reading As IReading In Me._readings
            reading.Reset()
        Next
    End Sub

#End Region

#Region " PROPERTIES and METHODS "

    Private _readings As ReadingCollection
    ''' <summary>
    ''' Gets a collection of readings that implements the <see cref="IReading">interface</see>.
    ''' </summary>
    Protected ReadOnly Property Readings() As ReadingCollection
        Get
            Return Me._readings
        End Get
    End Property

    ''' <summary>
    '''  Adds a reading to the collection and sets the measurement readings.
    ''' </summary>
    ''' <param name="reading"></param>
    Protected Sub AddReading(ByVal reading As IReading, ByVal readingLength As Integer)
        If Me._readings Is Nothing Then
            Me._readings = New ReadingCollection
        End If
        Me._readings.Add(reading, readingLength)
    End Sub

    ''' <summary>
    ''' Returns the number of elements in the reading set.
    ''' </summary>
    Public ReadOnly Property ElementsCount() As Integer
        Get
            If Me._readings Is Nothing Then
                Return 0
            Else
                Return Me._readings.Count
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns the total length of each reading record.
    ''' </summary>
    Public ReadOnly Property ElementsLength() As Integer
        Get
            If Me._readings Is Nothing Then
                Return 0
            Else
                Return Me._readings.ElementsLength
            End If
        End Get
    End Property


#End Region

End Class

''' <summary>
''' Defines the collection of reading elements.
''' </summary>
Public Class ReadingCollection
    Inherits System.Collections.ObjectModel.Collection(Of IReading)

    Public Shadows Sub Add(ByVal item As IReading, ByVal readingLength As Integer)
        MyBase.Add(item)
        If Me._elementsLength > 0 Then
            ' add the length of the delimiter.
            Me._elementsLength += 1
        End If
        Me._elementsLength += readingLength
    End Sub

    Private _elementsLength As Integer
    ''' <summary>
    ''' Returns the total length of the reading elements including delimiters.
    ''' </summary>
    Public ReadOnly Property ElementsLength() As Integer
        Get
            Return Me._elementsLength
        End Get
    End Property

End Class