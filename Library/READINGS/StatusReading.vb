''' <summary>
''' Defines a <see cref="System.Int32">Status</see> reading.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/09/08" by="David" revision="3.0.3021.x">
''' Inherit from <see cref="isr.Scpi.ValueReading">Value Reading</see>
''' </history>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class StatusReading
    Inherits isr.Scpi.ReadingLong

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a measured value without specifying the value or 
    ''' its validity, which must be specified for the value to be made valid.</summary>
    Public Sub New()
        MyBase.New()
        onInstantiate()
    End Sub

    ''' <summary>
    ''' Constructs a measured value with a valid new value.</summary>
    ''' <param name="newValue">Specifies the measured value.</param>
    Public Sub New(ByVal newValue As Long)
        MyBase.New(newValue)
        onInstantiate()
    End Sub

    ''' <summary>
    ''' Constructs a copy of an existing value.
    ''' </summary>
    Public Sub New(ByVal model As StatusReading)

        MyBase.New(model)
        If model IsNot Nothing Then
            Me._complianceBits = model._complianceBits
            Me._limit1Bits = model._limit1Bits
            Me._limit2Bits = model._limit2Bits
            Me._rangeComplianceBits = model._rangeComplianceBits
        End If

    End Sub

    ''' <summary>
    ''' Set default initialization values
    ''' </summary>
    Private Sub onInstantiate()
        MyBase.DisplayCaption.Units.LongUnits = "Hex"
        MyBase.DisplayCaption.Units.ValueFormat = "X"
        Me._complianceBits = CInt(2 ^ 3) ' StatusWordBits.HitCompliance
        Me._limit1Bits = CInt(2 ^ 9) ' StatusWordBits.LimitResultBit1
        Me._limit2Bits = CInt(2 ^ 19) ' StatusWordBits.LimitResultBit2
        Me._rangeComplianceBits = CInt(2 ^ 16) ' StatusWordBits.HitRangeCompliance
    End Sub

#End Region

#Region " PROPERTIES "

    Private _complianceBits As Integer
    ''' <summary>
    ''' Gets or sets the bits for detecting compliance.
    ''' </summary>
    Public Property ComplianceBits() As Integer
        Get
            Return Me._complianceBits
        End Get
        Set(ByVal value As Integer)
            Me._complianceBits = value
        End Set
    End Property

    Private _limit1Bits As Integer
    ''' <summary>
    ''' Gets or sets the bits for detecting limit 1 failure.
    ''' </summary>
    Public Property Limit1Bits() As Integer
        Get
            Return Me._limit1Bits
        End Get
        Set(ByVal value As Integer)
            Me._limit1Bits = value
        End Set
    End Property

    Private _limit2Bits As Integer
    ''' <summary>
    ''' Gets or sets the bits for detecting limit 2 failure.
    ''' </summary>
    Public Property Limit2Bits() As Integer
        Get
            Return Me._limit2Bits
        End Get
        Set(ByVal value As Integer)
            Me._limit2Bits = value
        End Set
    End Property

    Private _rangeComplianceBits As Integer
    ''' <summary>
    ''' Gets or sets the bits for detecting range compliance.
    ''' </summary>
    Public Property RangeComplianceBits() As Integer
        Get
            Return Me._rangeComplianceBits
        End Get
        Set(ByVal value As Integer)
            Me._rangeComplianceBits = value
        End Set
    End Property

    ''' <summary>Returns an outcome string depending on the measured outcome or pass code.</summary>
    Public ReadOnly Property LimitResults() As String
        Get

            Select Case MyBase.Value And (Me.Limit1Bits Or Me.Limit2Bits)

                Case Me.Limit1Bits

                    Return "F1"

                Case Me.Limit2Bits

                    Return "F2"

                Case Me.Limit1Bits Or Me.Limit2Bits

                    Return "F3"

                Case Else

                    ' 1.11.14 display P and not...
                    Return "P"

            End Select

        End Get
    End Property

    ''' <summary>Gets or sets the condition for hit real compliance.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
    Public ReadOnly Property IsHitCompliance() As Boolean
        Get
            Return (Me.Value.Value And Me.ComplianceBits) <> 0
        End Get
    End Property

    ''' <summary>Gets or sets the condition for hit range compliance.</summary>
    ''' <value>A <see cref="System.Boolean">Boolean</see> value</value>
    Public ReadOnly Property IsHitRangeCompliance() As Boolean
        Get
            Return (Me.Value.Value And Me.RangeComplianceBits) <> 0
        End Get
    End Property

#End Region

End Class

