''' <summary>
''' Holds the measurand outcome meta data and derived properties.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' Created
''' </history>
Public Class MeasurandOutcome

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Contracts this class setting the outcome to <see cref="MetaOutcomes">None</see>.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        Me.Reset()
    End Sub

    ''' <summary>
    ''' Constructs a copy of an existing value.
    ''' </summary>
    Public Sub New(ByVal model As MeasurandOutcome)

        MyBase.New()
        If model IsNot Nothing Then
            Me._value = model._value
        End If

    End Sub

#End Region

#Region " SHARED "

    ''' <summary>
    ''' Returns an outcome string depending on the outcome code.
    ''' </summary>
    ''' <param name="outcomeCode">Specifies an outcome code such as that returned by the 2400 Keithley
    ''' source measure unit.
    ''' </param>
    Public Shared Function GetOutcome(ByVal outcomeCode As Int64) As String

        Select Case (Convert.ToInt32(outcomeCode) And &H300S)

            Case &H100S

                Return "F1"

            Case &H200S

                Return "F2"

            Case &H300S

                Return "F3"

            Case Else

                ' 1.11.14 display P and not...
                Return "P"

        End Select

    End Function

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Resets the outcome to None.
    ''' </summary>
    Public Sub Reset()
        Me._value = MetaOutcomes.None
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Returns a short outcome string representing the measurand meta outcome.
    ''' </summary>
    Public ReadOnly Property ShortOutcome() As String
        Get
            If Me.IsOutcome(MetaOutcomes.None) Then
                ' if not set then return NO
                Return "no"
            ElseIf Not Me.HasValue Then
                ' if no value we are out of luck
                Return "nv"
            ElseIf Me.IsValid Then
                ' if valid, it can be high, low, or passed
                If Me.IsPass Then
                    Return "p"
                ElseIf Me.IsHigh Then
                    Return "hi"
                ElseIf Me.IsLow Then
                    Return "lo"
                End If
                ' if we do not return a value, raise an exception
                Debug.Assert(Not Debugger.IsAttached, "Unhandled case in determining valid short outcome.")
                Return "na"
            Else
                ' if not valid then it can be contact check or compliance. 
                If Me.FailedContactCheck Then
                    Return "cc"
                ElseIf Me.HitCompliance Then
                    Return "c"
                ElseIf Me.HitRangeCompliance Then
                    Return "rc"
                End If
                ' if we do not return a value, raise an exception
                Debug.Assert(Not Debugger.IsAttached, "Unhandled case in determining invalid short outcome.")
                Return "na"
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns a long outcome string representing the measurand meta outcome.
    ''' </summary>
    Public ReadOnly Property LongOutcome() As String
        Get
            If Me.IsOutcome(MetaOutcomes.None) Then
                ' if not set then return NO
                Return "Not Set"
            ElseIf Not Me.HasValue Then
                ' if no value we are out of luck
                Return "No Value"
            ElseIf Me.IsValid Then
                ' if valid, it can be high, low, or passed
                If Me.IsPass Then
                    Return "Pass"
                ElseIf Me.IsHigh Then
                    Return "High"
                ElseIf Me.IsLow Then
                    Return "Low"
                End If
                ' if we do not return a value, raise an exception
                Debug.Assert(Not Debugger.IsAttached, "Unhandled case in determining valid short outcome.")
                Return "NA"
            Else
                ' if not valid then it can be contact check or compliance. 
                If Me.FailedContactCheck Then
                    Return "Contact Check"
                ElseIf Me.HitCompliance Then
                    Return "Compliance"
                ElseIf Me.HitRangeCompliance Then
                    Return "Range Compliance"
                End If
                ' if we do not return a value, raise an exception
                Debug.Assert(Not Debugger.IsAttached, "Unhandled case in determining invalid short outcome.")
                Return "NA"
            End If
        End Get
    End Property

#End Region

#Region " CONDITIONS "

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a failed contact check.
    ''' </summary>
    Public Property FailedContactCheck() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.FailedContactCheck)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.FailedContactCheck) = value
            Me.IsOutcome(MetaOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a having a value, namely if a value was set.
    ''' </summary>
    Public Property HasValue() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.HasValue)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.HasValue) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a high value.
    ''' </summary>
    Public Property IsHigh() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.High)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.High) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a measurement that Hit Compliance.
    ''' </summary>
    Public ReadOnly Property HitCompliance() As Boolean
        Get
            Return Me.HitLevelCompliance OrElse Me.HitStatusCompliance
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a measurement that hit level compliance.
    ''' Level compliance is hit if the measured level absolute value
    ''' is outside the compliance limits.  This is a calculated value
    ''' designed to address cases where the compliance bit is in correct.
    ''' </summary>
    Public Property HitLevelCompliance() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.HitLevelCompliance)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.HitLevelCompliance) = value
            Me.IsOutcome(MetaOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a measurement that hit Status compliance.
    ''' Status compliance is reported by the instrument in the status word.
    ''' </summary>
    Public Property HitStatusCompliance() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.HitStatusCompliance)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.HitStatusCompliance) = value
            Me.IsOutcome(MetaOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a measurement that Hit Range Compliance.
    ''' </summary>
    Public Property HitRangeCompliance() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.HitRangeCompliance)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.HitRangeCompliance) = value
            Me.IsOutcome(MetaOutcomes.Valid) = Not value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a measurement that is low.
    ''' </summary>
    Public Property IsLow() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.Low)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.Low) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the outcome.  This is the only valid path for setting the 
    ''' outcome.
    ''' </summary>
    ''' <param name="outcome">Specifies the outcome flag which to get or set.</param>
    Public Property IsOutcome(ByVal outcome As MetaOutcomes) As Boolean
        Get
            Return (Me._value And outcome) = outcome
        End Get
        Set(ByVal value As Boolean)
            If value Then
                Me._value = Me._value Or outcome
            Else
                Me._value = Me._value And (Not outcome)
            End If
            ' set the pass flag
            If Me.Passed Then
                Me._value = Me._value Or MetaOutcomes.Pass
            Else
                Me._value = Me._value And (Not MetaOutcomes.Pass)
            End If
            ' set the Has Value flag
            Me._value = Me._value Or MetaOutcomes.HasValue
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome is Pass.
    ''' </summary>
    Public Property IsPass() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.Pass)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.Pass) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome is Valid. 
    ''' The measured value is valid when its value or pass/fail outcomes or compliance or
    ''' contact check conditions are set.
    ''' </summary>
    Public Property IsValid() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.Valid)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.Valid) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a measurement that is infinity.
    ''' </summary>
    Public Property Infinity() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.Infinity)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.Infinity) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a measurement that is not a number.
    ''' </summary>
    Public Property NotANumber() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.NotANumber)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.NotANumber) = value
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the condition determining if the outcome indicates a measurement that is negative infinity.
    ''' </summary>
    Public Property NegativeInfinity() As Boolean
        Get
            Return Me.IsOutcome(MetaOutcomes.NegativeInfinity)
        End Get
        Set(ByVal value As Boolean)
            Me.IsOutcome(MetaOutcomes.NegativeInfinity) = value
        End Set
    End Property

    ''' <summary>
    ''' Returns True if passed.
    ''' </summary>
    Public Function Passed() As Boolean

        Return Me.IsValid AndAlso
            (Not (Me.FailedContactCheck OrElse Me.HitCompliance OrElse Me.HitRangeCompliance)) AndAlso
            (Not (Me.IsHigh OrElse Me.IsLow))

    End Function

    ''' <summary>
    ''' Returns true if the value is valid, not out of range and not failed.
    ''' </summary>
    Public Function Valid() As Boolean
        Return Me.IsOutcome(MetaOutcomes.Valid) AndAlso Not (
               Me.IsOutcome(MetaOutcomes.NotANumber) OrElse
               Me.IsOutcome(MetaOutcomes.Infinity) OrElse
               Me.IsOutcome(MetaOutcomes.NegativeInfinity) OrElse
               Me.IsOutcome(MetaOutcomes.FailedContactCheck) OrElse
               Me.IsOutcome(MetaOutcomes.HitLevelCompliance) OrElse
               Me.IsOutcome(MetaOutcomes.HitRangeCompliance))
    End Function

    Private _value As MetaOutcomes
    ''' <summary>
    ''' Holds the outcome
    ''' </summary>
    Public ReadOnly Property [Value]() As MetaOutcomes
        Get
            Return Me._value
        End Get
    End Property

#End Region

End Class

