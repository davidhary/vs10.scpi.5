''' <summary>
''' Implements a generic value reading.  A value reading as a reading such as time stamp or
''' reading number that requires no limits and thus has no outcome type.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' Created
''' </history>
Public MustInherit Class ValueReading(Of T As {Structure, IComparable(Of T), IEquatable(Of T), IFormattable})
    Implements IReading

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a measured value without specifying the value or 
    ''' its validity, which must be specified for the value to be made valid.</summary>
    Protected Sub New()
        MyBase.New()
        Me._reading = "Not Set"
        Me._generator = New RandomNumberGenerator()
        Me._displayCaption = New FormattedReading(Of T)
        Me._saveCaption = New FormattedReading(Of T)
    End Sub

    ''' <summary>
    ''' Constructs a measured value with a valid new value.</summary>
    ''' <param name="newValue">Specifies the measured value.</param>
    Protected Sub New(ByVal newValue As T)
        Me._value = newValue
    End Sub

    ''' <summary>
    ''' Constructs a copy of an existing value.
    ''' </summary>
    Protected Sub New(ByVal model As ValueReading(Of T))

        MyBase.New()

        If model IsNot Nothing Then
            Me._displayCaption = New FormattedReading(Of T)(model._displayCaption)
            Me._heading = model._heading
            Me._reading = model._reading
            Me._saveCaption = New FormattedReading(Of T)(model._saveCaption)
            Me._value = model._value
        End If

    End Sub

#End Region

#Region " IREADING "

    ''' <summary>
    ''' Parses the reading to create the specific reading type in the inherited class.
    ''' </summary>
    ''' <param name="reading">Specifies the reading text.</param>
    ''' <returns>
    ''' Parse can only be implemented in the inheriting classes because type
    ''' calculations are required.
    ''' </returns>
    Public MustOverride Function Parse(ByVal reading As String) As Boolean Implements IReading.Parse

#End Region

#Region " EQUALS "

    ''' <summary>Returns True if the value of the <paramref name="obj" /> equals to the
    '''   instance value.</summary>
    ''' <param name="obj">The object to compare for equality with this instance.
    '''   This object should be type <see cref="ValueReading"/></param>
    ''' <returns>Returns <c>True</c> if <paramref name="obj" /> is the same value as this
    '''   instance; otherwise, <c>False</c></returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return ValueReading(Of T).Equals(Me, TryCast(obj, ValueReading(Of T)))
    End Function

    ''' <summary>Creates a unique hash code.</summary>
    ''' <returns>An <see cref="System.Int32">Int32</see> value</returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me._value.GetHashCode
    End Function

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Resets measured value to nothing.
    ''' </summary>
    Public Overridable Sub Reset() Implements IReading.Reset
        Me.Reset(Nothing)
    End Sub

    ''' <summary>
    ''' Resets measured value to given value.
    ''' </summary>
    Public Overridable Sub Reset(ByVal resetValue As T)
        Me._value = resetValue
    End Sub

    ''' <summary>
    ''' Sets the measured value and it validity.  
    ''' An invalid value indicates, for instance, 
    ''' a failed measurement.
    ''' </summary>
    ''' <param name="newValue">Specifies the new value.</param>
    Public Overridable Sub SetValue(ByVal newValue As T)
        Me._value = newValue
    End Sub

    ''' <summary>Returns the value to display using the <see cref="DisplayCaption"/>.</summary>
    Public Overridable Overloads Function ToDisplayString() As String
        Return Me.DisplayCaption.Caption()
    End Function

    ''' <summary>Returns the value to save using the <see cref="SaveCaption"/>.</summary>
    Public Overridable Function ToSaveString() As String
        Return Me.SaveCaption.ToString()
    End Function

    ''' <summary>Returns the default string representation of the value.</summary>
    ''' <param name="format">The format string</param>
    Public Overridable Overloads Function ToString(ByVal format As String) As String
        If Me._value.HasValue Then
            Return Me._value.Value.ToString(format, Globalization.CultureInfo.CurrentCulture)
        Else
            Return Me._reading
        End If
    End Function

#End Region

#Region " PROPERTIES "

    Private _displayCaption As FormattedReading(Of T)
    ''' <summary>
    ''' Holds the caption for saving values to file.
    ''' </summary>
    Public ReadOnly Property DisplayCaption() As FormattedReading(Of T)
        Get
            Return Me._displayCaption
        End Get
    End Property

    Private _heading As String
    Public Property Heading() As String
        Get
            Return Me._heading
        End Get
        Set(ByVal value As String)
            Me._heading = value
        End Set
    End Property

    Private _reading As String = "Not Set"
    ''' <summary>Gets or sets the reading text.</summary>
    Public Overridable Property Reading() As String Implements IReading.Reading
        Get
            Return Me._reading
        End Get
        Set(ByVal value As String)
            If value IsNot Nothing Then
                Me._reading = value.Trim
            End If
        End Set
    End Property

    Private _value As Nullable(Of T)
    ''' <summary>
    ''' Holds the measured value.
    ''' </summary>
    Public ReadOnly Property [Value]() As Nullable(Of T)
        Get
            Return Me._value
        End Get
    End Property

    Private _generator As RandomNumberGenerator
    Public ReadOnly Property Generator() As RandomNumberGenerator
        Get
            Return Me._generator
        End Get
    End Property

    Private _saveCaption As FormattedReading(Of T)
    ''' <summary>
    ''' Holds the caption for saving values to file.
    ''' </summary>
    Public ReadOnly Property SaveCaption() As FormattedReading(Of T)
        Get
            Return Me._saveCaption
        End Get
    End Property

    ''' <summary>
    ''' Holds the simulated value.
    ''' </summary>
    Public ReadOnly Property SimulatedValue() As Double
        Get
            Return Me._generator.Value
        End Get
    End Property

#End Region

End Class

