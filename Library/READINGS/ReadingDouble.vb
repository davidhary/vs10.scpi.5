''' <summary>
''' Implements a double value reading.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' Created
''' </history>
Public Class ReadingDouble
    Inherits ValueReading(Of Double)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a measured value without specifying the value or 
    ''' its validity, which must be specified for the value to be made valid.</summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Constructs a measured value with a valid new value.</summary>
    ''' <param name="newValue">Specifies the measured value.</param>
    Public Sub New(ByVal newValue As Double)
        MyBase.New(newValue)
    End Sub

    ''' <summary>
    ''' Constructs a copy of an existing value.
    ''' </summary>
    Public Sub New(ByVal model As ReadingDouble)

        MyBase.New(model)

    End Sub

#End Region

    ''' <summary>
    ''' Sets a new double reading value.
    ''' </summary>
    ''' <param name="newValue"></param>
    Public Overrides Sub SetValue(ByVal newValue As Double)
        MyBase.SetValue(newValue)
        MyBase.SaveCaption.ScaledValue = Me.Value.Value * MyBase.SaveCaption.Units.ScaleFactor
        MyBase.DisplayCaption.ScaledValue = Me.Value.Value * MyBase.DisplayCaption.Units.ScaleFactor
    End Sub

    ''' <summary>
    ''' Parses the reading to create the specific reading type in the inherited class.
    ''' </summary>
    ''' <param name="reading">Specifies the reading text.</param>
    Public Overrides Function Parse(ByVal reading As String) As Boolean
        ' convert reading to numeric
        Dim value As Double
        If Double.TryParse(reading, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture, value) Then
            Me.SetValue(value)
            Return True
        Else
            Me.SetValue(isr.Scpi.Syntax.NotANumber)
            Return False
        End If
    End Function

    ''' <summary>Gets or sets the reading text.</summary>
    Public Overrides Property Reading() As String
        Get
            Return MyBase.Reading
        End Get
        Set(ByVal value As String)
            MyBase.Reading = value
            If String.IsNullOrWhiteSpace(value) Then
                Me.SetValue(isr.Scpi.Syntax.NotANumber)
            ElseIf Parse(value) Then
            End If
        End Set
    End Property

End Class