#Region " MEASURAND-RELATED TYPES "

''' <summary>
''' Holds the measurand outcome flags.
''' </summary>
<System.Flags()>
Public Enum MetaOutcomes
    <ComponentModel.Description("None")> None = CLng(0)
    <ComponentModel.Description("Failed Contact Check")> FailedContactCheck = CLng(2 ^ 0)
    <ComponentModel.Description("Has Value")> HasValue = CLng(2 ^ 1)
    <ComponentModel.Description("High")> High = CLng(2 ^ 2)
    <ComponentModel.Description("Hit Status Compliance")> HitStatusCompliance = CLng(2 ^ 3)
    <ComponentModel.Description("Hit Level Compliance")> HitLevelCompliance = CLng(2 ^ 4)
    <ComponentModel.Description("Hit Range Compliance")> HitRangeCompliance = CLng(2 ^ 5)
    <ComponentModel.Description("Infinity")> Infinity = CLng(2 ^ 6)
    <ComponentModel.Description("Low")> Low = CLng(2 ^ 7)
    <ComponentModel.Description("Negative Infinity")> NegativeInfinity = CLng(2 ^ 8)
    <ComponentModel.Description("Not a number")> NotANumber = CLng(2 ^ 9)
    <ComponentModel.Description("Pass")> Pass = CLng(2 ^ 10)
    <ComponentModel.Description("Valid")> Valid = CLng(2 ^ 11)
End Enum

#End Region
