''' <summary>
''' Encapsulation conversion of measured values for display or storage.
''' </summary>
''' <typeparam name="T"></typeparam>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' Created
''' </history>
Public Class FormattedReading(Of T As IFormattable)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.new()
        Me._units = New Scpi.FormattedUnits()
        Me._units.ValueFormat = "0.000E+0"
    End Sub

    ''' <summary>
    ''' Constructs a copy of an existing value.
    ''' </summary>
    Public Sub New(ByVal model As FormattedReading(Of T))

        ' instantiate the base class
        MyBase.New()
        If model IsNot Nothing Then
            Me._scaledValue = model._scaledValue
            Me._showUnits = model._showUnits
            Me._units = New FormattedUnits(model._units)
        End If

    End Sub

#End Region

#Region " CAPTION "

    ''' <summary>
    ''' Returns the value to display using the given <paramref name="format">format</paramref> and
    ''' the <see cref="ScaledValue"/></summary>
    ''' <param name="outcome">Specifies the measured outcome for handling invalid values.</param>
    ''' <param name="format">Specifies the format for the caption.</param>
    Public Function Caption(ByVal outcome As MeasurandOutcome, ByVal format As String) As String

        If outcome Is Nothing Then
            Throw New ArgumentNullException("outcome")
        ElseIf outcome.IsValid Then
            If Me.ShowUnits Then
                Return Me.Caption(format)
            Else
                Return Me.ScaledValue.ToString(format, Globalization.CultureInfo.CurrentCulture)
            End If
        Else
            Return outcome.LongOutcome
        End If

    End Function

    ''' <summary>
    ''' Returns the value to display using the <see cref="FormattedUnits.ValueFormat"/> and
    ''' the <see cref="ScaledValue"/></summary>
    ''' <param name="outcome">Specifies the measured outcome for handling invalid values.</param>
    Public Function Caption(ByVal outcome As MeasurandOutcome) As String

        If outcome Is Nothing Then
            Throw New ArgumentNullException("outcome")
        ElseIf outcome.IsValid Then
            Return Me.Caption(Me.Units.ValueFormat)
        Else
            Return outcome.LongOutcome
        End If

    End Function

    ''' <summary>
    ''' Returns the value to display using the given <paramref name="format">format</paramref> and
    ''' the <see cref="ScaledValue"/></summary>
    ''' <param name="format">Specifies the format for the caption.</param>
    Public Function Caption(ByVal format As String) As String

        If format Is Nothing Then
            Throw New ArgumentNullException("format")
        ElseIf Me.ShowUnits Then
            Return Scpi.FormattedUnits.ToCaption(format, Me.ScaledValue, Me._units)
            ' Return Me.ScaledValue.ToString(format, Globalization.CultureInfo.CurrentCulture) & String.Empty.PadRight(1) & Me._units.ToString
        Else
            Return Me.ScaledValue.ToString(format, Globalization.CultureInfo.CurrentCulture)
        End If

    End Function

    ''' <summary>
    ''' Returns the value to display using the <see cref="FormattedUnits.ValueFormat"/> and
    ''' the <see cref="ScaledValue"/></summary>
    Public Function Caption() As String
        Return Me.Caption(Me.Units.ValueFormat)
    End Function

#End Region

#Region " SCALED VALUE "

    ''' <summary>
    ''' Holds the scaled value to use for display.  This equals the value times
    ''' the display scale factor.  In this generic class, we have to
    ''' expose this for the user to set otherwise we need to add algebra to 
    ''' this generic class which hopefully will be done with the next Visual Studio.
    ''' </summary>
    Public Property ScaledValue() As T

    ''' <summary>
    ''' Holds true for adding units to the display.
    ''' </summary>
    Public Property ShowUnits() As Boolean

#End Region

    ''' <summary>Gets or sets the units format for the value</summary>
    Public Property Units() As Scpi.FormattedUnits

End Class

