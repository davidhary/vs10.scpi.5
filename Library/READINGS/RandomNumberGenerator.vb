''' <summary>
''' Provides a simulated value for testing measurements without having the benefit of 
''' instruments.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' Created
''' </history>
Public Class RandomNumberGenerator

    ''' <summary>
    ''' Contracts this class setting the outcome to <see cref="MetaOutcomes">None</see>.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        If generator Is Nothing Then
            generator = New Random() ' CInt(Date.Now.Ticks Mod Integer.MaxValue))
        End If
        Me.Min = 0
        Me.Max = 1
    End Sub

    ''' <summary>
    ''' Holds a shared reference to the number generator.
    ''' </summary>
    ''' <remarks>A shared generator was selected because multiple generators tended 
    ''' to create the same random number.</remarks>
    Private Shared generator As Random
    Private range As Double

    Private _min As Double
    Public Property Min() As Double
        Get
            Return Me._min
        End Get
        Set(ByVal value As Double)
            Me._min = value
            range = Me._max - Me._min
        End Set
    End Property

    Private _max As Double
    Public Property Max() As Double
        Get
            Return Me._max
        End Get
        Set(ByVal value As Double)
            Me._max = value
            range = Me._max - Me._min
        End Set
    End Property

    ''' <summary>
    ''' Returns a simulated value
    ''' </summary>
    Public ReadOnly Property [Value]() As Double
        Get
            Dim newValue As Double = generator.NextDouble * Me.range + Me.Min
            '      Debug.WriteLine(newValue)
            Return newValue
        End Get
    End Property

End Class

