''' <summary>
''' Implements a generic measurand reading.
''' </summary>
''' <typeparam name="T">Specifies the type parameter of the generic class.</typeparam>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' Created
''' </history>
Public MustInherit Class MeasurandReading(Of T As {Structure, IComparable(Of T), IEquatable(Of T), IFormattable})
    Inherits ValueReading(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a measured value without specifying the value or 
    ''' its validity, which must be specified for the value to be made valid.</summary>
    Protected Sub New()
        MyBase.New()
        Me._outcome = New MeasurandOutcome()
    End Sub

    ''' <summary>
    ''' Constructs a measured value with a valid new value.</summary>
    ''' <param name="newValue">Specifies the measured value.</param>
    Protected Sub New(ByVal newValue As T)
        MyBase.New(newValue)
        Me._outcome = New MeasurandOutcome()
        Me._UpdateValidity(True)
    End Sub

    ''' <summary>Constructs a measured value</summary>
    ''' <param name="newValue">Specifies the measured value.</param>
    ''' <param name="valid">The value auto settings mode</param>
    Protected Sub New(ByVal newValue As T, ByVal valid As Boolean)
        MyBase.New(newValue)
        Me._outcome = New MeasurandOutcome()
        Me._UpdateValidity(valid)
    End Sub

    ''' <summary>
    ''' Constructs a copy of an existing value.
    ''' </summary>
    Protected Sub New(ByVal model As MeasurandReading(Of T))

        MyBase.New(model)
        If model IsNot Nothing Then
            Me._highLimit = model._highLimit
            Me._lastValidReading = model._lastValidReading
            Me._lowLimit = model._lowLimit
            Me._outcome = New MeasurandOutcome(model._outcome)
        End If

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Resets measured value to nothing.
    ''' </summary>
    Public Overrides Sub Reset()
        Me.Reset(Nothing)
    End Sub

    ''' <summary>
    ''' Resets measured value to given value.  This also sets <see cref="Outcome"/>
    ''' to <see cref="MetaOutcomes">None</see>.
    ''' </summary>
    Public Overrides Sub Reset(ByVal resetValue As T)
        MyBase.Reset(resetValue)
        Me._outcome.Reset()
    End Sub

    ''' <summary>
    ''' Updates the measured value validity.  
    ''' An invalid value indicates, for instance, 
    ''' a failed measurement.
    ''' Does not set the base value so that it can be called from the constructor.
    ''' </summary>
    ''' <param name="valid">True to tag the value as valid or false to tag the value as
    ''' invalid.  A value is invalid in cases where the measurement could be met such as
    ''' contact check failure or compliance.  In these cases, the relevant 
    ''' <see cref="MeasurandOutcome">outcomes</see> must also be set.</param>
    Private Sub _UpdateValidity(ByVal valid As Boolean)
        Me._outcome.IsValid = valid
        If valid Then
            Me._outcome.IsHigh = Me.Value.Value.CompareTo(Me._highLimit) > 0
            Me._outcome.IsLow = Me.Value.Value.CompareTo(Me._lowLimit) < 0
        End If
    End Sub

    ''' <summary>
    ''' Sets the measured value and it validity.  
    ''' An invalid value indicates, for instance, 
    ''' a failed measurement.
    ''' </summary>
    ''' <param name="newValue">Specifies the new value.</param>
    ''' <param name="valid">True to tag the value as valid or false to tag the value as
    ''' invalid.  A value is invalid in cases where the measurement could be met such as
    ''' contact check failure or compliance.  In these cases, the relevant 
    ''' <see cref="MeasurandOutcome">outcomes</see> must also be set.</param>
    Public Overridable Overloads Sub SetValue(ByVal newValue As T, ByVal valid As Boolean)
        MyBase.SetValue(newValue)
        Me._UpdateValidity(valid)
    End Sub

    ''' <summary>Returns the value to display using the <see cref="DisplayCaption"/>.</summary>
    Public Overloads Overrides Function ToDisplayString() As String
        Return Me.DisplayCaption.Caption(Me._outcome)
    End Function

    ''' <summary>Returns the value to save using the <see cref="SaveCaption"/>.</summary>
    Public Overrides Function ToSaveString() As String
        Return Me.SaveCaption.Caption(Me._outcome)
    End Function

    ''' <summary>
    ''' Returns the default string representation of the value.
    ''' This uses the display format with units.
    ''' </summary>
    Public Overloads Function ToString() As String
        Return Me.DisplayCaption.Caption(Me._outcome)
    End Function

    ''' <summary>Returns the default string representation of the value.</summary>
    ''' <param name="format">The format string</param>
    Public Overloads Function ToString(ByVal format As String) As String
        If Me._outcome.IsValid Then
            Return MyBase.ToString(format)
        Else
            Return Me._outcome.ShortOutcome
        End If
    End Function

#End Region

#Region " PROPERTIES "

    Private _highLimit As T
    ''' <summary>Gets or sets the high limit</summary>
    Public Property HighLimit() As T
        Get
            Return Me._highLimit
        End Get
        Set(ByVal value As T)
            Me._highLimit = value
        End Set
    End Property

    Private _lastValidReading As String
    ''' <summary>
    ''' Holds the last valid reading.
    ''' </summary>
    Public ReadOnly Property LastValidReading() As String
        Get
            Return Me._lastValidReading
        End Get
    End Property

    Private _lowLimit As T
    ''' <summary>Gets or sets the low limit</summary>
    Public Property LowLimit() As T
        Get
            Return Me._lowLimit
        End Get
        Set(ByVal value As T)
            Me._lowLimit = value
        End Set
    End Property

    Private _outcome As MeasurandOutcome
    ''' <summary>
    ''' Holds the measurand outcome value.
    ''' </summary>
    Public Property Outcome() As MeasurandOutcome
        Get
            Return Me._outcome
        End Get
        Set(ByVal value As MeasurandOutcome)
            Me._outcome = value
        End Set
    End Property

    ''' <summary>Gets or sets the reading text.</summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Public Overrides Property Reading() As String
        Get
            Return MyBase.Reading
        End Get
        Set(ByVal value As String)
            MyBase.Reading = value
            Me._outcome.HasValue = True
            If String.IsNullOrWhiteSpace(value) Then
                Me._outcome.IsValid = False
                Me._outcome.HasValue = False
            ElseIf Me.Parse(value) Then
                Me._lastValidReading = value
                Me._outcome.IsValid = True
            Else
                Me._outcome.IsValid = False
            End If
        End Set
    End Property

#End Region

End Class

