''' <summary>
''' Implements a double measurand value.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' Created
''' </history>
Public Class MeasurandDouble
    Inherits MeasurandReading(Of Double)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs a measured value without specifying the value or 
    ''' its validity, which must be specified for the value to be made valid.</summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Constructs a measured value with a valid new value.</summary>
    ''' <param name="newValue">Specifies the measured value.</param>
    Public Sub New(ByVal newValue As Double)
        MyBase.New(newValue)
    End Sub

    ''' <summary>Constructs a measured value</summary>
    ''' <param name="newValue">Specifies the measured value.</param>
    ''' <param name="valid">The value auto settings mode</param>
    Public Sub New(ByVal newValue As Double, ByVal valid As Boolean)
        MyBase.New(newValue, valid)
    End Sub

    ''' <summary>
    ''' Constructs a copy of an existing value.
    ''' </summary>
    Public Sub New(ByVal model As MeasurandDouble)

        MyBase.New(model)
        If model IsNot Nothing Then
            Me._complianceLimit = model._complianceLimit
            Me._complianceLimitMargin = model._complianceLimitMargin
        End If

    End Sub

#End Region

    Private _complianceLimit As Double
    ''' <summary>Gets or sets the compliance limit for testing if the reading exceeded the
    '''   compliance level.</summary>
    ''' <value>A <see cref="System.Double">Double</see> value</value>
    Public Property ComplianceLimit() As Double
        Get
            Return Me._complianceLimit
        End Get
        Set(ByVal value As Double)
            Me._complianceLimit = value
        End Set
    End Property

    Private _complianceLimitMargin As Double = 0.001
    ''' <summary>Gets or sets the margin of how close will allow the measured value to 
    '''   the compliance limit.  For instance, if the margin is 0.001, the measured
    '''   value must not exceed 99.9% of the compliance limit. The default is 0.001.</summary>
    ''' <value>A <see cref="System.Double">Double</see> value</value>
    Public Property ComplianceLimitMargin() As Double
        Get
            Return Me._complianceLimitMargin
        End Get
        Set(ByVal value As Double)
            Me._complianceLimitMargin = value
        End Set
    End Property

    ''' <summary>
    ''' Sets a new double reading value.
    ''' </summary>
    ''' <param name="newValue"></param>
    ''' <param name="valid">True if the value is valid.</param>
    Public Overrides Sub SetValue(ByVal newValue As Double, ByVal valid As Boolean)
        MyBase.SetValue(newValue, valid)
        If MyBase.Outcome.IsValid Then

            MyBase.Outcome.IsOutcome(MetaOutcomes.Infinity) =
                Math.Abs(newValue - isr.Scpi.Syntax.Infinity) < 1
            MyBase.Outcome.IsOutcome(MetaOutcomes.NegativeInfinity) =
                Math.Abs(newValue - isr.Scpi.Syntax.NegativeInfinity) < 1
            MyBase.Outcome.IsOutcome(MetaOutcomes.NotANumber) =
                Math.Abs(newValue - isr.Scpi.Syntax.NotANumber) < 1

            MyBase.Outcome.HitLevelCompliance = Not (newValue >= Me._complianceLimit) Xor (Me._complianceLimit > 0)
            MyBase.SaveCaption.ScaledValue = Me.Value.Value * MyBase.SaveCaption.Units.ScaleFactor
            MyBase.DisplayCaption.ScaledValue = Me.Value.Value * MyBase.DisplayCaption.Units.ScaleFactor
        End If
    End Sub

    ''' <summary>
    ''' Parses the reading to create the specific reading type in the inherited class.
    ''' </summary>
    ''' <param name="reading">Specifies the reading text.</param>
    ''' <remarks>
    ''' Assumes that reading is a number.
    ''' </remarks>
    Public Overrides Function Parse(ByVal reading As String) As Boolean
        ' convert reading to numeric
        Dim value As Double
        If Double.TryParse(reading, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture, value) Then
            Me.SetValue(value, True)
            Return True
        Else
            Me.SetValue(isr.Scpi.Syntax.NotANumber, False)
            Return False
        End If
    End Function

End Class