﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SCPI Library")> 
<Assembly: AssemblyDescription("SCPI Library")> 
<Assembly: AssemblyCompany("Integrated Scientific Resources")> 
<Assembly: AssemblyProduct("Scpi.Library.2010")> 
<Assembly: AssemblyCopyright("(c) 2008 Integrated Scientific Resources, Inc. All rights reserved.")> 
<Assembly: AssemblyTrademark("Licensed under The MIT License.")> 
<Assembly: CLSCompliant(True)> 

<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("d0209f57-5e28-4f0e-bf9a-011531ad0e90")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
<Assembly: AssemblyVersion("5.1.*")> 

' The file version specifies the version that is deployed.
' Commenting this out makes these versions the same as the Assembly Version.
' <Assembly: AssemblyFileVersion("5.1.0.0")> 
' <Assembly: AssemblyInformationalVersion("5.1.0.0")> 
