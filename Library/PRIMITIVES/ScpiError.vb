''' <summary>
''' Define SCPI error information
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/08/2001" by="David" revision="1.0.646.x">
''' Created
''' </history>
Public Class ScpiError

    Private Shared _failedPasringErrorNumber As Integer = -9998
    ''' <summary>
    ''' Gets or sets the failed parsing error number.
    ''' </summary>
    ''' <value>The failed parsing error number.</value>
    Public Shared Property FailedParsingErrorNumber() As Integer
        Get
            Return ScpiError._failedPasringErrorNumber
        End Get
        Set(ByVal value As Integer)
            ScpiError._failedPasringErrorNumber = value
        End Set
    End Property

    Private Shared _errorNumberNotSpecified As Integer = -9999
    ''' <summary>
    ''' Gets or sets the error number not specified.
    ''' </summary>
    ''' <value>The error number not specified.</value>
    Public Shared Property ErrorNumberNotSpecified() As Integer
        Get
            Return ScpiError._errorNumberNotSpecified
        End Get
        Set(ByVal value As Integer)
            ScpiError._errorNumberNotSpecified = value
        End Set
    End Property

    ''' <summary>
    ''' Constructs a new instance of this class.
    ''' </summary>
    ''' <param name="info"></param>
    Public Sub New(ByVal info As String)
        MyBase.New()
        Me._errorInformation = info
        Me._hasValidInfo = Not String.IsNullOrWhiteSpace(info)
        If Me._hasValidInfo Then
            If Me._errorInformation.Contains(Syntax.Comma) Then
                Dim errorSet() As String = Me._errorInformation.Split(Syntax.Comma.ToCharArray)
                If errorSet.Length > 0 Then
                    Me._errorNumber = errorSet(0)
                    If Not Integer.TryParse(Me._errorNumber, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, Globalization.CultureInfo.CurrentCulture, Me._number) Then
                        Me._number = _failedPasringErrorNumber
                    End If
                    If errorSet.Length > 1 Then
                        Me._description = errorSet(1)
                    Else
                        Me._description = "Error information not specified"
                    End If
                Else
                    Me._number = ScpiError._errorNumberNotSpecified
                    Me._errorNumber = "Not Specified"
                    Me._description = Me._errorInformation
                End If
            Else
                Me._number = ScpiError._errorNumberNotSpecified
                Me._errorNumber = "Not Specified"
                Me._description = Me._errorInformation
            End If
        Else
        End If
    End Sub

    Private _hasValidInfo As Boolean
    ''' <summary>
    ''' Gets the condition for determining if the error has valid information.
    ''' </summary>
    Public ReadOnly Property HasValidInfo() As Boolean
        Get
            Return Me._hasValidInfo
        End Get
    End Property

    Private _number As Integer
    ''' <summary>
    ''' Gets the error number.
    ''' </summary>
    Public ReadOnly Property Number() As Integer
        Get
            Return Me._number
        End Get
    End Property

    Private _errorNumber As String
    Public ReadOnly Property ErrorNumber() As String
        Get
            Return Me._errorNumber
        End Get
    End Property

    Private _description As String
    ''' <summary>
    '''  Gets the error description
    ''' </summary>
    Public ReadOnly Property Description() As String
        Get
            Return Me._description
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the full error text.
    ''' </summary>
    Private _errorInformation As String

    ''' <summary>
    ''' Gets the full error text.
    ''' </summary>
    Public ReadOnly Property Information() As String
        Get
            Return Me._errorInformation
        End Get
    End Property

End Class

