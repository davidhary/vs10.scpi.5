''' <summary>Defines a SCPI Base Subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public NotInheritable Class Syntax

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Private Sub New()
        ' instantiate the base class
        MyBase.New()
    End Sub

#End Region

#Region " FORMAT CONSTANTS "

    ''' <summary>Gets or sets the SCPI value for infinity</summary>
    Public Const Infinity As Double = 9.9E+37

    ''' <summary>Gets or sets the SCPI caption for infinity</summary>
    Public Const InfinityCaption As String = "9.90000E+37"

    ''' <summary>Gets or sets the SCPI value for negative infinity</summary>
    Public Const NegativeInfinity As Double = -9.9E+37

    ''' <summary>Gets or sets the SCPI caption for negative infinity</summary>
    Public Const NegativeInfinityCaption As String = "-9.90000E+37"

    ''' <summary>Gets or sets the SCPI value for 'non-a-number' (NAN)</summary>
    Public Const NotANumber As Double = 9.91E+37

    ''' <summary>Gets or sets the SCPI caption for 'not-a-number' (NAN)</summary>
    Public Const NotANumberCaption As String = "9.91000E+37"

#End Region

#Region " IEEE 488.2 STANDARD COMMANDS "

    ''' <summary>
    ''' Gets the Clear Status (CLS) command.
    ''' </summary>
    Public Const ClearExecutionStateCommand As String = "*CLS"

    ''' <summary>
    ''' Gets the Identify query (*IDN?) command.
    ''' </summary>
    Public Const IdentifyQueryCommand As String = "*IDN?"

    ''' <summary>
    ''' Gets the operation complete (*OPC) command.
    ''' </summary>
    Public Const OperationCompletedCommand As String = "*OPC"

    ''' <summary>
    ''' Gets the operation complete query (*OPC?) command.
    ''' </summary>
    Public Const OperationCompletedQueryCommand As String = "*OPC?"

    ''' <summary>
    ''' Gets the options query (*OPT?) command.
    ''' </summary>
    Public Const OptionsQueryCommand As String = "*OPT?"

    ''' <summary>
    ''' Gets the Wait (*WAI) command.
    ''' </summary>
    Public Const WaitCommand As String = "*WAI"

    ''' <summary>
    ''' Gets the Standard Event Enable (*ESE) command.
    ''' </summary>
    Public Const StandardEventEnableCommand As String = "*ESE {0:D}"

    ''' <summary>
    ''' Gets the Standard Event Enable query (*ESE?) command.
    ''' </summary>
    Public Const StandardEventEnableQueryCommand As String = "*ESE?"

    ''' <summary>
    ''' Gets the Standard Event Enable (*ESR?) command.
    ''' </summary>
    Public Const StandardEventStatusQueryCommand As String = "*ESR?"

    ''' <summary>
    ''' Gets the Service Request Enable Enable (*SRE) command.
    ''' </summary>
    Public Const ServiceRequestEnableCommand As String = "*SRE {0:D}"

    ''' <summary>
    ''' Gets the Service Request Enable query (*SRE?) command.
    ''' </summary>
    Public Const ServiceRequestEnableQueryCommand As String = "*SRE?"

    ''' <summary>
    ''' Gets the Service Request Status query (*STB?) command.
    ''' </summary>
    Public Const ServiceRequestStatusQueryCommand As String = "*STB?"

    ''' <summary>
    ''' Gets the reset to know state (*RST) command.
    ''' </summary>
    Public Const ResetKnownStateCommand As String = "*RST"

#End Region

#Region " SCPI SYNTAX WORDS "

    Public Const Colon As String = ":"
    Public Const Comma As String = ","
    Public Const [Space] As String = " "
    Public Const All As String = "ALL"
    Public Const Abort As String = "ABOR"
    Public Const Acceptor As String = "ACC"
    Public Const Actual As String = "ACT"
    Public Const Ascii As String = "ASC"
    Public Const Arm As String = "ARM"
    Public Const [Binary] As String = "BIN"
    Public Const Bus As String = "BUS"
    Public Const Busy As String = "BUSY"
    Public Const Cable As String = "CABL"
    Public Const Calculate As String = "CALC"
    Public Const Calculate1 As String = "CALC1"
    Public Const Calculate2 As String = "CALC2"
    Public Const CLimits As String = "CLIM"
    Public Const ComplianceAbort As String = "CAB"
    Public Const ContactCheck As String = "CCH"
    Public Const Continuity As String = "CONT"
    Public Const [Control] As String = "CONT"
    Public Const Current As String = "CURR"
    Public Const CurrentDC As String = "CURR:DC"
    Public Const CurrentAC As String = "CURR:AC"
    Public Const Delay As String = "DEL"
    Public Const Direction As String = "DIR"
    Public Const Elements As String = "ELEM"
    Public Const Enable As String = "ENAB"
    Public Const [End] As String = "END"
    Public Const EOTest As String = "EOT"
    Public Const Feed As String = "FEED"
    Public Const Fixed As String = "FIX"
    Public Const Forced As String = "FORC"
    Public Const [Format] As String = "FORM"
    Public Const Frequency As String = "FREQ"
    Public Const FResistance As String = "FRES"
    Public Const Front As String = "FRON"
    Public Const Grading As String = "GRAD"
    Public Const Guard As String = "GUAR"
    Public Const [Hex] As String = "HEX"
    Public Const High As String = "HI"
    Public Const HImpedance As String = "HIMP"
    Public Const Immediate As String = "IMM"
    Public Const [In] As String = "IN"
    Public Const Input As String = "INP"
    Public Const InputLine As String = "ILIN"
    Public Const Initiate As String = "INIT"
    Public Const List As String = "LIST"
    Public Const Low As String = "LO"
    Public Const Manual As String = "MAN"
    Public Const Mean As String = "MEAN"
    Public Const Measurement As String = "MEAS"
    Public Const Memory As String = "MEM"
    Public Const Never As String = "NEV"
    Public Const [Next] As String = "NEXT"
    Public Const None As String = "NONE"
    Public Const Normal As String = "NORM"
    Public Const Notify As String = "NOT"
    Public Const Octal As String = "OCT"
    Public Const Ohms As String = "OHMS"
    Public Const [Off] As String = "OFF"
    Public Const [On] As String = "ON"
    Public Const Operation As String = "OPER"
    Public Const [Out] As String = "OUT"
    Public Const Output As String = "OUTP"
    Public Const OutputLine As String = "OLIN"
    Public Const Pattern As String = "PATT"
    Public Const Period As String = "PER"
    Public Const PowerLineCycles As String = "NPLC"
    Public Const PSTest As String = "PST"
    Public Const Protection As String = "PROT"
    Public Const Questionable As String = "QUES"
    Public Const Resistance As String = "RES"
    Public Const Real As String = "REAL"
    Public Const Rear As String = "REAR"
    Public Const Sense As String = "SENS"
    Public Const Sorting As String = "SORT"
    Public Const Source As String = "SOUR"
    Public Const SReal As String = "SRE"
    Public Const Status As String = "STAT"
    Public Const STest As String = "STES"
    Public Const Sweep As String = "SWE"
    Public Const Swap As String = "SWAP"
    Public Const Temperature As String = "TEMP"
    Public Const [Time] As String = "TIME"
    Public Const [Timer] As String = "TIM"
    Public Const TLink As String = "TLIN"
    Public Const Trigger As String = "TRIG"
    Public Const Volt As String = "VOLT"
    Public Const VoltDC As String = "VOLT:DC"
    Public Const VoltAC As String = "VOLT:AC"
    Public Const Zero As String = "ZERO"

#End Region

#Region " MODALITY NAMES "

    ''' <summary>
    ''' Gets the Average modality name.
    ''' </summary>
    Public Const AverageModalityName As String = "AVER"

    ''' <summary>
    ''' Gets the Current modality name.
    ''' </summary>
    Public Const CurrentModalityName As String = "CURR"

    ''' <summary>
    ''' Gets the Delta modality name.
    ''' </summary>
    Public Const DeltaModalityName As String = "DELT"

    ''' <summary>
    ''' Gets the Memory modality name.
    ''' </summary>
    Public Const MemoryModalityName As String = "MEM"

    ''' <summary>
    ''' Gets the password modality name.
    ''' </summary>
    Public Const PasswordModalityName As String = "PASS"

    ''' <summary>
    ''' Gets the resistance modality name.
    ''' </summary>
    Public Const ResistanceModalityName As String = "RES"

    ''' <summary>
    ''' Gets the route modality name.
    ''' </summary>
    Public Const RouteModalityName As String = "ROUT"

    ''' <summary>
    ''' Gets the Sweep modality name.
    ''' </summary>
    Public Const SweepModalityName As String = "SWE"

    ''' <summary>
    ''' Gets the voltage modality name.
    ''' </summary>
    Public Const VoltageModalityName As String = "VOLT"

    ''' <summary>
    ''' Gets the Wave modality name.
    ''' </summary>
    Public Const WaveModalityName As String = "WAVE"

#End Region

#Region " SCPI COMMAND SYNTAX "

    ''' <summary>
    ''' Gets the abort command.
    ''' </summary>
    Public Const AbortCommand As String = "ABOR"

    ''' <summary>
    ''' Gets the arm command.
    ''' </summary>
    Public Const ArmCommand As String = "ARM"

    ''' <summary>
    ''' Gets the arm command.
    ''' </summary>
    Public Const AutoZeroCommand As String = "AZER"

    ''' <summary>
    ''' Gets the auto clear command string.
    ''' </summary>
    Public Const AutoClearCommand As String = "CLE:AUTO"

    ''' <summary>
    ''' Gets the auto Delay command string.
    ''' </summary>
    Public Const AutoDelayCommand As String = "DEL:AUTO"

    ''' <summary>
    ''' Gets the auto range command string.
    ''' </summary>
    Public Const AutoRangeCommand As String = "RANG:AUTO"

    ''' <summary>
    ''' Gets the clear command string.
    ''' </summary>
    Public Const ClearCommand As String = "CLE"

    ''' <summary>
    ''' Gets the compliance command string.
    ''' </summary>
    Public Const ComplianceCommand As String = "COMP"

    ''' <summary>
    ''' Gets the count command string.
    ''' </summary>
    Public Const CountCommand As String = "COUN"

    ''' <summary>
    ''' Gets the data command string.
    ''' </summary>
    Public Const DataCommand As String = "DATA"

    ''' <summary>
    ''' Gets the delay command string.
    ''' </summary>
    Public Const DelayCommand As String = "DEL"

    ''' <summary>
    ''' Gets the fail command string.
    ''' </summary>
    Public Const FailCommand As String = "FAIL"

    ''' <summary>
    ''' Gets the front Switched command string.
    ''' </summary>
    Public Const FrontSwitchedCommand As String = "FRSW"

    ''' <summary>
    ''' Gets the function command string.
    ''' </summary>
    Public Const FunctionCommand As String = "FUNC"

    ''' <summary>
    ''' Gets the high level command.
    ''' </summary>
    Public Const HighCommand As String = "HIGH"

    ''' <summary>
    ''' Gets the initialize command string.
    ''' </summary>
    Public Const InitCommand As String = "INIT"

    ''' <summary>
    ''' Gets the subsystem integration period command string.
    ''' </summary>
    Public Const IntegrationPeriodCommand As String = "NPLC"

    ''' <summary>
    ''' Gets the Line Frequency command string.
    ''' </summary>
    Public Const LineFrequencyCommand As String = "LFR"

    ''' <summary>
    ''' Gets the level command string.
    ''' </summary>
    Public Const LevelCommand As String = "LEV"

    ''' <summary>
    ''' Gets the Lock command string.
    ''' </summary>
    Public Const LockCommand As String = "LOCK"

    ''' <summary>
    ''' Gets the mode command.
    ''' </summary>
    Public Const ModeCommand As String = "MODE"

    ''' <summary>
    ''' Gets the off mode command.
    ''' </summary>
    Public Const OffModeCommand As String = "SMOD"

    ''' <summary>
    ''' Gets the  points command string.
    ''' </summary>
    Public Const PointsCommand As String = "POIN"

    ''' <summary>
    ''' Gets the present query string.
    ''' </summary>
    Public Const PresentCommand As String = "NVPR"

    ''' <summary>
    ''' Gets the preset command string.
    ''' </summary>
    Public Const PresetCommand As String = "PRES"

    ''' <summary>
    ''' Gets the protection limit command string.
    ''' </summary>
    Public Const ProtectionCommand As String = "PROT"

    ''' <summary>
    ''' Gets the subsystem range command string.
    ''' </summary>
    Public Const RangeCommand As String = "RANG"

    ''' <summary>
    ''' Gets the Remote Sense command string.
    ''' </summary>
    Public Const RemoteSenseCommand As String = "RSEN"

    ''' <summary>
    ''' Gets the Selected data command string.
    ''' </summary>
    Public Const SelectedDataCommand As String = "DATA:SEL"

    ''' <summary>
    ''' Gets the source 2 command (level) string.
    ''' </summary>
    Public Const Source2Command As String = "SOUR2"

    ''' <summary>
    ''' Gets the state command string.
    ''' </summary>
    Public Const StateCommand As String = "STAT"

    ''' <summary>
    ''' Gets the start command string.
    ''' </summary>
    Public Const StartLevelCommand As String = "STAR"

    ''' <summary>
    ''' Gets the stop command string.
    ''' </summary>
    Public Const StopLevelCommand As String = "STOP"

    ''' <summary>
    ''' Gets the subsystem terminals command string.
    ''' </summary>
    Public Const TerminalsCommand As String = "TERM"

    ''' <summary>
    ''' Gets the Version command string.
    ''' </summary>
    Public Const VersionCommand As String = "VERS"

    ''' <summary>
    ''' Gets the Unlock command string.
    ''' </summary>
    Public Const UnlockCommand As String = "UNL"

#End Region

#Region " REGISTER COMMANDS "

    ''' <summary>
    ''' Gets the event condition command string.
    ''' </summary>
    Public Const EventConditionCommand As String = "COND"

    ''' <summary>
    ''' Gets the event status command string.
    ''' </summary>
    Public Const EventStatusCommand As String = "EVEN"

    ''' <summary>
    ''' Gets the event status command string.
    ''' </summary>
    Public Const EventEnableCommand As String = "ENAB"

    ''' <summary>
    ''' Gets the negative transition event enable command string.
    ''' </summary>
    Public Const NegativeTransitionEventEnableCommand As String = "NTR"

    ''' <summary>
    ''' Gets the negative transition event enable command string.
    ''' </summary>
    Public Const PositiveTransitionEventEnableCommand As String = "PTR"

#End Region

#Region " COMMAND BUILDERS: EXECUTE "

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="scpiCommand">Specifies the command sub header for a command string for the command form ':{0}' where 
    ''' the first item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Shared Function BuildExecute(ByVal scpiCommand As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}", scpiCommand)
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref> 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Shared Function BuildExecute(ByVal systemName As String, ByVal subHeader As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1}", systemName, subHeader)
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, and
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Shared Function BuildExecute(ByVal systemName As String, ByVal modalityName As String, 
                                        ByVal subHeader As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1}:{2}", systemName, modalityName, subHeader)
    End Function

#End Region

#Region " COMMAND BUILDERS: QUERIES "

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}?' where 
    ''' the first item is the <paramref name="command">command</paramref>.
    ''' </param>
    Public Shared Function BuildQuery(ByVal subHeader As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}?", subHeader)
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}?' where 
    ''' the first item is the <paramref name="systemName">system name</paramref> and 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Shared Function BuildQuery(ByVal systemName As String, ByVal subHeader As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1}?", systemName, subHeader)
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2}?' where 
    ''' the first item is the <paramref name="systemName">system name</paramref>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, and
    ''' the third item is a <paramref name="command">command</paramref>.
    ''' </param>
    Public Shared Function BuildQuery(ByVal systemName As String, ByVal modalityName As String, 
                                        ByVal subHeader As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1}:{2}?", systemName, modalityName, subHeader)
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}? {2},{3}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref> 
    ''' the second item is a <paramref name="command">command</paramref>.
    ''' the third and item is the <paramref name="first">first item to select</paramref> and the
    ''' last item is the <paramref name="count">item count</paramref>.
    ''' </param>
    ''' <param name="first">First data point/</param>
    ''' <param name="count">Data count</param>
    Public Shared Function BuildQuery(ByVal systemName As String, ByVal subHeader As String, ByVal first As Integer, ByVal count As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1}? {2},{3}", systemName, subHeader, first, count)
    End Function

#End Region

#Region " COMMAND BUILDERS: BOOLEAN COMMAND "

    ''' <summary>
    ''' Returns true if the value matches the true value.
    ''' </summary>
    ''' <param name="value">Value read from the instrument.</param>
    ''' <param name="trueValue">The value expected for a true reply</param>
    Public Shared Function ParseBoolean(ByVal value As String, ByVal trueValue As String) As Nullable(Of Boolean)
        Return Not String.IsNullOrWhiteSpace(value) AndAlso value.StartsWith(trueValue, StringComparison.Ordinal)
    End Function

    ''' <summary>
    ''' Returns a boolean or no value.
    ''' </summary>
    ''' <param name="value">Value read from the instrument.</param>
    Public Shared Function ParseBoolean(ByVal value As String) As Nullable(Of Boolean)
        If String.IsNullOrWhiteSpace(value) Then
            Return New Nullable(Of Boolean)
        ElseIf value.StartsWith("0", StringComparison.Ordinal) Then
            Return False
        ElseIf value.StartsWith("1", StringComparison.Ordinal) Then
            Return True
        ElseIf value.StartsWith("OFF", StringComparison.OrdinalIgnoreCase) Then
            Return False
        ElseIf value.StartsWith("ON", StringComparison.OrdinalIgnoreCase) Then
            Return True
        ElseIf value.StartsWith("TRUE", StringComparison.OrdinalIgnoreCase) Then
            Return True
        ElseIf value.StartsWith("FALSE", StringComparison.OrdinalIgnoreCase) Then
            Return False
        ElseIf value.StartsWith("T", StringComparison.OrdinalIgnoreCase) Then
            Return True
        ElseIf value.StartsWith("F", StringComparison.OrdinalIgnoreCase) Then
            Return False
        ElseIf value.StartsWith("IN", StringComparison.OrdinalIgnoreCase) Then
            Return True
        ElseIf value.StartsWith("OUT", StringComparison.OrdinalIgnoreCase) Then
            Return False
        ElseIf value.StartsWith("ACC", StringComparison.OrdinalIgnoreCase) Then
            Return True
        ElseIf value.StartsWith("SOUR", StringComparison.OrdinalIgnoreCase) Then
            Return False
        ElseIf value.StartsWith("IMM", StringComparison.OrdinalIgnoreCase) Then
            Return True
        ElseIf value.StartsWith("END", StringComparison.OrdinalIgnoreCase) Then
            Return False
        ElseIf value.StartsWith("GRAD", StringComparison.OrdinalIgnoreCase) Then
            Return True
        ElseIf value.StartsWith("SORT", StringComparison.OrdinalIgnoreCase) Then
            Return False
        ElseIf value.StartsWith("FRON", StringComparison.OrdinalIgnoreCase) Then
            Return True
        ElseIf value.StartsWith("REAR", StringComparison.OrdinalIgnoreCase) Then
            Return False
        ElseIf value.StartsWith("NEXT", StringComparison.OrdinalIgnoreCase) Then
            Return True
        ElseIf value.StartsWith("NEVER", StringComparison.OrdinalIgnoreCase) Then
            Return False
        ElseIf value.StartsWith("CABL", StringComparison.OrdinalIgnoreCase) Then
            Return True
        ElseIf value.StartsWith("OHMS", StringComparison.OrdinalIgnoreCase) Then
            Return False
        Else
            Return New Nullable(Of Boolean)
        End If
    End Function

    ''' <summary>
    ''' Returns the String representing the boolean value in the specified format.
    ''' </summary>
    ''' <param name="value">Specifies the boolean value.</param>
    ''' <param name="boolFormat">Specifies the <see cref="BooleanDataFormat">format</see></param>
    Public Shared Function BuildBooleanValue(ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat) As String
        Select Case boolFormat
            Case BooleanDataFormat.None
                Return String.Empty
            Case BooleanDataFormat.OneZero
                Return Syntax.ImmediateIf(value, "1", "0")
            Case BooleanDataFormat.OnOff
                Return Syntax.ImmediateIf(value, "ON", "OFF")
            Case BooleanDataFormat.TrueFalse
                Return Syntax.ImmediateIf(value, "TRUE", "FALSE")
            Case BooleanDataFormat.InOut
                Return Syntax.ImmediateIf(value, "IN", "OUT")
            Case BooleanDataFormat.AcceptorSource
                Return Syntax.ImmediateIf(value, "ACC", "SOUR")
            Case BooleanDataFormat.ImmediateEnd
                Return Syntax.ImmediateIf(value, "IMM", "END")
            Case BooleanDataFormat.GradingSorting
                Return Syntax.ImmediateIf(value, "GRAD", "SORT")
            Case BooleanDataFormat.FrontRear
                Return Syntax.ImmediateIf(value, "FRON", "READ")
            Case BooleanDataFormat.NextNever
                Return Syntax.ImmediateIf(value, "NEXT", "NEVER")
            Case BooleanDataFormat.CableOhms
                Return Syntax.ImmediateIf(value, "CABL", "OHMS")
            Case Else
                Return String.Empty
        End Select
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0} {1}' where 
    ''' the first item is a <paramref name="command">command</paramref> and 
    ''' the second item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    ''' <param name="boolFormat">Specifies the <see cref="BooleanDataFormat">format</see></param>
    Public Shared Function BuildCommand(ByVal subHeader As String, ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0} {1}", subHeader, Syntax.BuildBooleanValue(value, boolFormat))
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item represents a <paramref name="value">numeric value</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    ''' <param name="boolFormat">Specifies the <see cref="BooleanDataFormat">format</see></param>
    Public Shared Function BuildCommand(ByVal systemName As String, ByVal subHeader As String, ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1} {2}", systemName, subHeader, 
                            Syntax.BuildBooleanValue(value, boolFormat))
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref> and 
    ''' the fourth item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    ''' <param name="boolFormat">Specifies the <see cref="BooleanDataFormat">format</see></param>
    Public Shared Function BuildCommand(ByVal systemName As String, ByVal modalityName As String, ByVal subHeader As String, 
                                        ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1}:{2} {3}", systemName, modalityName, subHeader, 
                            Syntax.BuildBooleanValue(value, boolFormat))
    End Function

#End Region

#Region " COMMAND BUILDERS: STRING COMMAND "

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0} {1}' where 
    ''' the first item is a <paramref name="command">command</paramref> and 
    ''' the second item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Shared Function BuildCommand(ByVal subHeader As String, ByVal value As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0} {1}", subHeader, value)
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Shared Function BuildCommand(ByVal systemName As String, 
                                        ByVal subHeader As String, ByVal value As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1} {2}", systemName, subHeader, value)
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref> and 
    ''' the fourth item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Shared Function BuildCommand(ByVal systemName As String, ByVal modalityName As String, 
                                        ByVal subHeader As String, ByVal value As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1}:{2} {3}", systemName, modalityName, subHeader, value)
    End Function

#End Region

#Region " COMMAND BUILDERS: INTEGER COMMAND "

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0} {1}' where 
    ''' the first item is a <paramref name="command">command</paramref> and 
    ''' the second item item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Shared Function BuildCommand(ByVal subHeader As String, ByVal value As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0} {1}", subHeader, value)
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Shared Function BuildCommand(ByVal systemName As String, ByVal subHeader As String, ByVal value As Integer) As String
        If value >= Integer.MaxValue Then
            Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                                ":{0}:{1} INF", systemName, subHeader)
        Else
            Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                                ":{0}:{1} {2}", systemName, subHeader, value)
        End If
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="modalityName">Specifies the modality name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref> and 
    ''' the fourth item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Shared Function BuildCommand(ByVal systemName As String, ByVal modalityName As String, 
                                        ByVal subHeader As String, ByVal value As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1}:{2} {3}", systemName, modalityName, subHeader, value)
    End Function

#End Region

#Region " COMMAND BUILDERS: DOUBLE COMMAND "

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0} {1}' where 
    ''' the first item is a <paramref name="command">command</paramref> and 
    ''' the second item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Shared Function BuildCommand(ByVal subHeader As String, ByVal value As Double) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0} {1}", subHeader, value)
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax.
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1} {2}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref>, 
    ''' the second item is a <paramref name="command">command</paramref> and 
    ''' the third item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Shared Function BuildCommand(ByVal systemName As String, ByVal subHeader As String, 
                                        ByVal value As Double) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1} {2}", systemName, subHeader, value)
    End Function

    ''' <summary>
    ''' Gets a query command string for a <paramref name="command">SCPI command</paramref> syntax
    ''' </summary>
    ''' <param name="systemName">Specifies the SCPI system name.</param>
    ''' <param name="subHeader">Specifies the command sub header for a command string in the form ':{0}:{1}:{2} {3}' where 
    ''' the first item is the <paramref name="systemName">system name</paramref>, 
    ''' the second item is the <paramref name="modalityName">modality name</paramref>, 
    ''' the third item is a <paramref name="command">command</paramref> and 
    ''' the fourth item represents an instrument <paramref name="value">settings</paramref>.
    ''' </param>
    ''' <param name="value">Specifies the command value.</param>
    Public Shared Function BuildCommand(ByVal systemName As String, ByVal modalityName As String, 
                                        ByVal subHeader As String, ByVal value As Double) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, 
                            ":{0}:{1}:{2} {3}", systemName, modalityName, subHeader, value)
    End Function

#End Region

#Region " DELIMITED SCPI "

    Private Const startScpiDelimiter As Char = "("c
    Private Const endScpiDelimiter As Char = ")"c
    Private Const embeddedScpiFormat As String = startScpiDelimiter & "{0}" & endScpiDelimiter

    ''' <summary>
    ''' Get a delimited SCPI command.  This is used to help parse scpi enumerated values that include
    ''' delimited SCPI command strings in their descriptions.
    ''' </summary>
    Public Shared Function DelimitedScpiCommand(ByVal value As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, embeddedScpiFormat, value)
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the relevant enumerated value.
    ''' </summary>
    ''' <param name="startDelimiter"></param>
    ''' <param name="endDelimiter"></param>
    Public Shared Function ExtractBetween(ByVal value As String, ByVal startDelimiter As Char, ByVal endDelimiter As Char) As String
        If value IsNot Nothing Then
            Dim startingIndex As Integer = value.IndexOf(startDelimiter)
            Dim endingIndex As Integer = value.LastIndexOf(endDelimiter)
            If startingIndex > 0 AndAlso endingIndex > startingIndex Then
                Return value.Substring(startingIndex + 1, endingIndex - startingIndex - 1)
            Else
                Return value
            End If
        Else
            Return ""
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the relevant enumerated value.
    ''' </summary>
    Private Shared Function ExtractScpi(ByVal value As String) As String
        Return ExtractBetween(value, startScpiDelimiter, endScpiDelimiter)
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As Nullable(Of ArmSource)) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return Syntax.ExtractScpi(value.Value)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As ArmSource) As String
        If value = ArmSource.None Then
            Return String.Empty
        Else
            Return Syntax.ExtractScpi(Syntax.GetDescription(value))
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As Nullable(Of FeedSource)) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return Syntax.ExtractScpi(value.Value)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As Nullable(Of OutputOffMode)) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return Syntax.ExtractScpi(value.Value)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As OutputOffMode) As String
        If value = OutputOffMode.None Then
            Return String.Empty
        Else
            Return Syntax.ExtractScpi(Syntax.GetDescription(value))
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As Nullable(Of ReadingElements)) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return Syntax.ExtractScpi(value.Value)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As ReadingElements) As String
        If value = ReadingElements.None Then
            Return String.Empty
        Else
            Return Syntax.ExtractScpi(Syntax.GetDescription(value))
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As Nullable(Of SenseFunctionModes)) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return Syntax.ExtractScpi(value.Value)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As SenseFunctionModes) As String
        If value = SenseFunctionModes.None Then
            Return String.Empty
        Else
            Return Syntax.ExtractScpi(Syntax.GetDescription(value))
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As Nullable(Of SourceFunctionMode)) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return Syntax.ExtractScpi(value.Value)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As SourceFunctionMode) As String
        If value = SourceFunctionMode.None Then
            Return String.Empty
        Else
            Return Syntax.ExtractScpi(Syntax.GetDescription(value))
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As Nullable(Of SweepMode)) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return Syntax.ExtractScpi(value.Value)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As SweepMode) As String
        If value = SweepMode.None Then
            Return String.Empty
        Else
            Return Syntax.ExtractScpi(Syntax.GetDescription(value))
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As Nullable(Of TriggerEvent)) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return Syntax.ExtractScpi(value.Value)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary>
    ''' Extracts a SCPI string from the description of the <paramref name="value">specified value</paramref>.
    ''' </summary>
    Public Shared Function ExtractScpi(ByVal value As TriggerEvent) As String
        Return Syntax.ExtractScpi(Syntax.GetDescription(value))
    End Function

#End Region

#Region " DESCRIPTION PARSERS "

    ''' <summary>
    ''' Gets the <see cref="ComponentModel.DescriptionAttribute"/> of an <see cref="[Enum]"/> type value.
    ''' </summary>
    ''' <param name="value">The <see cref="[Enum]"/> type value.</param>
    ''' <returns>A string containing the text of the <see cref="ComponentModel.DescriptionAttribute"/>.</returns>
    Public Shared Function GetDescription(ByVal value As [Enum]) As String

        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If

        Dim description As String = value.ToString()
        Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(description)
        Dim attributes As ComponentModel.DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(ComponentModel.DescriptionAttribute), False), ComponentModel.DescriptionAttribute())

        If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
            description = attributes(0).Description
        End If
        Return description

    End Function

    ''' <summary>Returns the substring after the last occurrence of the specified string</summary>
    ''' <param name="source">The string to substring.</param>
    ''' <param name="startDelimiter">The start delimiter to search for.</param>
    ''' <param name="endDelimiter">The end delimiter to search for.</param>
    Public Shared Function SubstringBetween(ByVal source As String, ByVal startDelimiter As String, ByVal endDelimiter As String) As String

        If String.IsNullOrWhiteSpace(source) Then
            Return String.Empty
        End If

        If String.IsNullOrWhiteSpace(startDelimiter) Then
            Return String.Empty
        End If

        Dim startLocation As Int32 = source.LastIndexOf(startDelimiter, StringComparison.OrdinalIgnoreCase) + startDelimiter.Length
        Dim endLocation As Int32 = source.LastIndexOf(endDelimiter, StringComparison.OrdinalIgnoreCase)

        If startLocation >= 0 AndAlso startLocation < endLocation Then
            Return source.Substring(startLocation, endLocation - startLocation)
        Else
            Return String.Empty
        End If

    End Function

#End Region

#Region " ESCAPE SEQUENCES "

    ''' <summary>Replace common escape sequences such as '\n' or '\r\ with the 
    '''   corresponding environment characters.</summary>
    ''' <param name="old">The string which characters are replaced.</param>
    Public Shared Function ReplaceCommonEscapeSequences(ByVal old As String) As String
        If old Is Nothing Then
            Return String.Empty
        Else
            Return old.Replace("\n", Convert.ToChar(10)).Replace("\r", Convert.ToChar(13))
        End If
    End Function

    ''' <summary>Replaces environment characters with common escape sequences such as 
    '''   '\n', for new line, or '\r\ for carriage return.</summary>
    ''' <param name="old">The string which characters are replaced.</param>
    Public Shared Function InsertCommonEscapeSequences(ByVal old As String) As String
        If old Is Nothing Then
            Return String.Empty
        Else
            Return old.Replace(Convert.ToChar(10), "\n").Replace(Convert.ToChar(13), "\r")
        End If
    End Function

#End Region

#Region " IMMEDIATE IF "

    ''' <summary>Returns a string selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As String, ByVal falsePart As String) As String
        If condition Then
            Return truePart
        Else
            Return falsePart
        End If
    End Function

    ''' <summary>Returns an Int32 selected value</summary>
    ''' <param name="condition">The condition for selecting the true or false parts.</param>
    ''' <param name="truePart">The part selected if the condition is True.</param>
    ''' <param name="falsePart">The part selected if the condition is false.</param>
    Public Shared Function ImmediateIf(ByVal condition As Boolean, ByVal truePart As Int32, ByVal falsePart As Int32) As Int32
        If condition Then
            Return truePart
        Else
            Return falsePart
        End If
    End Function

#End Region

#Region " PARSERS: NUMERIC "

    ''' <summary>
    ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
    ''' </summary>
    ''' <param name="value">The string containing the value</param>
    ''' <param name="default">The default value</param>
    Public Shared Function Parse(ByVal value As String, ByVal [default] As Double) As Double

        Dim numericValue As Double
        If String.IsNullOrWhiteSpace(value) Then
            Return [default]
        ElseIf Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, 
                      System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
            Return numericValue
        Else
            Return [default]
        End If

    End Function

    ''' <summary>
    ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
    ''' </summary>
    ''' <param name="value">The string containing the value</param>
    ''' <param name="default">The default value</param>
    Public Shared Function Parse(ByVal value As String, ByVal [default] As Double?) As Double?

        Dim numericValue As Double
        If String.IsNullOrWhiteSpace(value) Then
            Return [default]
        ElseIf Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, 
                      System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
            Return numericValue
        Else
            Return [default]
        End If

    End Function

    ''' <summary>
    ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
    ''' </summary>
    ''' <param name="value">The string containing the value</param>
    ''' <param name="default">The default value</param>
    Public Shared Function Parse(ByVal value As String, ByVal [default] As Integer) As Integer

        Dim numericValue As Integer
        If String.IsNullOrWhiteSpace(value) Then
            Return [default]
        ElseIf Integer.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, 
                      System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
            Return numericValue
        Else
            Return [default]
        End If

    End Function

    ''' <summary>
    ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
    ''' </summary>
    ''' <param name="value">The string containing the value</param>
    ''' <param name="default">The default value</param>
    Public Shared Function Parse(ByVal value As String, ByVal [default] As Integer?) As Integer?

        Dim numericValue As Integer
        If String.IsNullOrWhiteSpace(value) Then
            Return [default]
        ElseIf Integer.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, 
                      System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
            Return numericValue
        Else
            Return [default]
        End If

    End Function

    ''' <summary>
    ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
    ''' </summary>
    ''' <param name="value">The string containing the value</param>
    ''' <param name="default">The default value</param>
    Public Shared Function Parse(ByVal value As String, ByVal [default] As Short) As Short

        Dim numericValue As Short
        If String.IsNullOrWhiteSpace(value) Then
            Return [default]
        ElseIf Short.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, 
                      System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
            Return numericValue
        Else
            Return [default]
        End If

    End Function

    ''' <summary>
    ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
    ''' </summary>
    ''' <param name="value">The string containing the value</param>
    ''' <param name="default">The default value</param>
    Public Shared Function Parse(ByVal value As String, ByVal [default] As Short?) As Short?

        Dim numericValue As Short
        If String.IsNullOrWhiteSpace(value) Then
            Return [default]
        ElseIf Short.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, 
                      System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
            Return numericValue
        Else
            Return [default]
        End If

    End Function

    ''' <summary>
    ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
    ''' </summary>
    ''' <param name="value">The string containing the value</param>
    ''' <param name="default">The default value</param>
    Public Shared Function Parse(ByVal value As String, ByVal [default] As Single) As Single

        Dim numericValue As Single
        If String.IsNullOrWhiteSpace(value) Then

            Return [default]

        ElseIf Single.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, 
                      System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
            Return numericValue
        Else
            Return [default]
        End If

    End Function

    ''' <summary>
    ''' Parse and return the parsed <paramref name="default">default</paramref> if failed to parse.
    ''' </summary>
    ''' <param name="value">The string containing the value</param>
    ''' <param name="default">The default value</param>
    Public Shared Function Parse(ByVal value As String, ByVal [default] As Single?) As Single?

        Dim numericValue As Single
        If String.IsNullOrWhiteSpace(value) Then

            Return [default]

        ElseIf Single.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, 
                      System.Globalization.CultureInfo.CurrentCulture, numericValue) Then
            Return numericValue
        Else
            Return [default]
        End If

    End Function

    ''' <summary>
    ''' Remove unit characters from SCPI data.
    ''' Some instruments append units to the end of the fetched values.
    ''' This methods removes alpha characters as well as the number sign which
    ''' the 2700 appends to the reading number.
    ''' </summary>
    ''' <param name="data"></param>
    Public Shared Function TrimUnits(ByVal data As String) As String
        Const unitCharacters As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ#"
        Dim dataBuilder As New System.Text.StringBuilder
        If Not String.IsNullOrWhiteSpace(data) Then
            If data.Contains(",") Then
                For Each dataElement As String In data.Split(","c)
                    Scpi.Syntax.AddWord(dataBuilder, dataElement.TrimEnd(unitCharacters.ToCharArray))
                Next
            Else
                dataBuilder.Append(data.TrimEnd(unitCharacters.ToCharArray))
            End If
        End If
        Return dataBuilder.ToString
    End Function

#End Region

#Region " PARSERS: ENUMS "

    ''' <summary>
    ''' Returns the <see cref="ArmSource"></see> from the specified value.
    ''' </summary>
    Public Shared Function ParseArmSource(ByVal value As String) As ArmSource
        If String.IsNullOrWhiteSpace(value) Then
            Return ArmSource.None
        Else
            Dim se As New StringEnumerator(Of ArmSource)
            Return se.ParseContained(Syntax.DelimitedScpiCommand(value))
        End If
    End Function

    ''' <summary>
    ''' Returns the <see cref="FeedSource"></see> from the specified value.
    ''' </summary>
    Public Shared Function ParseFeedSource(ByVal value As String) As FeedSource
        If String.IsNullOrWhiteSpace(value) Then
            Return FeedSource.None
        Else
            Dim se As New StringEnumerator(Of FeedSource)
            Return se.ParseContained(Syntax.DelimitedScpiCommand(value))
        End If
    End Function

    ''' <summary>
    ''' Returns the <see cref="OutputOffMode"></see> from the specified value.
    ''' </summary>
    Public Shared Function ParseOutputOffMode(ByVal value As String) As OutputOffMode
        If String.IsNullOrWhiteSpace(value) Then
            Return OutputOffMode.None
        Else
            Dim se As New StringEnumerator(Of OutputOffMode)
            Return se.ParseContained(Syntax.DelimitedScpiCommand(value))
        End If
    End Function

    ''' <summary>
    ''' Returns the <see cref="ReadingElements"></see> from the specified value.
    ''' </summary>
    Public Shared Function ParseReadingElements(ByVal value As String) As ReadingElements
        If String.IsNullOrWhiteSpace(value) Then
            Return ReadingElements.None
        Else
            Dim se As New StringEnumerator(Of ReadingElements)
            Return se.ParseContained(Syntax.DelimitedScpiCommand(value))
        End If
    End Function

    ''' <summary>
    ''' Returns the <see cref="SenseFunctionModes"></see> from the specified value.
    ''' </summary>
    Public Shared Function ParseSenseFunctionModes(ByVal value As String) As SenseFunctionModes
        If String.IsNullOrWhiteSpace(value) Then
            Return SenseFunctionModes.None
        Else
            Dim se As New StringEnumerator(Of SenseFunctionModes)
            Return se.ParseContained(Syntax.DelimitedScpiCommand(value))
        End If
    End Function

    ''' <summary>
    ''' Returns the enumerated function corresponding to the function name.
    ''' </summary>
    ''' <param name="value">Specifies the function.</param>
    Public Shared Function ParseSenseFunctionMode(ByVal value As String) As Scpi.SenseFunctionModes
        If String.IsNullOrWhiteSpace(value) Then
            Return SenseFunctionModes.None
        Else
            Dim se As New StringEnumerator(Of SenseFunctionModes)
            Return se.ParseContained(Syntax.DelimitedScpiCommand(value.Replace(""""c, "'"c)))
        End If
    End Function

    ''' <summary>
    ''' Returns the <see cref="SourceFunctionMode"></see> from the specified value.
    ''' </summary>
    Public Shared Function ParseSourceFunctionMode(ByVal value As String) As SourceFunctionMode
        If String.IsNullOrWhiteSpace(value) Then
            Return SourceFunctionMode.None
        Else
            Dim se As New StringEnumerator(Of SourceFunctionMode)
            Return se.ParseContained(Syntax.DelimitedScpiCommand(value))
        End If
    End Function

    ''' <summary>
    ''' Returns the <see cref="SweepMode"></see> from the specified value.
    ''' </summary>
    Public Shared Function ParseSweepMode(ByVal value As String) As SweepMode
        If String.IsNullOrWhiteSpace(value) Then
            Return SweepMode.None
        Else
            Dim se As New StringEnumerator(Of SweepMode)
            Return se.ParseContained(Syntax.DelimitedScpiCommand(value))
        End If
    End Function

    ''' <summary>
    ''' Returns the <see cref="SweepMode"></see> from the specified value.
    ''' </summary>
    Public Shared Function ParseTriggerEvent(ByVal value As String) As TriggerEvent
        If String.IsNullOrWhiteSpace(value) Then
            Return TriggerEvent.None
        Else
            Dim se As New StringEnumerator(Of TriggerEvent)
            Return se.ParseContained(Syntax.DelimitedScpiCommand(value))
        End If
    End Function

#End Region

#Region " TEXT BUILDER "

    ''' <summary>
    ''' Adds text to string builder starting with a new line.
    ''' </summary>
    Public Shared Sub AddLine(ByVal builder As System.Text.StringBuilder, ByVal value As String)
        If Not String.IsNullOrWhiteSpace(value) Then
            If builder Is Nothing Then
                builder = New System.Text.StringBuilder
            End If
            If builder.Length > 0 Then
                builder.Append(Environment.NewLine)
            End If
            builder.Append(value)
        End If
    End Sub

    ''' <summary>
    ''' Adds text to string builder starting with a new line.
    ''' </summary>
    Public Shared Sub AddWord(ByVal builder As System.Text.StringBuilder, ByVal value As String)
        If Not String.IsNullOrWhiteSpace(value) Then
            If builder Is Nothing Then
                builder = New System.Text.StringBuilder
            End If
            If builder.Length > 0 Then
                builder.Append(",")
            End If
            builder.Append(value)
        End If
    End Sub

#End Region

End Class
