''' <summary>Provides event arguments for instrument events.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <remarks>The class inherits from <see cref="System.EventArgs">system event arguments</see>.
''' It servers to pass event arguments trough the inheritance tree of SCPI instruments 
''' when processing VISA service requests.
''' </remarks>
''' <history date="08/17/05" by="David" revision="1.0.2420.x">
''' Add error queue.  All getting error information from the parent instrument.
''' </history>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Class DeviceServiceEventArgs
    Inherits BaseServiceEventArgs

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="controller">Reference to a <see cref="Scpi.IDevice">SCPI controller</see></param>
    Public Sub New(ByVal controller As Scpi.IDevice)

        ' instantiate the base class
        MyBase.New()

        Me._controller = controller

    End Sub

    ''' <summary>Constructs this class with a status message.</summary>
    ''' <param name="eventMessage">Specifies the event message.</param>
    ''' <param name="controller">Reference to a <see cref="Scpi.IDevice">SCPI controller</see></param>
    ''' <remarks>Use this constructor to instantiate this class with a status group.</remarks>
    Public Sub New(ByVal eventMessage As String, ByVal controller As Scpi.IDevice)

        ' instantiate the base class
        MyBase.New(eventMessage)

        Me._controller = controller

    End Sub

#End Region

#Region " CONTROLLER "

    Private _controller As Scpi.IDevice

#End Region

#Region " EVENT STATUS READERS "

    ''' <summary>
    ''' Reads the available message.
    ''' </summary>
    Protected Overrides Function ReadLineTrimEnd() As String
        Return Me._controller.ReadLineTrimEnd
    End Function

    ''' <summary>
    ''' Reads the measurement events register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected Overrides Function ReadMeasurementEventStatus() As Integer
        Dim value As Nullable(Of Int32) = StatusSubsystem.MeasurementEventStatus(Me._controller)
        If value IsNot Nothing AndAlso value.HasValue Then
            Return value.Value
        Else
            Return -1
        End If
    End Function

    ''' <summary>
    ''' Reads the operation event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected Overrides Function ReadOperationEventStatus() As Integer
        Return StatusSubsystem.OperationEventStatus(Me._controller)
    End Function

    ''' <summary>
    ''' Reads the questionable event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected Overrides Function ReadQuestionableEventStatus() As Integer
        Return StatusSubsystem.QuestionableEventStatus(Me._controller)
    End Function

    ''' <summary>
    ''' Reads the standard event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected Overrides Function ReadStandardEventStatus() As Integer
        Return Me._controller.ReadStandardEventStatus()
    End Function

    ''' <summary>Reads the standard registers.</summary>
    ''' <remarks>Child classes must implement methods such as 
    ''' <see cref="ReadMeasurementEventStatus">read measurement event status</see>
    ''' must read registers depending on the specific instrument capabilities.</remarks>
    Public Overrides Sub ReadRegisters()
        MyBase.ReadRegisters()
    End Sub

    ''' <summary>
    ''' Reads the event status register.
    ''' </summary>
    Protected Overrides Function ReadStatusByte() As Integer
        Return Me._controller.ReadStatusByte
    End Function

    Protected Overrides Function ReadLastError() As String
        Return SystemSubsystem.ReadLastError(Me._controller)
    End Function

    Protected Overrides Function FetchErrorQueue() As String
        Return StatusSubsystem.FetchErrorQueue(Me._controller)
    End Function

#End Region

#Region " PROCESS EVENT  "

    ''' <summary>Reads and processes the service request register byte.</summary>
    Public Overrides Sub ProcessServiceRequest()
        MyBase.ProcessServiceRequest()
    End Sub

#End Region

End Class

