''' <summary>Provides an interface for all resettable instruments.</summary>
''' <remarks>
''' Used to split the connectible interface so as to use the resettable elements in classes such as SCPI that
''' are not connectible.
''' </remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="03/24/2008" by="David" revision="2.0.3003.x">
''' Created
''' </history>
Public Interface IResettable

    ''' <summary>Clears the queues and resets all registers to zero.</summary>
    Function ClearExecutionState() As Boolean

    ''' <summary>Returns subsystem to its preset values.</summary>
    Function PresetKnownState() As Boolean

    ''' <summary>Returns the device to its default known state.</summary>
    Function ResetKnownState() As Boolean

End Interface

#Region " RESSETABLE COLLECTION "

''' <summary>
''' A collection of resettable elements.
''' </summary>
Public Class ResettableCollection(Of TItem As IResettable)
    Inherits Collections.ObjectModel.Collection(Of TItem)
    Implements IResettable

    Public Overloads Function Add(ByVal item As TItem) As TItem
        MyBase.Add(item)
        Return item
    End Function

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        For Each ressettableItem As IResettable In MyBase.Items
            ressettableItem.ClearExecutionState()
        Next
        Return True
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        For Each ressettableItem As IResettable In MyBase.Items
            ressettableItem.PresetKnownState()
        Next
        Return True
    End Function

    ''' <summary>Restore member properties to the following RST or System Preset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        For Each ressettableItem As IResettable In MyBase.Items
            ressettableItem.ResetKnownState()
        Next
        Return True
    End Function

End Class

#End Region

#Region " RESSETABLE KEYABLE COLLECTION "

''' <summary>
''' A <see cref="Collections.ObjectModel.KeyedCollection">collection</see> of
''' <see cref="IKeyable">Keyable</see> 
''' items keyed by the <see cref="IKeyable.UniqueKey">unique key.</see>
''' </summary>
Public Class ResettableKeyableCollection(Of TItem As {IKeyable, IResettable})
    Inherits Collections.ObjectModel.KeyedCollection(Of String, TItem)
    Implements IResettable

    Protected Overrides Function GetKeyForItem(ByVal item As TItem) As String
        Return item.UniqueKey
    End Function

    Public Overloads Function Add(ByVal item As TItem) As TItem
        MyBase.Add(item)
        Return item
    End Function

    ''' <summary>Clears the queues and resets all registers to zero.
    ''' Sets the subsystem properties to the following CLS default values:<para>
    ''' 
    ''' </para>
    ''' </summary>
    Public Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        For Each limit As TItem In MyBase.Items
            limit.ClearExecutionState()
        Next
        Return True
    End Function

    ''' <summary>
    ''' Sets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    Public Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        For Each limit As TItem In MyBase.Items
            limit.PresetKnownState()
        Next
        Return True
    End Function

    ''' <summary>Restore member properties to the following RST or System Preset values:<para>
    ''' </para>
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        For Each limit As TItem In MyBase.Items
            limit.ResetKnownState()
        Next
        Return True
    End Function

End Class

#End Region
