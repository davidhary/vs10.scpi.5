''' <summary>
''' Provides an interface for handing service requests as part of the service request arguments.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <remarks>The service event arguments class would inherit from 
''' <see cref="System.EventArgs">system event arguments</see>.
''' It would pass event arguments trough the inheritance tree of SCPI instruments 
''' when processing VISA service requests.
''' </remarks>
''' <history date="03/27/08" by="David" revision="2.1.3008.x">
''' Create based on the service request arguments class
''' </history>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public Interface IRequestable

#Region " READ REGISTERS AND MESSAGES "

    ''' <summary>
    ''' Reads the available message.
    ''' </summary>
    Function ReadLineTrimEnd() As String

    ''' <summary>
    ''' Reads the measurement events register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Function ReadMeasurementEventStatus() As Integer

    ''' <summary>
    ''' Reads the operation event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Function ReadOperationEventStatus() As Integer

    ''' <summary>
    ''' Reads the questionable event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Function ReadQuestionableEventStatus() As Integer

    ''' <summary>
    ''' Reads the standard event register status.
    ''' </summary>
    ''' <returns>
    ''' Returns an <see cref="Integer">integer</see> to permit using this 
    ''' interface with different register value interpretations.
    ''' </returns>
    ''' <remarks>Override to get the event status.</remarks>
    Function ReadStandardEventStatus() As Integer '  isr.Visa.Ieee4882.StandardEvents

    ''' <summary>
    ''' Reads the device status byte. 
    ''' </summary>
    ''' <returns>
    ''' Returns an <see cref="Integer">integer</see> to permit using this 
    ''' interface with different register value interpretations.
    ''' </returns>
    Function ReadStatusByte() As Integer '  isr.Visa.Ieee4882.ServiceRequests

    ''' <summary>Returns the last error from the instrument.</summary>
    Function ReadLastError() As String

    ''' <summary>Reads the error queue from the instrument.</summary>
    ''' <remarks>This reflects the real time condition of the instrument.</remarks>
    Function FetchErrorQueue() As String

#End Region

#Region " PROCESS EVENT "

    ''' <summary>Returns a detailed report for the given standard status register (ESR) byte.
    ''' </summary>
    Function BuildStandardStatusRegisterReport() As String

    ''' <summary>Reads the service request register.</summary>
    Sub ProcessServiceRequestRegister()

    ''' <summary>Reads and processes the service request register byte.</summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"></exception>
    Sub ProcessServiceRequest()

    ''' <summary>Reads the standard registers.</summary>
    ''' <remarks>Child classes must implement methods such as 
    ''' <see cref="ReadMeasurementEventStatus">read measurement event status</see>
    ''' must read registers depending on the specific instrument capabilities.</remarks>
    Sub ReadRegisters()

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Holds the status register bits that flag an error.  Default to 
    ''' <see cref="ServiceRequests.ErrorAvailable">Error Available</see>.
    ''' </summary>
    ''' <returns>
    ''' Returns an <see cref="Integer">integer</see> to permit using this 
    ''' interface with different register value interpretations.
    ''' </returns>
    Property ErrorAvailableBits() As Integer ' isr.Visa.Ieee4882.ServiceRequests

    ''' <summary>Returns the event message.</summary>
    ReadOnly Property EventMessage() As String

    ''' <summary>Gets or sets the event time.</summary>
    ReadOnly Property EventTime() As DateTime

    ''' <summary>Gets or sets the condition for the service request status error flag is on.</summary>
    Property HasError() As Boolean

    ''' <summary>
    ''' Holds true if error handling is to be delegated to the parent instrument.
    ''' </summary>
    Property IsDelegateErrorHandling() As Boolean

    ''' <summary>Returns the last error read from the instrument.</summary>
    ReadOnly Property LastError() As String

    ''' <summary>Returns the last error queue read from the instrument.</summary>
    ReadOnly Property LastErrorQueue() As String

    ''' <summary>Returns the error queue and ESR report.
    ''' </summary>
    ReadOnly Property LastErrorReport() As String

    ReadOnly Property LastStandardStatusRegisterReport() As String

    Property ServicingRequest() As Boolean

    ''' <summary>Returns the measurement Event register status.  To decipher, cast to the event status
    '''   of the specific instrument type.</summary>
    ReadOnly Property MeasurementEventStatus() As Integer

    Property OperationCompleted() As Boolean

    ''' <summary>Gets or sets the operation condition status. To decipher, cast to the event status
    '''   of the specific instrument type.</summary>
    Property OperationCondition() As Integer

    ''' <summary>Returns the operation elapsed time.</summary>
    ReadOnly Property OperationElapsedTime() As TimeSpan

    ''' <summary>Returns the operation time.  This is typically set to when the 
    '''   operation started so the operation can be timed to its event time.</summary>
    ReadOnly Property OperationTime() As DateTime

    ''' <summary>Returns the operation event register status.  To decipher, cast to the event status
    '''   of the specific instrument type.</summary>
    ReadOnly Property OperationEventStatus() As Integer

    ''' <summary>Returns the Questionable Event register status. To decipher, cast to the event status
    '''   of the specific instrument type.</summary>
    ReadOnly Property QuestionableEventStatus() As Integer

    ''' <summary>Returns the message that was received from the instrument.</summary>
    ReadOnly Property ReceivedMessage() As String

    ''' <summary>Returns the number of service request that were recorded since the
    '''   last Clear Status command.</summary>
    ReadOnly Property RequestCount() As Long

    ''' <summary>Returns the service request  (status byte).</summary>
    ''' <returns>
    ''' Returns an <see cref="Integer">integer</see> to permit using this 
    ''' interface with different register value interpretations.
    ''' </returns>
    ReadOnly Property ServiceRequestStatusBits() As Integer ' isr.Visa.Ieee4882.ServiceRequests

    ''' <summary>Returns the standard event register</summary>
    ''' <returns>
    ''' Returns an <see cref="Integer">integer</see> to permit using this 
    ''' interface with different register value interpretations.
    ''' </returns>
    ReadOnly Property StandardEventStatusBits() As Integer '  isr.Visa.Ieee4882.StandardEvents

#End Region

#Region " EVENT and HANDLERS "

    ''' <summary>Raised upon receiving a request to have the instrument retrieve error information.
    ''' This is done in case the instrument does not comply with standard SCPI methods for retrieving
    ''' the error queue of standard event register errors.
    ''' </summary>
    Event ErrorInfoRequested As EventHandler(Of BaseServiceEventArgs) ' System.EventArgs)

    ''' <summary>Raises the request for error information.</summary>
    ''' <param name="e">Passes reference to the <see cref="BaseServiceEventArgs">SCPI service request event arguments</see>.</param>
    Sub OnErrorInfoRequested(ByVal e As BaseServiceEventArgs)

#End Region

End Interface
