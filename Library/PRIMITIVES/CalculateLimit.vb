''' <summary>
''' Defines a limit for the SCPI Calculate SCPI subsystem
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/08/2001" by="David" revision="1.0.646.x">
''' Created
''' </history>
Public Class CalculateLimit
    Implements IResettable, IKeyable

    Public Sub New(ByVal syntaxHeader As String)
        MyBase.New()
        Me._syntaxHeader = syntaxHeader

        Me._resettableValues = New ResettableCollection(Of IResettable)
        Me._complianceBits = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._ComplianceBits)
        Me._incomplianceFailCondition = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._IncomplianceFailCondition)
        Me._failureBits = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._FailureBits)
        Me._limitFailed = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._LimitFailed)
        Me._lowerLimit = New ResettableDouble
        Me._resettableValues.Add(Me._LowerLimit)
        Me._lowerLimitFailureBits = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._LowerLimitFailureBits)
        Me._passBits = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._PassBits)
        Me._state = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._State)
        Me._upperLimit = New ResettableDouble
        Me._resettableValues.Add(Me._UpperLimit)
        Me._upperLimitFailureBits = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._upperLimitFailureBits)

        Me.ComplianceBits.ResetValue = 15
        Me.IncomplianceFailCondition = True
        Me.FailureBits.ResetValue = 15
        Me.LowerLimit.ResetValue = -1
        Me.LowerLimitFailureBits.ResetValue = 15
        Me.PassBits.ResetValue = 15
        Me.State.ResetValue = False
        Me.UpperLimit.ResetValue = 1
        Me.UpperLimitFailureBits.ResetValue = 15

    End Sub

#Region " IRESETTABLE "

    ''' <summary>
    ''' Holds all resettable values for this function.
    ''' </summary>
    Private _resettableValues As ResettableCollection(Of IResettable)

    ''' <summary>Clears the queues and resets all registers to zero.</summary>
    Public Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        Return Me._resettableValues.ClearExecutionState()
    End Function

    ''' <summary>Returns subsystem to its preset values.</summary>
    Public Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        Return Me._resettableValues.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Preset values:<para>
    ''' </para>
    ''' </summary>
    Public Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        Return Me._resettableValues.ResetKnownState()
    End Function

#End Region

    ''' <summary>
    '''  Gets or sets the Output fail bit pattern for compliance (15)
    ''' </summary>
    Public Property ComplianceBits() As ResettableValue(Of Integer)

    ''' <summary>
    ''' Gets or sets the Compliance fail condition (In/Out) (in)
    ''' </summary>
    Public Property IncomplianceFailCondition() As ResettableValue(Of Boolean)

    ''' <summary>
    ''' Gets or sets the Output fail bit pattern (15)
    ''' </summary>
    Public Property FailureBits() As ResettableValue(Of Integer)

    ''' <summary>
    '''  Gets or sets the limit failure status.
    ''' </summary>
    Public Property LimitFailed() As ResettableValue(Of Boolean)

    ''' <summary>
    ''' Gets or sets the Lower limit (-1)
    ''' </summary>
    Public Property LowerLimit() As ResettableDouble

    ''' <summary>
    ''' Gets or sets the Output fail bit pattern for lower limit (15)
    ''' </summary>
    Public Property LowerLimitFailureBits() As ResettableValue(Of Integer)

    ''' <summary>
    ''' Gets or sets the Output pass bit pattern (15)
    ''' </summary>
    Public Property PassBits() As ResettableValue(Of Integer)

    ''' <summary>
    ''' Gets or sets the Limit on/off state (off).
    ''' </summary>
    Public Property State() As ResettableValue(Of Boolean)

    ''' <summary>
    ''' Gets or sets the header is used in the SCPI commands.
    ''' </summary>
    Public Property SyntaxHeader() As String Implements IKeyable.UniqueKey

    ''' <summary>
    ''' Gets or sets the Upper limit (1)
    ''' </summary>
    Public Property UpperLimit() As ResettableDouble

    ''' <summary>
    ''' Gets or sets the Output fail bit pattern for upper limit (15)
    ''' </summary>
    Public Property UpperLimitFailureBits() As ResettableValue(Of Integer)

End Class
