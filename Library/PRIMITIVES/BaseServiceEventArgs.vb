''' <summary>Provides event arguments for instrument events.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <remarks>The class inherits from <see cref="System.EventArgs">system event arguments</see>.
''' It servers to pass event arguments trough the inheritance tree of SCPI instruments 
''' when processing VISA service requests.
''' </remarks>
''' <history date="08/17/05" by="David" revision="1.0.2420.x">
''' Add error queue.  All getting error information from the parent instrument.
''' </history>
''' <history date="01/15/05" by="David" revision="1.0.1841.x">
''' Created
''' </history>
Public MustInherit Class BaseServiceEventArgs
    Inherits System.EventArgs
    Implements IRequestable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Protected Sub New()

        ' instantiate the base class
        MyBase.New()

        ' Date.Now set the member properties.
        Me._operationTime = Date.Now

    End Sub

    ''' <summary>Constructs this class with a status message.</summary>
    ''' <param name="eventMessage">Specifies the event message.</param>
    ''' <remarks>Use this constructor to instantiate this class with a status group.</remarks>
    Protected Sub New(ByVal eventMessage As String)

        ' instantiate the base class
        Me.New()

        ' Date.Now set the member properties.
        Me._eventMessage = eventMessage

    End Sub

#End Region

#Region " MUST OVERRIDE READ REGISTERS AND MESSAGES "

    ''' <summary>
    ''' Reads the available message.
    ''' </summary>
    Protected MustOverride Function ReadLineTrimEnd() As String Implements IRequestable.ReadLineTrimEnd

    ''' <summary>
    ''' Reads the measurement events register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected MustOverride Function ReadMeasurementEventStatus() As Integer Implements IRequestable.ReadMeasurementEventStatus

    ''' <summary>
    ''' Reads the operation event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected MustOverride Function ReadOperationEventStatus() As Integer Implements IRequestable.ReadOperationEventStatus

    ''' <summary>
    ''' Reads the Questionable event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected MustOverride Function ReadQuestionableEventStatus() As Integer Implements IRequestable.ReadQuestionableEventStatus

    ''' <summary>
    ''' Reads the standard event register status.
    ''' </summary>
    ''' <remarks>Override to get the event status.</remarks>
    Protected MustOverride Function ReadStandardEventStatus() As Integer Implements IRequestable.ReadStandardEventStatus

    ''' <summary>
    ''' Reads the device status byte.
    ''' </summary>
    Protected MustOverride Function ReadStatusByte() As Integer Implements IRequestable.ReadStatusByte

    Protected MustOverride Function ReadLastError() As String Implements IRequestable.ReadLastError

    Protected MustOverride Function FetchErrorQueue() As String Implements IRequestable.FetchErrorQueue

#End Region

#Region " PROCESS EVENT ARGUMENTS "

    ''' <summary>Reads the standard registers.</summary>
    ''' <remarks>Child classes must implement methods such as 
    ''' <see cref="ReadMeasurementEventStatus">read measurement event status</see>
    ''' must read registers depending on the specific instrument capabilities.</remarks>
    Public Overridable Sub ReadRegisters() Implements IRequestable.ReadRegisters

        If (Me._serviceRequestStatus And ServiceRequests.MeasurementEvent) <> 0 Then
            Me._measurementEventStatus = ReadMeasurementEventStatus()
        End If
        If (Me._serviceRequestStatus And ServiceRequests.MessageAvailable) <> 0 Then
            Me._receivedMessage = ReadLineTrimEnd()
        End If
        If (Me._serviceRequestStatus And ServiceRequests.OperationEvent) <> 0 Then
            Me._operationEventStatus = ReadOperationEventStatus()
        End If
        If (Me._serviceRequestStatus And ServiceRequests.QuestionableEvent) <> 0 Then
            Me._questionableEventStatus = ReadQuestionableEventStatus()
        End If
        If (Me._serviceRequestStatus And ServiceRequests.RequestingService) <> 0 Then
        End If
        If (Me._serviceRequestStatus And ServiceRequests.StandardEvent) <> 0 Then
            Me._standardEventStatus = CType(ReadStandardEventStatus(), StandardEvents)
        End If
    End Sub

    ''' <summary>Reads and processes the service request register byte.</summary>
    Public Overridable Sub ProcessServiceRequestRegister() Implements IRequestable.ProcessServiceRequestRegister
        Me._serviceRequestStatus = CType(ReadStatusByte(), ServiceRequests)
    End Sub

    ''' <summary>Reads and processes the service request register byte.</summary>
    Public Overridable Sub ProcessServiceRequest() Implements IRequestable.ProcessServiceRequest

        ProcessServiceRequestRegister()
        Me._eventTime = Date.Now
        If Me._requestCount >= Long.MaxValue Then
            Me._requestCount = 0
        End If
        Me._requestCount += 1
        If Me.HasError Then
            If Me._isDelegateErrorHandling Then
                Me.OnErrorInfoRequested(Me)
            Else
                Me._lastError = ReadLastError()
                Me._lastErrorQueue = FetchErrorQueue()
            End If
        Else
            ' if not an error, clear the error queue
            Me._lastError = String.Empty
            Me.HasError = False
        End If
    End Sub

    ''' <summary>Returns a detailed report for the given standard status register (ESR) byte.
    ''' </summary>
    Public Function BuildStandardStatusRegisterReport() As String Implements IRequestable.BuildStandardStatusRegisterReport

        Dim delimiter As String
        delimiter = "; "
        Dim messageBuilder As New System.Text.StringBuilder

        ' operation complete
        If (Me._standardEventStatus And StandardEvents.OperationComplete) = StandardEvents.OperationComplete Then
            If messageBuilder.Length > 0 Then
                messageBuilder.Append(delimiter)
            End If
            messageBuilder.Append("Operation Complete")
        End If

        ' Command error
        If (Me._standardEventStatus And StandardEvents.CommandError) = StandardEvents.CommandError Then
            If messageBuilder.Length > 0 Then
                messageBuilder.Append(delimiter)
            End If
            messageBuilder.Append("Command error")
        End If

        ' device-dependent error
        If (Me._standardEventStatus And StandardEvents.DeviceDependentError) = StandardEvents.DeviceDependentError Then
            If messageBuilder.Length > 0 Then
                messageBuilder.Append(delimiter)
            End If
            messageBuilder.Append("Device-dependent error")
        End If

        ' Execution error
        If (Me._standardEventStatus And StandardEvents.ExecutionError) = StandardEvents.ExecutionError Then
            If messageBuilder.Length > 0 Then
                messageBuilder.Append(delimiter)
            End If
            messageBuilder.Append("Execution error")
        End If

        ' power on
        If (Me._standardEventStatus And StandardEvents.PowerToggled) = StandardEvents.PowerToggled Then
            If messageBuilder.Length > 0 Then
                messageBuilder.Append(delimiter)
            End If
            messageBuilder.Append("Power toggled")
        End If

        ' query error
        If (Me._standardEventStatus And StandardEvents.QueryError) = StandardEvents.QueryError Then
            If messageBuilder.Length > 0 Then
                messageBuilder.Append(delimiter)
            End If
            messageBuilder.Append("Query error")
        End If

        ' User request
        If (Me._standardEventStatus And StandardEvents.UserRequest) = StandardEvents.UserRequest Then
            If messageBuilder.Length > 0 Then
                messageBuilder.Append(delimiter)
            End If
            messageBuilder.Append("User request")
        End If

        If messageBuilder.Length > 0 Then
            messageBuilder.Insert(0, "The device standard status register reported: ")
            messageBuilder.Append(".")
        End If

        BuildStandardStatusRegisterReport = messageBuilder.ToString

    End Function

#End Region

#Region " PROPERTIES "

    Private _errorAvailableBits As ServiceRequests = ServiceRequests.ErrorAvailable
    ''' <summary>
    ''' Gets or sets the status register bits that flag an error.  Defaults to 
    ''' <see cref="ServiceRequests.ErrorAvailable">Error Available</see>.
    ''' </summary>
    Public Property ErrorAvailableBits() As Integer Implements IRequestable.ErrorAvailableBits
        Get
            Return Me._errorAvailableBits
        End Get
        Set(ByVal value As Integer)
            Me._errorAvailableBits = CType(value, ServiceRequests)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the status register bits that flag an error.  Defaults to 
    ''' <see cref="ServiceRequests.ErrorAvailable">Error Available</see>.
    ''' This is the typed value of <see cref="ErrorAvailableBits">error available bits</see>
    ''' </summary>
    Public Property ErrorAvailableServiceRequest() As ServiceRequests
        Get
            Return Me._errorAvailableBits
        End Get
        Set(ByVal value As ServiceRequests)
            Me._errorAvailableBits = value
        End Set
    End Property

    Private _eventMessage As String = ""
    ''' <summary>Returns the event message.</summary>
    Public ReadOnly Property EventMessage() As String Implements IRequestable.EventMessage
        Get
            Return Me._eventMessage
        End Get
    End Property

    Private _eventTime As DateTime = Date.Now
    ''' <summary>Gets or sets the event time.</summary>
    Public ReadOnly Property EventTime() As DateTime Implements IRequestable.EventTime
        Get
            Return Me._eventTime
        End Get
    End Property

    ''' <summary>Gets or sets the condition for the service request status error flag is on.</summary>
    Public Property HasError() As Boolean Implements IRequestable.HasError
        Get
            ' reset the consecutive request count because calling this indicates the service
            ' request was handled.
            Me._requestCount = 0
            Return (Me._serviceRequestStatus And Me._errorAvailableBits) <> 0
        End Get
        Set(ByVal value As Boolean)
            If value Then
                Me._serviceRequestStatus = Me._serviceRequestStatus Or Me._errorAvailableBits
            Else
                Me._serviceRequestStatus = Me._serviceRequestStatus And (Not Me._errorAvailableBits)
            End If
        End Set
    End Property

    Private _isDelegateErrorHandling As Boolean
    ''' <summary>
    ''' Holds true if error handling is to be delegated to the parent instrument.
    ''' </summary>
    Public Property IsDelegateErrorHandling() As Boolean Implements IRequestable.IsDelegateErrorHandling
        Get
            Return Me._isDelegateErrorHandling
        End Get
        Set(ByVal value As Boolean)
            Me._isDelegateErrorHandling = value
        End Set
    End Property

    Private _lastError As String
    ''' <summary>Returns the last error read from the instrument.</summary>
    Public ReadOnly Property LastError() As String Implements IRequestable.LastError
        Get
            Return Me._lastError
        End Get
    End Property

    Private _lastErrorQueue As String
    ''' <summary>Returns the last error queue read from the instrument.</summary>
    Public ReadOnly Property LastErrorQueue() As String Implements IRequestable.LastErrorQueue
        Get
            Return Me._lastErrorQueue
        End Get
    End Property

    ''' <summary>Returns the error queue and ESR report.
    ''' </summary>
    Public ReadOnly Property LastErrorReport() As String Implements IRequestable.LastErrorReport
        Get

            Dim messageBuilder As New System.Text.StringBuilder

            If Me._lastErrorQueue.Length > 0 Then
                If messageBuilder.Length > 0 Then
                    messageBuilder.Append(". ")
                End If
                messageBuilder.Append("The device reported the following error(s) from the error queue: ")
                messageBuilder.Append(Environment.NewLine)
                messageBuilder.Append(Me._lastErrorQueue)
                messageBuilder.Append(".")
            End If

            Dim report As String = Me.LastStandardStatusRegisterReport()
            If report.Length > 0 Then
                If messageBuilder.Length > 0 Then
                    messageBuilder.Append(Environment.NewLine)
                End If
                messageBuilder.Append("The device standard status register reported the following error(s): ")
                messageBuilder.Append(Environment.NewLine)
                messageBuilder.Append(report)
                messageBuilder.Append(".")
            End If

            Return messageBuilder.ToString

        End Get
    End Property

    Public ReadOnly Property LastStandardStatusRegisterReport() As String Implements IRequestable.LastStandardStatusRegisterReport
        Get
            Return BuildStandardStatusRegisterReport()
        End Get
    End Property

    Private _servicingRequest As Boolean
    Public Property ServicingRequest() As Boolean Implements IRequestable.ServicingRequest
        Get
            Return Me._servicingRequest
        End Get
        Set(ByVal value As Boolean)
            Me._servicingRequest = value
        End Set
    End Property

    Private _measurementEventStatus As Integer
    ''' <summary>Returns the measurement Event register status.  To decipher, cast to the event status
    '''   of the specific instrument type.</summary>
    Public ReadOnly Property MeasurementEventStatus() As Integer Implements IRequestable.MeasurementEventStatus
        Get
            Return Me._measurementEventStatus
        End Get
    End Property

    Private _operationCompleted As Boolean
    Public Property OperationCompleted() As Boolean Implements IRequestable.OperationCompleted
        Get
            Return Me._operationCompleted
        End Get
        Set(ByVal value As Boolean)
            Me._operationCompleted = value
        End Set
    End Property

    Private _operationCondition As Integer
    ''' <summary>Gets or sets the operation condition status. To decipher, cast to the event status
    '''   of the specific instrument type.</summary>
    Public Property OperationCondition() As Integer Implements IRequestable.OperationCondition
        Get
            Return Me._operationCondition
        End Get
        Set(ByVal value As Integer)
            Me._operationCondition = value
        End Set
    End Property

    ''' <summary>Returns the operation elapsed time.</summary>
    Public ReadOnly Property OperationElapsedTime() As TimeSpan Implements IRequestable.OperationElapsedTime
        Get
            Return Me._eventTime.Subtract(Me._operationTime)
        End Get
    End Property

    Private _operationTime As DateTime = Date.Now
    ''' <summary>Returns the operation time.  This is typically set to when the 
    '''   operation started so the operation can be timed to its event time.</summary>
    Public ReadOnly Property OperationTime() As DateTime Implements IRequestable.OperationTime
        Get
            Return Me._operationTime
        End Get
    End Property

    Private _operationEventStatus As Integer
    ''' <summary>Returns the operation event register status.  To decipher, cast to the event status
    '''   of the specific instrument type.</summary>
    Public ReadOnly Property OperationEventStatus() As Integer Implements IRequestable.OperationEventStatus
        Get
            Return Me._operationEventStatus
        End Get
    End Property

    Private _questionableEventStatus As Integer
    ''' <summary>Returns the Questionable Event register status. To decipher, cast to the event status
    '''   of the specific instrument type.</summary>
    Public ReadOnly Property QuestionableEventStatus() As Integer Implements IRequestable.QuestionableEventStatus
        Get
            Return Me._questionableEventStatus
        End Get
    End Property

    Private _receivedMessage As String
    ''' <summary>Returns the message that was received from the instrument.</summary>
    Public ReadOnly Property ReceivedMessage() As String Implements IRequestable.ReceivedMessage
        Get
            Return Me._receivedMessage
        End Get
    End Property

    Private _requestCount As Long
    ''' <summary>Returns the number of service request that were recorded since the
    '''   last Clear Status command.</summary>
    Public ReadOnly Property RequestCount() As Long Implements IRequestable.RequestCount
        Get
            Return Me._requestCount
        End Get
    End Property

    Private _serviceRequestStatus As ServiceRequests
    ''' <summary>Returns the service request.</summary>
    Public ReadOnly Property ServiceRequestStatusBits() As Integer Implements IRequestable.ServiceRequestStatusBits
        Get
            Return Me._serviceRequestStatus
        End Get
    End Property

    ''' <summary>Returns the service request 
    '''   <see cref="ServiceRequests">status byte</see>
    ''' This is the typed value of the <see cref="ServiceRequestStatusBits">service request status byte.</see>
    ''' </summary>
    Public ReadOnly Property ServiceRequestStatus() As ServiceRequests
        Get
            Return Me._serviceRequestStatus
        End Get
    End Property

    Private _standardEventStatus As StandardEvents
    ''' <summary>Returns the standard event register byte</summary>
    Public ReadOnly Property StandardEventStatusBits() As Integer Implements IRequestable.StandardEventStatusBits
        Get
            Return Me._standardEventStatus
        End Get
    End Property

    ''' <summary>Returns the standard event register
    '''   <see cref="StandardEvents">status</see>
    ''' This is the typed value of the <see cref="StandardEventStatusBits">standard event status byte.</see>
    ''' </summary>
    Public ReadOnly Property StandardEventStatus() As StandardEvents
        Get
            Return Me._standardEventStatus
        End Get
    End Property

#End Region

#Region " EVENT and HANDLERS "

    ''' <summary>Raised upon receiving a request to have the instrument retrieve error information.
    ''' This is done in case the instrument does not comply with standard SCPI methods for retrieving
    ''' the error queue of standard event register errors.
    ''' </summary>
    Public Event ErrorInfoRequested As EventHandler(Of BaseServiceEventArgs) Implements IRequestable.ErrorInfoRequested

    ''' <summary>Raises the request for error information.</summary>
    ''' <param name="e">Passes reference to the <see cref="BaseServiceEventArgs">SCPI service request event arguments</see>.</param>
    Protected Overridable Sub OnErrorInfoRequested(ByVal e As BaseServiceEventArgs) Implements IRequestable.OnErrorInfoRequested
        ErrorInfoRequestedEvent.SafeBeginEndInvoke(Me, e)
    End Sub

#End Region

End Class

Module EventHandlerExtensions

    ''' <summary>
    ''' Safely triggers an event.
    ''' </summary>
    ''' <typeparam name="TEventArgs">The type of the event arguments.</typeparam>
    ''' <param name="value">The event to trigger.</param>
    ''' <param name="sender">The sender of the event.</param>
    ''' <param name="e">The arguments for the event.</param>
    ''' <remarks>
    ''' Safe Begin End Invoke Benchmarked at 0.054185ms    
    ''' ''' </remarks>
    <System.Runtime.CompilerServices.Extension()> 
    Public Sub SafeBeginEndInvoke(Of TEventArgs As EventArgs)(ByVal value As EventHandler(Of TEventArgs), ByVal sender As Object, ByVal e As TEventArgs)
        If Not value Is Nothing Then
            For Each d As [Delegate] In value.GetInvocationList
                If d.Target IsNot Nothing AndAlso TypeOf d.Target Is System.ComponentModel.ISynchronizeInvoke Then
                    Dim target As System.ComponentModel.ISynchronizeInvoke = CType(d.Target, System.ComponentModel.ISynchronizeInvoke)
                    If target.InvokeRequired Then
                        Try
                            Dim result As IAsyncResult = target.BeginInvoke(value, New Object() {sender, e})
                            If result IsNot Nothing Then
                                target.EndInvoke(result)
                            End If
                        Catch ex As ObjectDisposedException
                        End Try
                    Else
                        d.DynamicInvoke(New Object() {sender, EventArgs.Empty})
                    End If
                Else
                    d.DynamicInvoke(New Object() {sender, EventArgs.Empty})
                End If
            Next

        End If
    End Sub

End Module

