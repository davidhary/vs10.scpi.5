''' <summary>
''' Defines an Arm Layer for the Trigger SCPI subsystem
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/08/2001" by="David" revision="1.0.646.x">
''' Created
''' </history>
Public Class ArmLayer
    Implements IResettable, IKeyable

    Public Sub New(ByVal syntaxHeader As String)
        MyBase.New()

        Me._syntaxHeader = syntaxHeader

        Me._resettableValues = New ResettableCollection(Of IResettable)
        Me._AutoDelay = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._AutoDelay)
        Me._Bypass = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._Bypass)
        Me._Count = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._Count)
        Me._Delay = New ResettableDouble
        Me._resettableValues.Add(Me._Delay)
        Me._InputLineNumber = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._InputLineNumber)
        Me._OutputLineNumber = New ResettableValue(Of Integer)
        Me._resettableValues.Add(Me._OutputLineNumber)
        Me._Source = New ResettableValue(Of ArmSource)
        Me._resettableValues.Add(Me._Source)
        Me._TimerSeconds = New ResettableDouble
        Me._resettableValues.Add(Me._timerSeconds)

        Me._autoDelay.ResetValue = False
        Me._count.ResetValue = 1
        Me._delay.ResetValue = 0
        Me._bypass.ResetValue = True
        Me._inputLineNumber.ResetValue = 1
        Me._outputLineNumber.ResetValue = 2
        Me._source.ResetValue = Scpi.ArmSource.Immediate
        Me._timerSeconds.ResetValue = 0.001

    End Sub

#Region " IRESETTABLE "

    ''' <summary>
    ''' Holds all resettable values for this function.
    ''' </summary>
    Private _resettableValues As ResettableCollection(Of IResettable)

    ''' <summary>Clears the queues and resets all registers to zero.</summary>
    Public Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        Return Me._resettableValues.ClearExecutionState()
    End Function

    ''' <summary>Returns subsystem to its preset values.</summary>
    Public Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        Return Me._resettableValues.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Preset values:<para>
    ''' </para>
    ''' </summary>
    Public Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        Return Me._resettableValues.ResetKnownState()
    End Function

#End Region

    ''' <summary>Gets the auto Delay cached value.
    ''' </summary>
    Public Property AutoDelay() As ResettableValue(Of Boolean)

    ''' <summary>Gets or sets the Count.</summary>
    Public Property Count() As ResettableValue(Of Integer)

    ''' <summary>Gets or sets the Delay.</summary>
    Public Property Delay() As ResettableDouble

    ''' <summary>
    ''' Gets or sets the bypass (Acceptor) or Enable (source) of the arm layer.
    ''' </summary>
    Public Property Bypass() As ResettableValue(Of Boolean)

    ''' <summary>
    ''' Gets or sets the input line number of the active layer.
    ''' </summary>
    Public Property InputLineNumber() As ResettableValue(Of Integer)

    ''' <summary>
    ''' Gets or sets the output line number of the active layer.
    ''' </summary>
    Public Property OutputLineNumber() As ResettableValue(Of Integer)

    ''' <summary>
    ''' Gets or sets the source immediate, manual, bus, trigger link, 
    ''' start test or stop test ior timer depending on the instrument..
    ''' </summary>
    Public Property Source() As ResettableValue(Of ArmSource)

    ''' <summary>
    ''' Gets or sets the header is used in the SCPI commands.
    ''' </summary>
    Public Property SyntaxHeader() As String Implements IKeyable.UniqueKey

    ''' <summary>
    ''' Requests a programmed timer interval.
    ''' </summary>
    Public Property TimerSeconds() As ResettableDouble

End Class
