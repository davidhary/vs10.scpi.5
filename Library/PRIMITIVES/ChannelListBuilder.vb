''' <summary>Builds a channel list.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/21/05" by="David" revision="1.0.1847.x">
''' Created
''' </history>
Public Class ChannelListBuilder
    Implements IDisposable

#Region " SHARED "

    ''' <summary>Constructs a channel list element.</summary>
    ''' <param name="memoryLocation">Specifies the memory location.</param>
    Public Shared Function ChannelListElement(ByVal memoryLocation As Int32) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "M{0}", memoryLocation)
    End Function

    ''' <summary>Constructs a channel list element.</summary>
    ''' <param name="slotNumber">Specifies the card slot number in the relay 
    '''   mainframe.</param>
    ''' <param name="relayNumber">Specifies the relay number in the slot.</param>
    Public Shared Function ChannelListElement(ByVal slotNumber As Int32, ByVal relayNumber As Int32) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}!{1}", slotNumber, relayNumber)
    End Function

    ''' <summary>Constructs a channel list element.</summary>
    ''' <param name="slotNumber">Specifies the card slot number in the relay 
    '''   mainframe.</param>
    ''' <param name="rowNumber">Specifies the row number of the relay.</param>
    ''' <param name="columnNumber">Specifies the column number of the relay.</param>
    Public Shared Function ChannelListElement(ByVal slotNumber As Int32, ByVal rowNumber As Int32, ByVal columnNumber As Int32) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0}!{1}!{2}", slotNumber, rowNumber, columnNumber)
    End Function

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    Public Sub New()

        ' instantiate the base class
        MyBase.New()

        Me._channelListStringBuilder = New System.Text.StringBuilder(String.Empty)
        '      Me._elementCount = 0

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="channelList">A channel list to initialize.</param>
    Public Sub New(ByVal channelList As String)

        ' instantiate the base class
        Me.New()
        Me.ChannelList = channelList

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me._channelListStringBuilder = New System.Text.StringBuilder(String.Empty)
                    Me._channelListStringBuilder = Nothing

                End If

            End If

            ' Free shared unmanaged resources

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " METHODS  and  PROPERTIES "

    ''' <summary>Adds a relay channel to the channel list.</summary>
    ''' <param name="channelElement">Specifies the channel element to add.</param>
    Public Sub AddChannel(ByVal channelElement As String)
        Syntax.AddWord(Me._channelListStringBuilder, channelElement)
        Me._elementCount += 1
    End Sub

    ''' <summary>Adds a memory location to the channel list.</summary>
    ''' <param name="memoryLocation">Specifies the memory location.</param>
    Public Sub AddChannel(ByVal memoryLocation As Int32)
        Me.AddChannel(ChannelListElement(memoryLocation))
    End Sub

    ''' <summary>Adds a card relay channel to the channel list.</summary>
    ''' <param name="slotNumber">Specifies the card slot number in the relay 
    '''   mainframe.</param>
    ''' <param name="relayNumber">Specifies the relay number in the slot.</param>
    Public Sub AddChannel(ByVal slotNumber As Int32, ByVal relayNumber As Int32)
        Me.AddChannel(ChannelListElement(slotNumber, relayNumber))
    End Sub

    ''' <summary>Adds a matrix relay channel to the channel list.</summary>
    ''' <param name="slotNumber">Specifies the card slot number in the relay 
    '''   mainframe.</param>
    ''' <param name="rowNumber">Specifies the row number of the relay.</param>
    ''' <param name="columnNumber">Specifies the column number of the relay.</param>
    Public Sub AddChannel(ByVal slotNumber As Int32, ByVal rowNumber As Int32, ByVal columnNumber As Int32)
        Me.AddChannel(ChannelListElement(slotNumber, rowNumber, columnNumber))
    End Sub

    ''' <summary>Adds a range of relay channels to the channel list.</summary>
    ''' <param name="fromChannelElement">Specifies the starting channel element to add.</param>
    ''' <param name="toChannelElement">Specifies the ending channel element to add.</param>
    Public Sub AddChannelRange(ByVal fromChannelElement As String, ByVal toChannelElement As String)
        Syntax.AddWord(Me._channelListStringBuilder, fromChannelElement)
        Me._channelListStringBuilder.Append(fromChannelElement)
        Me._channelListStringBuilder.Append(":")
        Me._channelListStringBuilder.Append(toChannelElement)
        Me._elementCount += 1
    End Sub

    ''' <summary>Adds a card relay range of channels channel to the channel list.</summary>
    ''' <param name="fromSlotNumber">Specifies the starting card slot number in the relay 
    '''   mainframe.</param>
    ''' <param name="fromRelayNumber">Specifies the starting relay number in the slot.</param>
    ''' <param name="toSlotNumber">Specifies the ending card slot number in the relay 
    '''   mainframe.</param>
    ''' <param name="toRelayNumber">Specifies the ending relay number in the slot.</param>
    Public Sub AddChannelRange(ByVal fromSlotNumber As Int32, ByVal fromRelayNumber As Int32, 
      ByVal toSlotNumber As Int32, ByVal toRelayNumber As Int32)
        Me.AddChannelRange(ChannelListElement(fromSlotNumber, fromRelayNumber), 
          ChannelListElement(toSlotNumber, toRelayNumber))
    End Sub

    ''' <summary>Adds a matrix relay range of channels to the channel list.</summary>
    ''' <param name="fromSlotNumber">Specifies the starting card slot number in the relay 
    '''   mainframe.</param>
    ''' <param name="fromRowNumber">Specifies the starting row number of the relay.</param>
    '''     ''' <param name="fromColumnNumber">Specifies the starting column number of the relay.</param>
    ''' <param name="toSlotNumber">Specifies the ending card slot number in the relay 
    '''   mainframe.</param>
    ''' <param name="toRowNumber">Specifies the ending row number of the relay.</param>
    ''' <param name="toColumnNumber">Specifies the ending column number of the relay.</param>
    Public Sub AddChannel(ByVal fromSlotNumber As Int32, ByVal fromRowNumber As Int32, ByVal fromColumnNumber As Int32, 
    ByVal toSlotNumber As Int32, ByVal toRowNumber As Int32, ByVal toColumnNumber As Int32)
        Me.AddChannelRange(ChannelListElement(fromSlotNumber, fromRowNumber, fromColumnNumber), 
          ChannelListElement(toSlotNumber, toRowNumber, toColumnNumber))
    End Sub

    Private _channelListStringBuilder As System.Text.StringBuilder
    ''' <summary>Gets or sets the channel list sans the '(@' prefix and ')' suffix.</summary>
    Public Property NakedChannelList() As String
        Get
            Return Me._channelListStringBuilder.ToString
        End Get
        Set(ByVal value As String)
            Me._channelListStringBuilder = New System.Text.StringBuilder(value)
            Me._elementCount = Me._channelListStringBuilder.ToString.Split(","c).Length
        End Set
    End Property

    ''' <summary>Gets or sets an operation-able channel list that can be passed to
    '''   the route and scan commands.</summary>
    ''' <remarks>The channel list specifies the channels to be closed or opened.  Each
    '''   comma delimited channel in the list is made up of either a exclamation point 
    '''   separated two Int32 card relay number (i.e., slot#!relay#) or a three Int32 
    '''   exclamation point separated matrix relay number (i.e., slot@!row#!column#) or 
    '''   a memory location (M#).  The channel list begins with a prefix '(@' and ends
    '''   with a suffix ")".</remarks>
    Public Property ChannelList() As String
        Get
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "(@{0})", Me._channelListStringBuilder)
        End Get
        Set(ByVal value As String)
            If Not String.IsNullOrWhiteSpace(value) Then
                Me.NakedChannelList = value.Substring(2).TrimEnd(")"c).Trim
            End If
        End Set
    End Property

    Private _elementCount As Int32
    ''' <summary>Returns the number of elements in the channel list.  This is 
    '''    not the same as the number of channels.</summary>
    Public ReadOnly Property ElementCount() As Int32
        Get
            Return Me._elementCount
        End Get
    End Property

#End Region

End Class

