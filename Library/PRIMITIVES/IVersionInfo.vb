''' <summary>
''' Interface for implementing version information.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' Created
''' </history>
Public Interface IVersionInfo

    ReadOnly Property FirmwareRevision() As String
    ReadOnly Property ManufacturerName() As String
    ReadOnly Property Model() As String
    ReadOnly Property SerialNumber() As String

    Sub ParseFirmwareRevision(ByVal revision As String)
    Sub ParseInstrumentId(ByVal id As String)

End Interface
