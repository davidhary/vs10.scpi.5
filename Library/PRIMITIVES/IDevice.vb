''' <summary>
''' Provides an interface for elements that implement a SCPI Device.
''' The SCPI controller reads from and write to an actual device be it VISA or GPIB.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="303/25/2008" by="David" revision="5.0.3004.x">
''' Created
''' </history>
Public Interface IDevice

#Region " ACCESS LEVEL "

    ''' <summary>
    ''' Gets or sets reference to the level of <see cref="ResourceAccessLevels">resource access.</see>
    ''' </summary>
    Property AccessLevel() As ResourceAccessLevels

    ''' <summary>
    ''' Saves the access level.
    ''' </summary>
    Sub StoreResourceAccessLevel()

    ''' <summary>
    ''' Restores the access level.
    ''' </summary>
    Sub RestoreResourceAccessLevel()

#End Region

#Region " CONTROLLER INFORMATION "

    ''' <summary>Gets the resource name.</summary>
    Property ResourceName() As String

    ''' <summary>Gets device access mode.</summary>
    Property UsingDevices() As Boolean

    ''' <summary>
    ''' Gets or sets the version information.  Allow a Protected way to set the
    ''' version information to an instrument-specific version information.
    ''' </summary>
    Property VersionInfo() As isr.Scpi.IVersionInfo

    ''' <summary>
    ''' Queries the instrument and returns the Identity string..
    ''' </summary>
    Function ReadIdentity() As String

#End Region

#Region " QUERY "

    ''' <summary>Queries the instrument and returns a Boolean value.</summary>
    ''' <param name="question">The query to use.</param>
    Function QueryBoolean(ByVal question As String) As Nullable(Of Boolean)

    ''' <summary>Queries the instrument and returns a double value.</summary>
    ''' <param name="question">The query to use.</param>
    Function QueryDouble(ByVal question As String) As Nullable(Of Double)

    ''' <summary>Queries the instrument and returns an integer value.</summary>
    ''' <param name="question">The query to use.</param>
    Function QueryInteger(ByVal question As String) As Nullable(Of Integer)

    ''' <summary>Queries the instrument and returns an integer value.
    ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
    ''' <param name="question">The query to use.</param>
    Function QueryInfInteger(ByVal question As String) As Nullable(Of Integer)

    ''' <summary>
    ''' Sends the query and reads the reply.
    ''' </summary>
    ''' <param name="query"></param>
    Function QueryString(ByVal query As String) As String

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    ''' <param name="question">The query to use.</param>
    Function QueryTrimEnd(ByVal question As String) As String

    ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
    ''' <param name="question">The query to use.</param>
    Function QueryTrimNewLine(ByVal question As String) As String

#End Region

#Region " READ "

    ''' <summary>
    ''' Gets or sets the default string size to read.
    ''' </summary>
    Property DefaultStringSize() As Integer

    ''' <summary>Reads and discard all unread data till the END character.</summary>
    Sub DiscardUnreadData()

    ''' <summary>Reads the instrument and returns a Boolean value.</summary>
    Function ReadBoolean() As Nullable(Of Boolean)

    ''' <summary>Reads the instrument and returns a double value.</summary>
    Function ReadDouble() As Nullable(Of Double)

    ''' <summary>Reads the instrument and returns an integer value.</summary>
    Function ReadInteger() As Nullable(Of Integer)

    ''' <summary>Reads the instrument and returns an integer value.
    ''' The instrument may return INF in which case the returned value is the maximum integer.</summary>
    Function ReadInfInteger() As Nullable(Of Integer)

    ''' <summary>Queries the instrument and returns a string save the termination character.</summary>
    Overloads Function ReadLineTrimEnd(ByVal terminationCharacters As Char()) As String

    ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
    Overloads Function ReadLineTrimEnd(ByVal terminationCharacter As Byte) As String

    ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
    Overloads Function ReadLineTrimEnd() As String

    ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
    Function ReadLineTrimNewLine() As String

    ''' <summary>Queries the instrument and returns a string save the new line and carriage return characters.</summary>
    Function ReadLineTrimLinefeed() As String

    ''' <summary>
    ''' Receives data from the SCPI instrument.
    ''' </summary>
    Function ReadLine() As String

    ''' <summary>Gets the last message that was received from the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    ReadOnly Property ReceiveBuffer() As String

    ''' <summary>
    ''' Gets or sets the termination character.
    ''' </summary>
    Property TerminationCharacter() As Byte

#End Region

#Region " REGISTERS "

    ''' <summary>
    ''' Awaits the message available bit.
    ''' </summary>
    ''' <param name="timeout">The timeout.</param>
    ''' <param name="pollDelay">The poll delay.</param>
    ''' <returns>
    ''' <c>True</c> if message available or false if not.
    ''' </returns>
    Function AwaitMessageAvailable(ByVal timeout As Integer, ByVal pollDelay As Integer) As Boolean

    ''' <summary>Returns True if the last operation reported an error by
    '''   way of a service request.</summary>
    ReadOnly Property HadError() As Boolean

    ''' <summary>Reads the device status data byte and returns True
    ''' if the message available bits were set.
    ''' </summary>
    ''' <returns>True if the device has data in the queue
    ''' </returns>
    Function IsMessageAvailable() As Boolean

    ''' <summary>Gets or sets the last service event arguments
    '''   <see cref="BaseServiceEventArgs">status</see></summary>
    ''' <remarks>Also used to hold values read from instrument by way of static methods
    '''   such as *OPC?</remarks>
    Property LastServiceEventArgs() As BaseServiceEventArgs

    ''' <summary>
    ''' Serial polls the device and returns the <see cref="ServiceRequests">service requests bits.</see>
    ''' </summary>
    Function SerialPoll() As ServiceRequests

    ''' <summary>
    ''' Serial polls the device and returns the <see cref="ServiceRequests">service requests bits.</see>
    ''' </summary>
    Function ReadStatusByte() As ServiceRequests

    ''' <summary>
    ''' Gets or sets the condition that determines if the device supports OPC.
    ''' </summary>
    ''' <remarks>
    ''' Some instruments provide only partial support of IEEE488.2 not supporting the query for 
    ''' determining completion of operations. For these instruments, another method needs to be 
    ''' used such as polling.
    ''' </remarks>
    Property SupportsOperationComplete() As Boolean

    ''' <summary>
    ''' Reads the standard event register status.
    ''' </summary>
    Function ReadStandardEventStatus() As Integer

    ''' <summary>Restores the last timeout from the stack.
    ''' </summary>
    Sub RestoreTimeout()

    ''' <summary>Saves the current timeout and sets a new setting timeout.
    ''' </summary>
    ''' <param name="timeout">Specifies the new timeout</param>
    Sub StoreTimeout(ByVal timeout As Integer)

#End Region

#Region " RESETTABLE "

    ''' <summary>Initialize resettable values, units, scales.
    ''' This is to be called once upon connecting but before reset and clear..</summary>
    Function InitializeExecutionState() As Boolean

    ''' <summary>
    ''' Clears (sets to 0) the bits of the Standard Event Register, Operation Event Register, Error Queue, 
    ''' Measurement Event Register and Questionable Event Register.  It also forces the instrument into the 
    ''' operation complete command idle state and operation complete query idle state.
    ''' </summary>
    Function ClearExecutionState() As Boolean

    ''' <summary>Returns subsystem to its preset values.</summary>
    Function PresetKnownState() As Boolean

    ''' <summary>Returns the device to its default known state.</summary>
    Function ResetKnownState() As Boolean

#End Region

#Region " WRITE "

    ''' <summary>Gets the last message that was received from the instrument.
    ''' Note that not all messages get recorded.
    ''' </summary>
    ReadOnly Property TransmitBuffer() As String

    ''' <summary>Writes boolean command to the instrument.</summary>
    ''' <param name="queryCommand">The main command.</param>
    ''' <param name="value">The boolean value to write.</param>
    ''' <param name="boolFormat">The boolean format.</param>
    Sub WriteLine(ByVal queryCommand As String, ByVal value As Boolean, ByVal boolFormat As BooleanDataFormat)

    ''' <summary>
    ''' Outputs a command based on the format string and the specified values.
    ''' </summary>
    Sub WriteLine(ByVal format As String, ByVal ParamArray values() As Object)

    ''' <summary>
    ''' Writes a string to the instrument.
    ''' </summary>
    ''' <param name="value">Specifies the data to send.</param>
    ''' <remarks>
    ''' This method makes no assumptions on the status of 
    ''' the GPIB instrument other than it must not have data
    ''' in the queue when sending new data to the instrument.
    ''' Because this method does not wait for operation to
    ''' complete, it is unable to report device errors as
    ''' they occur as the device may report an error way
    ''' after the message was sent to the GPIB.  Therefore,
    ''' in debug situation it is preferable to use the
    ''' full <see cref="WriteLine"/> and <see cref="ReadLine"/> methods.
    ''' </remarks>
    Sub WriteLine(ByVal value As String)

#End Region

#Region " SUB SYSTEMS "

    ''' <summary>
    ''' Reference to the SCPI <see cref="isr.Scpi.SystemSubsystem">System subsystem</see>.
    ''' Every controller must have a System subsystem. 
    ''' </summary>
    ReadOnly Property SystemSubsystem() As isr.Scpi.SystemSubsystem

#End Region

End Interface
