''' <summary>
''' Provides an interface for elements that implement a SCPI subsystem.</summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/07/2008" by="David" revision="1.0.2926.x">
''' Created
''' </history>
Public Interface ISubsystem

    ''' <summary>
    ''' Provides a reference to the SCPI controller.
    ''' </summary>
    ReadOnly Property Controller() As Scpi.IDevice

    ''' <summary>
    ''' Defines the subsystem name.
    ''' </summary>
    ReadOnly Property Name() As String

End Interface

