''' <summary>
''' Defines a SCPI function such as current, voltage.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/08/2001" by="David" revision="1.0.646.x">
''' Created
''' </history>
Public Class ScpiFunction
    Implements IResettable, IKeyable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Constructs this class.
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the syntax header for the function.</param>
    ''' <param name="lineFrequency">Specifies the power line frequency of the instrument.</param>
    Private Sub New(ByVal syntaxHeader As String, ByVal lineFrequency As Double)
        MyBase.New()
        Me._syntaxHeader = syntaxHeader
        Me._lineFrequency = lineFrequency
        Me._units = New Scpi.FormattedUnits()

        Me._resettableValues = New ResettableCollection(Of IResettable)
        Me._autoRange = New ResettableValue(Of Boolean)
        Me._resettableValues.Add(Me._autoRange)
        Me._compliance = New ResettableDouble
        Me._resettableValues.Add(Me._compliance)
        Me._level = New ResettableDouble
        Me._resettableValues.Add(Me._level)
        Me._integrationPeriod = New ResettableDouble
        Me._resettableValues.Add(Me._integrationPeriod)
        Me._integrationPeriod.Units = New Scpi.FormattedUnits(PhysicalUnit.Time, UnitsScale.None)
        Me._powerLineCycles = New ResettableDouble
        Me._resettableValues.Add(Me._powerLineCycles)
        Me._protectionLevel = New ResettableDouble
        Me._resettableValues.Add(Me._protectionLevel)
        Me._range = New ResettableDouble
        Me._resettableValues.Add(Me._range)
        Me._startLevel = New ResettableDouble
        Me._resettableValues.Add(Me._startLevel)
        Me._stopLevel = New ResettableDouble
        Me._resettableValues.Add(Me._stopLevel)
        Me._sweepMode = New ResettableValue(Of SweepMode)
        Me._resettableValues.Add(Me._sweepMode)

    End Sub

    ''' <summary>
    ''' Constructs this class.
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the syntax header for the function.</param>
    ''' <param name="modality">Defines the <see cref="SenseFunctionModes">SCPI function or element</see>.</param>
    ''' <param name="lineFrequency">Specifies the power line frequency of the instrument.</param>
    Public Sub New(ByVal syntaxHeader As String, ByVal lineFrequency As Double, ByVal modality As Scpi.SenseFunctionModes)
        Me.New(syntaxHeader, lineFrequency)
        Me._senseModality = modality
        Select Case modality
            Case Scpi.SenseFunctionModes.Current, Scpi.SenseFunctionModes.CurrentAC, Scpi.SenseFunctionModes.CurrentDC
                Me._units = New Scpi.FormattedUnits(PhysicalUnit.Current, UnitsScale.None)
            Case Scpi.SenseFunctionModes.Continuity
                Me._units = New Scpi.FormattedUnits(PhysicalUnit.Resistance, UnitsScale.None)
            Case Scpi.SenseFunctionModes.FourWireResistance, Scpi.SenseFunctionModes.Resistance
                Me._units = New Scpi.FormattedUnits(PhysicalUnit.Resistance, UnitsScale.None)
            Case Scpi.SenseFunctionModes.Temperature
                Me._units = New Scpi.FormattedUnits(PhysicalUnit.CelsiusTemperature, UnitsScale.None)
            Case Scpi.SenseFunctionModes.Voltage, Scpi.SenseFunctionModes.VoltageAC, Scpi.SenseFunctionModes.VoltageDC
                Me._units = New Scpi.FormattedUnits(PhysicalUnit.Voltage, UnitsScale.None)
            Case Scpi.SenseFunctionModes.Frequency
                Me._units = New Scpi.FormattedUnits(PhysicalUnit.Frequency, UnitsScale.None)
            Case Else
                Me._units = New Scpi.FormattedUnits(PhysicalUnit.None, UnitsScale.None)
        End Select

        Me._level.Units = New Scpi.FormattedUnits(Me._units)
        Me._protectionLevel.Units = New Scpi.FormattedUnits(Me._units)
        Me._range.Units = New Scpi.FormattedUnits(Me._units)
        Me._startLevel.Units = New Scpi.FormattedUnits(Me._units)
        Me._stopLevel.Units = New Scpi.FormattedUnits(Me._units)


    End Sub

    ''' <summary>
    ''' Constructs this class.
    ''' </summary>
    ''' <param name="syntaxHeader">Specifies the syntax header for the function.</param>
    ''' <param name="sourceModality">Defines the <see cref="SourceFunctionMode">SCPI function or element</see>.</param>
    Public Sub New(ByVal syntaxHeader As String, ByVal lineFrequency As Double, ByVal sourceModality As Scpi.SourceFunctionMode)
        Me.New(syntaxHeader, lineFrequency)
        Me._sourceModality = sourceModality
        Me._isSource = True
        Select Case sourceModality
            Case SourceFunctionMode.Current, SourceFunctionMode.CurrentAC, SourceFunctionMode.CurrentDC
                Me._units = New Scpi.FormattedUnits(PhysicalUnit.Current, UnitsScale.None)
                Me._compliance.Units = New Scpi.FormattedUnits(PhysicalUnit.Voltage, UnitsScale.None)
            Case SourceFunctionMode.Voltage, SourceFunctionMode.VoltageAC, SourceFunctionMode.VoltageDC
                Me._units = New Scpi.FormattedUnits(PhysicalUnit.Voltage, UnitsScale.None)
                Me._compliance.Units = New Scpi.FormattedUnits(PhysicalUnit.Current, UnitsScale.None)
            Case Else
                Me._units = New Scpi.FormattedUnits(PhysicalUnit.None, UnitsScale.None)
        End Select

        Me._level.Units = New Scpi.FormattedUnits(Me._units)
        Me._protectionLevel.Units = New Scpi.FormattedUnits(Me._units)
        Me._range.Units = New Scpi.FormattedUnits(Me._units)
        Me._startLevel.Units = New Scpi.FormattedUnits(Me._units)
        Me._stopLevel.Units = New Scpi.FormattedUnits(Me._units)

    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>
    ''' Holds all resettable values for this function.
    ''' </summary>
    Private _resettableValues As Scpi.ResettableCollection(Of Scpi.IResettable)

    ''' <summary>Clears the queues and resets all registers to zero.</summary>
    Public Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        Return Me._resettableValues.ClearExecutionState()
    End Function

    ''' <summary>Returns subsystem to its preset values.</summary>
    Public Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        Return Me._resettableValues.PresetKnownState()
    End Function

    ''' <summary>Restore member properties to the following RST or System Preset values:<para>
    ''' </para>
    ''' </summary>
    Public Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        Return Me._resettableValues.ResetKnownState()
    End Function

#End Region

#Region " MANAGEMENT VALUES "

    Private _isSource As Boolean
    ''' <summary>
    ''' Gets a value indicating whether this instance is source function.
    ''' </summary>
    ''' <value><c>True</c> if this instance is source function; otherwise, <c>False</c>.</value>
    Public ReadOnly Property IsSourceFunction() As Boolean
        Get
            Return Me._isSource
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the power line frequency.
    ''' </summary>
    Public Property LineFrequency() As Double

    Private _senseModality As Scpi.SenseFunctionModes
    ''' <summary>
    ''' Gets the sense modality.
    ''' </summary>
    ''' <value>The sense modality.</value>
    Public ReadOnly Property SenseModality() As Scpi.SenseFunctionModes
        Get
            Return Me._senseModality
        End Get
    End Property

    Private _sourceModality As Scpi.SourceFunctionMode
    ''' <summary>
    ''' Gets the source modality.
    ''' </summary>
    ''' <value>The source modality.</value>
    Public ReadOnly Property SourceModality() As Scpi.SourceFunctionMode
        Get
            Return Me._sourceModality
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the syntax header. 
    ''' Is used in the SCPI commands.
    ''' </summary>
    ''' <value>The syntax header.</value>
    Public Property SyntaxHeader() As String Implements IKeyable.UniqueKey

    ''' <summary>
    ''' Gets or sets the units for this function.
    ''' </summary>
    Public Property Units() As Scpi.FormattedUnits

#End Region

#Region " INSTRUMENT VALUES "

    ''' <summary>
    ''' Gets or sets the condition for auto current range.
    ''' </summary>
    ''' <value>The auto range.</value>
    Public Property AutoRange() As ResettableValue(Of Boolean)

    ''' <summary>
    ''' Gets or sets the compliance.
    ''' </summary>
    ''' <value>The compliance.</value>
    Public Property Compliance() As ResettableDouble

    ''' <summary>
    ''' Gets or sets the level.
    ''' </summary>
    ''' <value>The level.</value>
    Public Property Level() As ResettableDouble

    Private _sweepMode As ResettableValue(Of SweepMode)
    ''' <summary>
    ''' Gets or sets the sweep mode, e.g., Fixed, List, Sweep
    ''' </summary>
    ''' <value>The sweep mode.</value>
    Public Property SweepMode() As ResettableValue(Of SweepMode)
        Get
            Return Me._sweepMode
        End Get
        Set(ByVal value As ResettableValue(Of SweepMode))
            Me._sweepMode = value
            If value IsNot Nothing Then
                Me._sweepModeCaption = Syntax.ExtractScpi(value.Value)
            End If
        End Set
    End Property

    Private _sweepModeCaption As String
    ''' <summary>
    ''' Gets or sets the sweep mode caption.
    ''' </summary>
    ''' <value>The sweep mode caption.</value>
    Friend Property SweepModeCaption() As String
        Get
            Return Me._sweepModeCaption
        End Get
        Set(ByVal value As String)
            Me._sweepModeCaption = value
            If value IsNot Nothing Then
                Dim se As New StringEnumerator(Of SweepMode)
                Me._sweepMode.Value = se.ParseContained(Syntax.DelimitedScpiCommand(Me._sweepModeCaption))
            End If
        End Set
    End Property

    Private _integrationPeriod As ResettableDouble
    ''' <summary>
    ''' Gets or sets the integration period.
    ''' </summary>
    ''' <value>The integration period.</value>
    Public Property IntegrationPeriod() As ResettableDouble
        Get
            Return Me._integrationPeriod
        End Get
        Set(ByVal value As ResettableDouble)
            Me._integrationPeriod = value
            If value IsNot Nothing Then
                Me._powerLineCycles.Value = Me._integrationPeriod.Value.Value * Me._lineFrequency
            End If
        End Set
    End Property

    Private _powerLineCycles As ResettableDouble
    ''' <summary>
    ''' Gets or sets the power line cycles.
    ''' </summary>
    ''' <value>The power line cycles.</value>
    Friend Property PowerLineCycles() As ResettableDouble
        Get
            Return Me._powerLineCycles
        End Get
        Set(ByVal value As ResettableDouble)
            Me._powerLineCycles = value
            If value IsNot Nothing Then
                Me._integrationPeriod.Value = Me._powerLineCycles.Value.Value / Me._lineFrequency
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the protection level.
    ''' The import of this function depends on the instrument.
    ''' For instance, the protection level for the 2400 voltage source sets 
    ''' a limit on the maximum voltage.  
    ''' </summary>
    Public Property ProtectionLevel() As ResettableDouble

    ''' <summary>
    ''' Gets or sets the range.
    ''' </summary>
    ''' <value>The range.</value>
    Public Property Range() As ResettableDouble

    ''' <summary>
    ''' Gets or sets the source start level.
    ''' </summary>
    ''' <value>The start level.</value>
    Public Property StartLevel() As ResettableDouble

    ''' <summary>
    ''' Gets or sets the source sweep start level.
    ''' </summary>
    ''' <value>The stop level.</value>
    Public Property StopLevel() As ResettableDouble

#End Region

End Class


