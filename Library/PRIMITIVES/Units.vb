''' <summary>
''' Defines physical units and scales.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/05/2008" by="David" revision="5.0.3017.x">
''' Based on SCPI Readings.
''' Created
''' </history>
<Serializable()>
Public Class Units

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        Me.New(Scpi.PhysicalUnit.None, Scpi.UnitsScale.None)
    End Sub

    Public Sub New(ByVal units As Scpi.PhysicalUnit, ByVal scale As Scpi.UnitsScale)
        MyBase.New()
        Me._description = ""
        Me._longUnits = ""
        Me._name = ""
        Me._prefix = ""
        Me.PhysicalUnits = units
        Me.UnitsScale = scale
    End Sub

    ''' <summary>
    ''' Cloning Constructor.
    ''' </summary>
    Public Sub New(ByVal model As Scpi.Units)
        MyBase.New()
        If model IsNot Nothing Then
            Me.PhysicalUnits = model._physicalUnits
            Me.UnitsScale = model._unitsScale
            Me._description = model._description
            Me._longUnits = model._longUnits
            Me._name = model._name
            Me._prefix = model._prefix
            Me._scaleFactor = model._scaleFactor
            Me._scaleValue = model._scaleValue
        End If

    End Sub

#End Region

#Region " UNITS SYMBOLS "

    Public Shared ReadOnly Property Alpha() As Char
        Get
            Return Convert.ToChar(&H3B1)   ' alpha Chr(224)
        End Get
    End Property

    Public Shared ReadOnly Property Beta() As Char
        Get
            Return Convert.ToChar(&H3B2)   ' beta Chr(225)
        End Get
    End Property

    Public Shared ReadOnly Property DegreesC() As Char
        Get
            Return Convert.ToChar(&H2103)   ' °C
        End Get
    End Property

    Public Shared ReadOnly Property DegreesF() As Char
        Get
            Return Convert.ToChar(&H2109)   ' °F
        End Get
    End Property

    ''' <summary>
    ''' Gets the symbol of Ohms
    ''' </summary>
    Public Shared ReadOnly Property Omega() As Char
        Get
            Return Convert.ToChar(&H2126)   ' &H3A9 ChrW(&H2126) ' Omega
        End Get
    End Property

    ''' <summary>
    ''' Gets the symbol of Siemense
    ''' </summary>
    Public Shared ReadOnly Property InvertedOmega() As Char
        Get
            Return Convert.ToChar(&H2127)   ' ChrW(&H2126) ' Omega
        End Get
    End Property

    Public Shared ReadOnly Property GreekMU() As Char
        Get
            Return Convert.ToChar(&HB5)   ' ChrW(&HB5)   ' "µ"
        End Get
    End Property

    Public Shared ReadOnly Property GreekEta() As Char
        Get
            Return Convert.ToChar(&H3B7)   '  ChrW(&H3B7)  ' "n"
        End Get
    End Property

#End Region

#Region " DESCRIPTION "

    Private _description As String
    ''' <summary>
    ''' Gets or sets the description in the form:
    ''' Name (long units, short units)
    ''' </summary>
    Public Property Description() As String
        Get
            Return Me._description
        End Get
        Set(ByVal value As String)
            Me._description = value
            If String.IsNullOrWhiteSpace(Me._description) Then
                Me._name = String.Empty
                Me._longUnits = String.Empty
                Me._shortUnits = String.Empty
            Else
                ' extract the name, long units, and short units.
                Dim values As String() = Me._description.Split("("c)
                If values.Length > 0 Then
                    Me._name = values(0).Trim
                    If values.Length > 1 Then
                        values = values(1).Split(","c)
                        If values.Length > 0 Then
                            Me._longUnits = values(0).Trim
                            If values.Length > 1 Then
                                Me._shortUnits = values(1).Replace(")", "").Trim
                            End If
                        End If
                    End If
                End If
            End If
        End Set
    End Property

    Private _longUnits As String
    Public Property LongUnits() As String
        Get
            Return Me._longUnits
        End Get
        Set(ByVal value As String)
            Me._longUnits = value
        End Set
    End Property

    Private _name As String
    Public Property Name() As String
        Get
            Return Me._name
        End Get
        Set(ByVal value As String)
            Me._name = value
        End Set
    End Property

    Private _prefix As String
    Public Property Prefix() As String
        Get
            Return Me._prefix
        End Get
        Set(ByVal value As String)
            Me._prefix = value
        End Set
    End Property

    Private _physicalUnits As Scpi.PhysicalUnit
    Public Property PhysicalUnits() As Scpi.PhysicalUnit
        Get
            Return Me._physicalUnits
        End Get
        Set(ByVal value As Scpi.PhysicalUnit)
            Me._physicalUnits = value
            If value = Scpi.PhysicalUnit.None Then
                Me.Description = String.Empty
            Else
                Me.Description = Syntax.GetDescription(value)
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns the units with prefix.
    ''' </summary>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "{0}{1}", Me._prefix, Me._shortUnits)
    End Function

#End Region

#Region " SCALING "

    Private _scaleFactor As Double
    ''' <summary>
    ''' Gets or sets the actual scale factor.  This is the magnitude by which to multiply a raw 
    ''' value to express it in the scaled units.  For instance, to format a value as Kilo, 
    ''' multiply the value by 0.001. 
    ''' </summary>
    Public Property ScaleFactor() As Double
        Get
            Return Me._scaleFactor
        End Get
        Set(ByVal value As Double)
            Me._scaleFactor = value
            Me._scaleValue = 1 / value
        End Set
    End Property

    Private _scaleValue As Double
    ''' <summary>Gets or sets the actual scale value.  This is one over the 
    ''' <see cref="ScaleFactor">scale factor</see>.  For instance the Kilo scale value
    ''' is 1000 whereas the scale factor is 0.001.
    ''' </summary>
    Public ReadOnly Property ScaleValue() As Double
        Get
            Return Me._scaleValue
        End Get
    End Property

    Private _shortUnits As String
    Public Property ShortUnits() As String
        Get
            Return Me._shortUnits
        End Get
        Set(ByVal value As String)
            Me._shortUnits = value
        End Set
    End Property

    Private _unitsScale As Scpi.UnitsScale
    Public Property UnitsScale() As Scpi.UnitsScale
        Get
            Return Me._unitsScale
        End Get
        Set(ByVal value As Scpi.UnitsScale)
            Me._unitsScale = value
            Select Case value
                Case Scpi.UnitsScale.Atto
                    Me._scaleFactor = 1.0E+18
                    Me._scaleValue = 1.0E-18
                    Me._prefix = "a"
                Case Scpi.UnitsScale.Centi
                    Me._scaleFactor = 100.0
                    Me._scaleValue = 0.01
                    Me._prefix = "c"
                Case Scpi.UnitsScale.Deci
                    Me._scaleFactor = 10.0
                    Me._scaleValue = 0.1
                    Me._prefix = "d"
                Case Scpi.UnitsScale.Deka
                    Me._scaleFactor = 0.1
                    Me._scaleValue = 10.0
                    Me._prefix = "da"
                Case Scpi.UnitsScale.Femto
                    Me._scaleFactor = 1.0E+15
                    Me._scaleValue = 0.000000000000001
                    Me._prefix = "f"
                Case Scpi.UnitsScale.Giga
                    Me._scaleFactor = 0.000000001
                    Me._scaleValue = 1000000000.0
                    Me._prefix = "G"
                Case Scpi.UnitsScale.Hecto
                    Me._scaleFactor = 0.01
                    Me._scaleValue = 100.0
                    Me._prefix = "h"
                Case Scpi.UnitsScale.Kilo
                    Me._scaleFactor = 0.001
                    Me._scaleValue = 1000.0
                    Me._prefix = "k"
                Case Scpi.UnitsScale.Mega
                    Me._scaleFactor = 0.000001
                    Me._scaleValue = 1000000.0
                    Me._prefix = "M"
                Case Scpi.UnitsScale.Micro
                    Me._scaleFactor = 1000000.0
                    Me._scaleValue = 0.000001
                    Me._prefix = Convert.ToChar(&H3BC)  '  ChrW(&HB5)   ' "µ"
                Case Scpi.UnitsScale.Milli
                    Me._scaleFactor = 1000.0
                    Me._scaleValue = 0.001
                    Me._prefix = "m"
                Case Scpi.UnitsScale.Nano
                    Me._scaleFactor = 1000000000.0
                    Me._scaleValue = 0.000000001
                    Me._prefix = "n" ' ChrW(&H3B7)  ' "n"
                Case Scpi.UnitsScale.None
                    Me._scaleFactor = 1.0
                    Me._scaleValue = 1.0
                    Me._prefix = ""
                Case Scpi.UnitsScale.Pico
                    Me._scaleFactor = 1000000000000.0
                    Me._scaleValue = 0.000000000001
                    Me._prefix = "p"
                Case Scpi.UnitsScale.Tera
                    Me._scaleFactor = 0.000000000001
                    Me._scaleValue = 1000000000000.0
                    Me._prefix = "T"
                Case Scpi.UnitsScale.Peta
                    Me._scaleFactor = 0.000000000000001
                    Me._scaleValue = 1.0E+15
                    Me._prefix = "P"
                Case Scpi.UnitsScale.Exa
                    Me._scaleFactor = 1.0E-18
                    Me._scaleValue = 1.0E+18
                    Me._prefix = "E"
                Case Scpi.UnitsScale.Kibi
                    Me._scaleValue = Math.Pow(2, 10)
                    Me._scaleFactor = 1 / Me._scaleValue
                    Me._prefix = "Ki"
                Case Scpi.UnitsScale.Mebi
                    Me._scaleValue = Math.Pow(2, 20)
                    Me._scaleFactor = 1 / Me._scaleValue
                    Me._prefix = "Mi"
                Case Scpi.UnitsScale.Gibi
                    Me._scaleValue = Math.Pow(2, 30)
                    Me._scaleFactor = 1 / Me._scaleValue
                    Me._prefix = "Gi"
                Case Scpi.UnitsScale.Tebi
                    Me._scaleValue = Math.Pow(2, 40)
                    Me._scaleFactor = 1 / Me._scaleValue
                    Me._prefix = "Ti"
                Case Scpi.UnitsScale.Pebi
                    Me._scaleValue = Math.Pow(2, 50)
                    Me._scaleFactor = 1 / Me._scaleValue
                    Me._prefix = "Pi"
                Case Scpi.UnitsScale.Exbi
                    Me._scaleValue = Math.Pow(2, 60)
                    Me._scaleFactor = 1 / Me._scaleValue
                    Me._prefix = "Ei"
            End Select

        End Set
    End Property
#End Region

End Class

''' <summary>
''' Defines physical units and scales with formatting.
''' </summary>
''' <license>
''' (c) 2008 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="04/05/2008" by="David" revision="5.0.3017.x">
''' Created
''' </history>
<Serializable()> 
Public Class FormattedUnits
    Inherits Units

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        Me.New(Scpi.PhysicalUnit.None, Scpi.UnitsScale.None)
    End Sub

    Public Sub New(ByVal units As Scpi.PhysicalUnit, ByVal scale As Scpi.UnitsScale)
        MyBase.New()
        Me.PhysicalUnits = units
        Me.UnitsScale = scale
        Me.ValueFormat = ""
    End Sub

    ''' <summary>
    ''' Cloning Constructor.
    ''' </summary>
     Public Sub New(ByVal model As Scpi.FormattedUnits)
        MyBase.New(model)
        If model IsNot Nothing Then
            Me._valueFormat = model._valueFormat
        End If
    End Sub

#End Region

#Region " FORMAT "

    ''' <summary>
    ''' Returns a format string for using with the <see cref="String.Format"></see>
    ''' for display of the scaled value with units.
    ''' </summary>
     Public Shared Function BuildUnitsStringFormat(ByVal format As String) As String
        Return "{0:" & format & "} {1}"
    End Function

    ''' <summary>
    ''' Returns a format string for using with the <see cref="String.Format"></see>
    ''' for display of the scaled value with units.
    ''' </summary>
     Public Shared Function BuildFullStringFormat(ByVal format As String) As String
        Return "{0:" & format & "} {1}{2}"
    End Function

    ''' <summary>
    ''' Returns a format string for using with the <see cref="String.Format"></see>
    ''' for display of the scaled value without units.
    ''' </summary>
     Public Shared Function BuildValueStringFormat(ByVal format As String) As String
        Return "{0:" & format & "}"
    End Function

    ''' <summary>
    ''' Returns a caption without units.
    ''' </summary>
     Public Shared Function ToCaption(ByVal format As String, ByVal value As Double?) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return ToCaption(format, value.Value)
        Else
            Return ""
        End If
    End Function

    ''' <summary>
    ''' Returns a caption without units.
    ''' </summary>
     Public Shared Function ToCaption(ByVal format As String, ByVal value As Double) As String
        Return value.ToString(format, Globalization.CultureInfo.CurrentCulture)
        ' Return String.Format(Globalization.CultureInfo.CurrentCulture, BuildFullStringFormat(format), value)
    End Function

    ''' <summary>
    ''' Returns a caption including value and units.
    ''' </summary>
     Public Shared Function ToCaption(ByVal format As String, ByVal value As IFormattable) As String
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Return value.ToString(format, Globalization.CultureInfo.CurrentCulture)
    End Function

    ''' <summary>
    ''' Returns a caption including value and units.
    ''' </summary>
     Public Shared Function ToCaption(ByVal format As String, ByVal value As Double?, ByVal displayUnits As Units) As String
        If value IsNot Nothing AndAlso value.HasValue Then
            Return ToCaption(format, value.Value, displayUnits)
        Else
            Return ""
        End If
    End Function

    ''' <summary>
    ''' Returns a caption including value and units.
    ''' </summary>
     Public Shared Function ToCaption(ByVal format As String, ByVal value As Double, ByVal displayUnits As Units) As String
        If displayUnits Is Nothing Then
            Return value.ToString(format, Globalization.CultureInfo.CurrentCulture)
        Else
            Return value.ToString(format, Globalization.CultureInfo.CurrentCulture) & " " & displayUnits.ToString
        End If
    End Function

    ''' <summary>
    ''' Returns a caption including value and units.
    ''' </summary>
     Public Shared Function ToCaption(ByVal format As String, ByVal value As IFormattable, ByVal displayUnits As Units) As String
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        If displayUnits Is Nothing Then
            Return value.ToString(format, Globalization.CultureInfo.CurrentCulture)
        Else
            Return value.ToString(format, Globalization.CultureInfo.CurrentCulture) & " " & displayUnits.ToString
        End If
    End Function

    Private _valueFormat As String
    ''' <summary>
    ''' Gets or sets the format for returning a value.
    ''' </summary>
    Public Property ValueFormat() As String
        Get
            Return Me._valueFormat
        End Get
        Set(ByVal value As String)
            Me._valueFormat = value
        End Set
    End Property

#End Region

End Class

''' <summary>
'''  Enumerates the physical units.
''' </summary>
Public Enum PhysicalUnit
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Acceleration (Meter per Square Second, m/s²)")> Acceleration
    <ComponentModel.Description("Amount Of Substance (Mole, mol)")> AmountOfSubstance
    <ComponentModel.Description("Area (Square Meter, m²)")> Area
    <ComponentModel.Description("Capacitance (Farad, F)")> Capacitance
    <ComponentModel.Description("Celsius Temperature (DegC, °C)")> CelsiusTemperature
    <ComponentModel.Description("Charge (Coulomb, Cb)")> Charge
    <ComponentModel.Description("Conductance (Siemense, S)")> Conductance
    <ComponentModel.Description("Current (Ampere, A)")> Current
    <ComponentModel.Description("Fahrenheit Temperature (DegF, °F)")> FahrenheitTemperature
    <ComponentModel.Description("Force (Kilogram Force, kgf)")> Force
    <ComponentModel.Description("Frequency (Hertz, Hz)")> Frequency
    <ComponentModel.Description("Inductance (Henry, H)")> Inductance
    <ComponentModel.Description("Length (Meter, m)")> Length
    <ComponentModel.Description("Luminous Intensity (Candela, cd)")> LuminousIntensity
    <ComponentModel.Description("Mass (Kilogram, kg")> Mass
    <ComponentModel.Description("Magnetic Flux (Weber, Wb")> MagneticFlux
    <ComponentModel.Description("Resistance (Ohm, Ohm")> Resistance
    <ComponentModel.Description("Temperature (Kelvin, K)")> Temperature
    <ComponentModel.Description("Time (Second, s)")> [Time]
    <ComponentModel.Description("Velocity (Meter per Second, m/s)")> Velocity
    <ComponentModel.Description("Voltage (Volt, V)")> Voltage
    <ComponentModel.Description("Volume (Cubic Meter, m·m²)")> Volume
End Enum

''' <summary>
''' Enumerates the international units scales.
''' </summary>
Public Enum UnitsScale
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Atto")> Atto
    <ComponentModel.Description("Femto")> Femto
    <ComponentModel.Description("Pico")> Pico
    <ComponentModel.Description("Nano")> Nano
    <ComponentModel.Description("Micro")> Micro
    <ComponentModel.Description("Milli")> Milli
    <ComponentModel.Description("Centi")> Centi
    <ComponentModel.Description("Deci")> Deci
    <ComponentModel.Description("Deka")> Deka
    <ComponentModel.Description("Hecto")> Hecto
    <ComponentModel.Description("Kilo")> Kilo
    <ComponentModel.Description("Mega")> Mega
    <ComponentModel.Description("Giga")> Giga
    <ComponentModel.Description("Tera")> Tera
    <ComponentModel.Description("Peta")> Peta
    <ComponentModel.Description("Exa")> Exa
    <ComponentModel.Description("Kibi")> Kibi
    <ComponentModel.Description("Mebi")> Mebi
    <ComponentModel.Description("Gibi")> Gibi
    <ComponentModel.Description("Tebi")> Tebi
    <ComponentModel.Description("Pebi")> Pebi
    <ComponentModel.Description("Exbi")> Exbi
End Enum

