''' <summary>
''' Represents an object whose underlying types are value types, but can also be assigned 
''' a new value like a reference type.
''' </summary>
''' <typeparam name="T"></typeparam>
''' <remarks>
''' This is useful with SCPI values that require reseting to a known state.
''' </remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' Created
''' </history>
<Serializable()> 
Public Class ResettableValue(Of T As {Structure})
    Implements IResettable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.new()
    End Sub

    Public Sub New(ByVal value As T)
        Me.New()
        Me._value = New Nullable(Of T)(value)
    End Sub

    Public Sub New(ByVal value As T, ByVal resettingValue As T)
        Me.New()
        Me._value = New Nullable(Of T)(value)
        Me._resetValue = New Nullable(Of T)(resettingValue)
    End Sub

    Public Sub New(ByVal value As T, ByVal resettingValue As T, ByVal presettingValue As T)
        Me.New()
        Me._value = value
        Me._resetValue = resettingValue
        Me._presetValue = presettingValue
    End Sub

    Public Sub New(ByVal value As T, ByVal resettingValue As T, ByVal presettingValue As T, ByVal clearingValue As T)
        Me.New()
        Me._value = value
        Me._resetValue = resettingValue
        Me._presetValue = presettingValue
        Me._clearValue = clearingValue
    End Sub

#End Region

#Region " IRESETTABLE "

    ''' <summary>
    ''' Sets the value to the clear execution state.
    ''' </summary>
    Function ClearExecutionState() As Boolean Implements IResettable.ClearExecutionState
        If Me._clearValue.HasValue Then
            Me._value = Me._clearValue.Value
        End If
        Return True
    End Function

    ''' <summary>
    ''' Sets the value to the preset known state value.
    ''' </summary>
    Public Function PresetKnownState() As Boolean Implements IResettable.PresetKnownState
        If Me._presetValue.HasValue Then
            Me._value = Me._presetValue.Value
        End If
        Return True
    End Function

    ''' <summary>
    ''' Sets the value to the known state value.
    ''' </summary>
    Public Function ResetKnownState() As Boolean Implements IResettable.ResetKnownState
        If Me._resetValue.HasValue Then
            Me._value = Me._resetValue.Value
        End If
        Return True
    End Function

#End Region

#Region " EQUALS "

    ''' <summary>Indicates whether the current <see cref="T:ResettableValue"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns>true if the other parameter is equal to the current <see cref="T:ResettableValue"></see> value; 
    ''' otherwise, false. 
    ''' </returns>
    ''' <param name="obj">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        If Not Me.Value.HasValue Then
            Return (obj Is Nothing)
        End If
        If (obj Is Nothing) Then
            Return False
        End If
        Return Me.Value.Equals(obj)
    End Function

    ''' <summary>Retrieves the hash code of the object returned by the 
    ''' <see cref="P:ResettableValue.Value"></see> property.</summary>
    ''' <returns>The hash code of the object returned by the 
    ''' <see cref="P:ResettableValue.Value"></see> property if the 
    ''' <see cref="P:ResettableValue.Value.HasValue"></see> property is true, 
    ''' or zero if the <see cref="P:ResettableValue.Value.HasValue"></see> property is false. </returns>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function GetHashCode() As Integer
        Dim hashCode As Integer
        If Me._value.HasValue Then
            hashCode = Me._value.GetHashCode
        End If
        If Me._clearValue.HasValue Then
            hashCode = hashCode Xor Me._clearValue.GetHashCode
        End If
        If Me._presetValue.HasValue Then
            hashCode = hashCode Xor Me._presetValue.GetHashCode
        End If
        If Me._resetValue.HasValue Then
            hashCode = hashCode Xor Me._resetValue.GetHashCode
        End If
        Return hashCode
    End Function

#End Region

#Region " FORMAT "

    ''' <summary>
    ''' Gets the caption for displaying this information.
    ''' </summary>
    Public ReadOnly Property DisplayCaption() As String
        Get
            Return Me.ToString()
        End Get
    End Property

    ''' <summary>Returns the text representation of the value of the current <see cref="T:ResettableValue"></see> 
    ''' object.</summary>
    ''' <returns>The text representation of the value of the current <see cref="T:ResettableValue"></see> 
    ''' object if the <see cref="P:ResettableValue.Value.HasValue"></see> property is true, or an empty string ("") if the 
    ''' <see cref="P:ResettableValue.Value.HasValue"></see> property is false.</returns>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function ToString() As String
        Return Me._value.ToString
    End Function

#End Region

#Region " VALUES "

    Friend _clearValue As Nullable(Of T)
    ''' <summary>
    ''' Gets or sets the value to use when
    ''' <see cref="ClearExecutionState">clearing execution state</see>
    ''' </summary>
    ''' <value>A <see cref="System.Nullable(of T)">nullable value</see></value>
    Public Property ClearValue() As Nullable(Of T)
        Get
            Return Me._clearValue
        End Get
        Set(ByVal value As Nullable(Of T))
            Me._clearValue = value
        End Set
    End Property

    Friend _presetValue As Nullable(Of T)
    ''' <summary>
    ''' Gets or sets the value to use when
    ''' <see cref="PresetKnownState">presetting to a known state</see>
    ''' </summary>
    ''' <value>A <see cref="System.Nullable(of T)">nullable value</see></value>
    Public Property PresetValue() As Nullable(Of T)
        Get
            Return Me._presetValue
        End Get
        Set(ByVal value As Nullable(Of T))
            Me._presetValue = value
        End Set
    End Property

    Friend _resetValue As Nullable(Of T)
    ''' <summary>
    ''' Gets or sets the value to use when
    ''' <see cref="ResetKnownState">resetting to a known state</see>
    ''' </summary>
    ''' <value>A <see cref="System.Nullable(of T)">nullable value</see></value>
    Public Property ResetValue() As Nullable(Of T)
        Get
            Return Me._resetValue
        End Get
        Set(ByVal value As Nullable(Of T))
            Me._resetValue = value
        End Set
    End Property

    Friend _value As Nullable(Of T)
    ''' <summary>
    ''' Gets or sets the value.
    ''' </summary>
    ''' <value>
    ''' The value.
    ''' </value>
    Public Property [Value]() As Nullable(Of T)
        Get
            Return Me._value
        End Get
        Set(ByVal value As Nullable(Of T))
            Me._value = value
        End Set
    End Property

    Friend _actualValue As Nullable(Of T)
    ''' <summary>
    ''' Gets or sets the actual value.
    ''' </summary>
    ''' <value>
    ''' The actual value.
    ''' </value>
    Public Property ActualValue() As Nullable(Of T)
        Get
            Return Me._actualValue
        End Get
        Set(ByVal value As Nullable(Of T))
            Me._actualValue = value
        End Set
    End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is verified.
    ''' </summary>
    ''' <value>
    ''' 
    ''' <c>True</c> if this instance is verified; otherwise, <c>False</c>.
    ''' 
    ''' </value>
    Public Overridable ReadOnly Property IsVerified() As Boolean?
        Get
            If Value.HasValue AndAlso ActualValue.HasValue Then
                Return Value.Value.Equals(ActualValue.Value)
            Else
                Return New Boolean?
            End If
        End Get
    End Property

#End Region

#Region " OPERATORS "

    ''' <summary>Creates a new <see cref="T:ResettableValue"></see> object initialized to a specified value.</summary>
    ''' <returns>A <see cref="T:ResettableValue"></see> object whose <see cref="P:ResettableValue.Value"></see> 
    ''' property is initialized with the value parameter.</returns>
    ''' <param name="value">A value type.</param>
    Public Shared Widening Operator CType(ByVal value As Nullable(Of T)) As ResettableValue(Of T)
        If value IsNot Nothing AndAlso value.HasValue Then
            Return New ResettableValue(Of T)(value.Value)
        Else
            Return New ResettableValue(Of T)()
        End If
    End Operator

    ''' <summary>
    ''' Returns the value of a specified <see cref="T:ResettableValue"></see> value.
    ''' </summary>
    ''' <param name="value">A <see cref="T:ResettableValue"></see> value.</param>
    ''' <returns>The value of the <see cref="P:ResettableValue.Value"></see> property for the value parameter.</returns>
    Public Shared Narrowing Operator CType(ByVal value As ResettableValue(Of T)) As Nullable(Of T)
        If value Is Nothing Then
            Return New Nullable(Of T)
        End If
        Return value.Value
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ResettableValue"></see> value
    ''' equals the <paramref name="left">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableValue.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:Boolean"></see> value.</param>
    ''' <param name="right">A <see cref="T:ResettableValue"></see> value.</param>
    Public Shared Operator =(ByVal left As T, ByVal right As ResettableValue(Of T)) As Boolean
        If right IsNot Nothing AndAlso right.Value IsNot Nothing AndAlso right.Value.HasValue Then
            Return right.Value.Equals(left)
        Else
            Return False
        End If
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ResettableValue"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableValue.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ResettableValue"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Shared Operator =(ByVal left As ResettableValue(Of T), ByVal right As T) As Boolean
        If left Is Nothing Then
            Return False
        End If
        If left.Value.HasValue Then
            Return left.Value.Equals(right)
        Else
            Return False
        End If
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ResettableValue"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableValue.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ResettableValue"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Shared Operator =(ByVal left As ResettableValue(Of T), ByVal right As ResettableValue(Of T)) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        End If
        Return left.Equals(right)
    End Operator

    Public Shared Operator <>(ByVal left As T, ByVal right As ResettableValue(Of T)) As Boolean
        Return Not (left = right)
    End Operator

    Public Shared Operator <>(ByVal left As ResettableValue(Of T), ByVal right As T) As Boolean
        Return Not (left = right)
    End Operator

    Public Shared Operator <>(ByVal left As ResettableValue(Of T), ByVal right As ResettableValue(Of T)) As Boolean
        Return Not (left = right)
    End Operator

#End Region

End Class

''' <summary>
''' Represents an object whose underlying types are <see cref="Double"></see> value types, 
''' but can also be assigned a new value like a reference type.
''' </summary>
''' <remarks>
''' This is useful with SCPI value that require reseting to a known state.
''' </remarks>
<Serializable()> 
Public Class ResettableDouble
    Inherits ResettableValue(Of Double)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.new()
        Me._units = New Scpi.FormattedUnits
        Me._displayUnits = New Scpi.FormattedUnits
        Me._tolerance = Single.Epsilon
    End Sub

    Public Sub New(ByVal value As Double)
        Me.New()
        Me._value = New Nullable(Of Double)(value)
    End Sub

    Public Sub New(ByVal value As Double, ByVal resettingValue As Double)
        Me.New()
        Me._value = New Nullable(Of Double)(value)
        Me._resetValue = New Nullable(Of Double)(resettingValue)
    End Sub

    Public Sub New(ByVal value As Double, ByVal resettingValue As Double, ByVal presettingValue As Double)
        Me.New()
        Me._value = value
        Me._resetValue = resettingValue
        Me._presetValue = presettingValue
    End Sub

    Public Sub New(ByVal value As Double, ByVal resettingValue As Double, ByVal presettingValue As Double, ByVal clearingValue As Double)
        Me.New()
        Me._value = value
        Me._resetValue = resettingValue
        Me._presetValue = presettingValue
        Me._clearValue = clearingValue
    End Sub

#End Region

#Region " DISPLAY "

    ''' <summary>
    ''' Gets the caption for displaying the value.
    ''' </summary>
    Public Overloads ReadOnly Property Caption() As String
        Get
            If Me._value.HasValue Then
                Return Scpi.FormattedUnits.ToCaption(Me._units.ValueFormat, Me._value.Value * Me._units.ScaleFactor)
            Else
                Return ""
            End If
        End Get
    End Property

    ''' <summary>
    ''' Gets the caption for displaying the scaled value.
    ''' </summary>
    Public Overloads ReadOnly Property DisplayCaption() As String
        Get
            If Me._value.HasValue Then
                Return Scpi.FormattedUnits.ToCaption(Me._displayUnits.ValueFormat, Me._value.Value * Me._displayUnits.ScaleFactor)
            Else
                Return ""
            End If
        End Get
    End Property

#End Region

#Region " EDITED VALUE "

    Private _editedValue As Double
    ''' <summary>
    ''' Gets or sets the edited value to be used for setting a new value.
    ''' </summary>
    Public Property EditedValue() As Double
        Get
            Return Me._editedValue
        End Get
        Set(ByVal value As Double)
            Me._editedValue = value
        End Set
    End Property

    ''' <summary>
    ''' Parses the scaled value to create a <see cref="EditedValue">new unscaled value</see>.
    ''' </summary>
    ''' <param name="value">The scaled value.</param>
    ''' <returns>Returns the new unscaled value</returns>
    ''' <exception cref="System.InvalidCastException">Failed parsing the scaled value.</exception>
    Public Function ParseScaledValue(ByVal value As String) As Double
        If Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, 
                          Globalization.CultureInfo.CurrentCulture, Me._editedValue) Then
            Me._editedValue *= Me._displayUnits.ScaleValue
        Else
            Throw New InvalidCastException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                         "Failed parsing the scaled value '{0}'.", value))
        End If
        Return Me._editedValue
    End Function

    ''' <summary>
    ''' Parses the value to create a <see cref="EditedValue">new unscaled value</see>.
    ''' </summary>
    ''' <param name="value">The scaled value.</param>
    ''' <returns>Returns the new unscaled value</returns>
    ''' <exception cref="System.InvalidCastException">Failed pasring the value.</exception>
    Public Function ParseValue(ByVal value As String) As Double
        If Not Double.TryParse(value, Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent, 
                          Globalization.CultureInfo.CurrentCulture, Me._editedValue) Then
            Throw New InvalidCastException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                         "Failed parsing the value '{0}'.", value))
        End If
        Return Me._editedValue
    End Function

#End Region

#Region " VERIFIY "

    ''' <summary>
    ''' Gets a value indicating whether this instance is verified.
    ''' </summary>
    ''' <value>
    ''' 
    ''' <c>True</c> if this instance is verified; otherwise, <c>False</c>.
    ''' 
    ''' </value>
    Public Overrides ReadOnly Property IsVerified() As Boolean?
        Get
            If ActualValue.HasValue Then
                Return Me.Approximates(ActualValue.Value)
            Else
                Return New Boolean?
            End If
        End Get
    End Property

#End Region

#Region " UNITS AND SCALES "

    Private _units As Scpi.FormattedUnits
    ''' <summary>
    ''' Gets or sets the units for this value.
    ''' These are the units of the unscaled value. 
    ''' </summary>
    Public Property Units() As Scpi.FormattedUnits
        Get
            Return Me._units
        End Get
        Set(ByVal value As Scpi.FormattedUnits)
            Me._units = value
        End Set
    End Property

    Private _displayUnits As Scpi.FormattedUnits
    ''' <summary>
    ''' Gets or sets the Display Units.
    ''' </summary>
    Public Property DisplayUnits() As Scpi.FormattedUnits
        Get
            If Me._displayUnits Is Nothing Then
                Return Me._units
            Else
                Return Me._displayUnits
            End If
        End Get
        Set(ByVal value As Scpi.FormattedUnits)
            Me._displayUnits = value
        End Set
    End Property

#End Region

#Region " EQUALS "

    ''' <summary>Indicates whether the current <see cref="T:ResettableValue"></see> value is equal to 
    ''' a specified object.</summary>
    ''' <returns>true if the other parameter is equal to the current <see cref="T:ResettableValue"></see> value; 
    ''' otherwise, false. 
    ''' </returns>
    ''' <param name="obj">An object.</param>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        If Me.Value IsNot Nothing AndAlso Not Me.Value.HasValue Then
            Return (obj Is Nothing)
        End If
        If (obj Is Nothing) Then
            Return False
        End If
        Return Me.Value.Equals(obj)
    End Function

    ''' <summary>
    ''' Equalses the specified value.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <returns><c>True</c> if equals, <c>False</c> otherwise</returns>
    ''' <exception cref="System.ArgumentNullException">value</exception>
    Public Overloads Function Equals(ByVal value As ResettableValue(Of Double)) As Boolean
        If value Is Nothing Then
            Return False
        End If
        If value.Value IsNot Nothing AndAlso value.Value.HasValue Then
            Return Me.Equals(value.Value.Value)
        Else
            Return False
        End If
    End Function

    Public Overloads Function Equals(ByVal value As Double) As Boolean
        If Me.Value IsNot Nothing AndAlso Me.Value.HasValue Then
            Return ResettableDouble.AreEqual(Me._value.Value, value, Single.Epsilon)
        Else
            Return False
        End If
    End Function

    Public Overloads Function Equals(ByVal value As Single) As Boolean
        Return Me.Equals(CDbl(value))
    End Function

    Public Overloads Function Equals(ByVal value As Integer) As Boolean
        Return Me.Equals(CDbl(value))
    End Function

    Public Overloads Function Equals(ByVal value As Short) As Boolean
        Return Me.Equals(CDbl(value))
    End Function

    Public Overloads Function Equals(ByVal value As Long) As Boolean
        Return Me.Equals(CDbl(value))
    End Function

    ''' <summary>Retrieves the hash code of the object returned by the 
    ''' <see cref="P:ResettableValue.Value"></see> property.</summary>
    ''' <returns>The hash code of the object returned by the 
    ''' <see cref="P:ResettableValue.Value"></see> property if the 
    ''' <see cref="P:ResettableValue.Value.HasValue"></see> property is true, 
    ''' or zero if the <see cref="P:ResettableValue.Value.HasValue"></see> property is false. </returns>
    ''' <FilterPriority>1</FilterPriority>
    Public Overrides Function GetHashCode() As Integer
        Return MyBase.GetHashCode
    End Function

#End Region

#Region " OPERATORS "

    ''' <summary>Creates a new <see cref="T:ResettableDouble"></see> object initialized to a specified value.</summary>
    ''' <returns>A <see cref="T:ResettableDouble"></see> object whose <see cref="P:ResettableDouble.Value"></see> 
    ''' property is initialized with the value parameter.</returns>
    ''' <param name="value">A value type.</param>
    Public Overloads Shared Widening Operator CType(ByVal value As Nullable(Of Double)) As ResettableDouble
        Return FromNullableDouble(value)
    End Operator

    ''' <summary>Creates a new <see cref="T:ResettableDouble"></see> object initialized to a specified value.</summary>
    ''' <returns>A <see cref="T:ResettableDouble"></see> object whose <see cref="P:ResettableDouble.Value"></see> 
    ''' property is initialized with the value parameter.</returns>
    ''' <param name="value">A value type.</param>
    Public Shared Function FromNullableDouble(ByVal value As Nullable(Of Double)) As ResettableDouble
        If value IsNot Nothing AndAlso value.HasValue Then
            Return New ResettableDouble(value.Value)
        Else
            Return New ResettableDouble()
        End If
    End Function

    ''' <summary>
    ''' Returns the value of a specified <see cref="T:ResettableDouble"></see> value.
    ''' </summary>
    ''' <param name="value">A <see cref="T:ResettableDouble"></see> value.</param>
    ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
    Public Overloads Shared Narrowing Operator CType(ByVal value As ResettableDouble) As Nullable(Of Double)
        If value Is Nothing Then
            Return New Nullable(Of Double)
        End If
        Return value.Value
    End Operator

    ''' <summary>
    ''' Returns the value of a specified <see cref="T:ResettableDouble"></see> value.
    ''' </summary>
    ''' <param name="value">A <see cref="T:ResettableDouble"></see> value.</param>
    ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
    Public Shared Function ToNullableDouble(ByVal value As ResettableValue(Of Double)) As Nullable(Of Double)
        If value Is Nothing Then
            Return New Nullable(Of Double)
        End If
        Return value.Value
    End Function

    ''' <summary>Returns True if the specified <see cref="T:ResettableDouble"></see> value
    ''' equals the <paramref name="left">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:Boolean"></see> value.</param>
    ''' <param name="right">A <see cref="T:ResettableDouble"></see> value.</param>
    Public Overloads Shared Operator =(ByVal left As Double, ByVal right As ResettableDouble) As Boolean
        If right Is Nothing Then
            Return False
        End If
        Return right.Equals(left)
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ResettableDouble"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ResettableDouble"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Overloads Shared Operator =(ByVal left As ResettableDouble, ByVal right As Double) As Boolean
        If left Is Nothing Then
            Return False
        End If
        Return left.Equals(right)
    End Operator

    ''' <summary>Returns True if the specified <see cref="T:ResettableDouble"></see> value
    ''' equals the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ResettableDouble"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Overloads Shared Operator =(ByVal left As ResettableDouble, ByVal right As ResettableDouble) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        End If
        Return left.Equals(right)
    End Operator

    Public Overloads Shared Operator <>(ByVal left As Double, ByVal right As ResettableDouble) As Boolean
        Return Not (left = right)
    End Operator

    Public Overloads Shared Operator <>(ByVal left As ResettableDouble, ByVal right As Double) As Boolean
        Return Not (left = right)
    End Operator

    Public Overloads Shared Operator <>(ByVal left As ResettableDouble, ByVal right As ResettableDouble) As Boolean
        Return Not (left = right)
    End Operator

    ''' <summary>
    ''' Return 0 if the two values are equal.
    ''' </summary>
    ''' <param name="left">The left.</param>
    ''' <param name="right">The right.</param>
    ''' <returns>System.Int32.</returns>
    ''' <exception cref="System.ArgumentNullException">left</exception>
    Public Overloads Shared Function Compare(ByVal left As ResettableValue(Of Double), ByVal right As Double) As Integer
        If left Is Nothing Then
            Throw New ArgumentNullException("left")
        End If
        If left.Value IsNot Nothing AndAlso left.Value.HasValue Then
            Return left.Value.Value.CompareTo(right)
        Else
            Return 0
        End If
    End Function

    ''' <summary>Returns True if the specified <see cref="T:ResettableDouble"></see> value
    ''' is less than the <paramref name="right">value</paramref>.</summary>
    ''' <returns>The value of the <see cref="P:ResettableDouble.Value"></see> property for the value parameter.</returns>
    ''' <param name="left">A <see cref="T:ResettableDouble"></see> value.</param>
    ''' <param name="right">A <see cref="T:Boolean"></see> value.</param>
    Public Overloads Shared Operator <(ByVal left As ResettableDouble, ByVal right As Double) As Boolean
        Return ResettableDouble.Compare(left, right) < 0
    End Operator

    Public Overloads Shared Operator >(ByVal left As ResettableDouble, ByVal right As Double) As Boolean
        Return ResettableDouble.Compare(left, right) > 0
    End Operator

#End Region

#Region " RANGE "

    Friend _maximum As Nullable(Of Double)
    ''' <summary>
    ''' Gets or sets the maximum allowed value.
    ''' </summary>
    Public Property [Maximum]() As Nullable(Of Double)
        Get
            Return Me._maximum
        End Get
        Set(ByVal value As Nullable(Of Double))
            Me._maximum = value
        End Set
    End Property

    Friend _minimum As Nullable(Of Double)
    ''' <summary>
    ''' Gets or sets the minimum allowed value.
    ''' </summary>
    Public Property [Minimum]() As Nullable(Of Double)
        Get
            Return Me._minimum
        End Get
        Set(ByVal value As Nullable(Of Double))
            Me._minimum = value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the value is in range.
    ''' </summary>
    Public Function IsInRange(ByVal value As Double) As Boolean
        If Me._minimum.HasValue Then
            If Me._maximum.HasValue Then
                Return value <= Me._maximum.Value AndAlso value >= Me._minimum.Value
            Else
                Return value >= Me._minimum.Value
            End If
        ElseIf Me._maximum.HasValue Then
            Return value <= Me._maximum.Value
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' Returns true if the value is in range.
    ''' </summary>
    Public Function IsInRange(ByVal value As Double?) As Boolean?
        If value IsNot Nothing AndAlso value.HasValue Then
            Return Me.IsInRange(value.Value)
        Else
            Return New Boolean?
        End If
    End Function

    ''' <summary>
    ''' Returns true if the value is in range.
    ''' </summary>
    Public Function IsInRange() As Boolean?
        Return IsInRange(Me.Value)
    End Function

    ''' <summary>
    ''' Returns a value within the entity range.
    ''' </summary>
    Public Function BoundValue(ByVal value As Double) As Double
        If Me._minimum.HasValue Then
            If Me._maximum.HasValue Then
                If value >= Me._minimum Then
                    If value <= Me._maximum Then
                        Return value
                    Else
                        Return Me._maximum.Value
                    End If
                Else
                    Return Me._minimum.Value
                End If
            Else
                If value >= Me._minimum Then
                    Return value
                Else
                    Return Me._minimum.Value
                End If
            End If
        ElseIf Me._maximum.HasValue Then
            If value <= Me._maximum Then
                Return value
            Else
                Return Me._maximum.Value
            End If
        Else
            Return value
        End If
    End Function

    ''' <summary>
    ''' Returns a value within the entity range.
    ''' </summary>
    Public Function BoundValue(ByVal value As Double?) As Double?
        If value IsNot Nothing AndAlso value.HasValue Then
            Return Me.BoundValue(value.Value)
        Else
            Return value
        End If
    End Function

    ''' <summary>
    ''' Returns a value within the entity range.
    ''' </summary>
    Public Function BoundValue() As Double?
        Return BoundValue(Me.Value)
    End Function

#End Region

#Region " TOLERANCE "

    Private _tolerance As Double
    ''' <summary>
    ''' Gets or sets the tolerance level for comparisons.
    ''' </summary>
    Public Property Tolerance() As Double
        Get
            Return Me._tolerance
        End Get
        Set(ByVal value As Double)
            Me._tolerance = value
        End Set
    End Property

    ''' <summary>
    ''' Returns true if this value approximates the given value
    ''' with the set <see cref="Tolerance">tolerance</see>
    ''' </summary>
    Public Function Approximates(ByVal value As Double) As Boolean?
        If Me.Value IsNot Nothing AndAlso Me.Value.HasValue Then
            Return Math.Abs(value - Me.Value.Value) < Me.Tolerance
        Else
            Return New Boolean?
        End If
    End Function

    ''' <summary>
    ''' Returns true if this value approximates the given value
    ''' with the set <see cref="Tolerance">tolerance</see>
    ''' </summary>
    Public Function Approximates(ByVal value As Single) As Boolean?
        If Me.Value IsNot Nothing AndAlso Me.Value.HasValue Then
            Return Math.Abs(value - Me.Value.Value) < Me.Tolerance
        Else
            Return New Boolean?
        End If
    End Function

#End Region

#Region " COMPARE "

    ''' <summary>Returns true if the absolute difference is lower than <paramref name="epsilon"></paramref></summary>
    Public Shared Function AreEqual(ByVal value1 As Double, ByVal value2 As Double, ByVal epsilon As Single) As Boolean
        Return Math.Abs(value1 - value2) <= epsilon
    End Function

    ''' <summary>Returns true if the absolute difference exceeds <paramref name="epsilon"></paramref>.</summary>
    Public Shared Function AreDifferent(ByVal value1 As Double, ByVal value2 As Double, ByVal epsilon As Double) As Boolean
        Return Math.Abs(value1 - value2) > epsilon
    End Function

    ''' <summary>Returns true if the absolute difference exceeds <paramref name="epsilon"></paramref>.</summary>
    Public Shared Function AreDifferent(ByVal value1 As Double, ByVal value2 As Nullable(Of Double), ByVal epsilon As Double) As Boolean
        Return Not value2.HasValue OrElse Math.Abs(value1 - value2.Value) > epsilon
    End Function

    ''' <summary>Returns true if the absolute difference exceeds <paramref name="epsilon"></paramref>.</summary>
    Public Shared Function AreDifferent(ByVal value1 As Nullable(Of Double), ByVal value2 As Nullable(Of Double), ByVal epsilon As Double) As Boolean
        Return Not value1.HasValue OrElse Not value2.HasValue OrElse (Math.Abs(value1.Value - value2.Value) > epsilon)
    End Function

    ''' <summary>Returns true if the absolute difference exceeds <paramref name="epsilon"></paramref>.</summary>
    Public Shared Function AreDifferent(ByVal value1 As Nullable(Of Double), ByVal value2 As Double, ByVal epsilon As Double) As Boolean
        Return Not value1.HasValue OrElse (Math.Abs(value1.Value - value2) > epsilon)
    End Function

#End Region

End Class

