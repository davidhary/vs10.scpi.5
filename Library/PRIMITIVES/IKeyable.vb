''' <summary>
''' Defines an interface allowing objects to be included in a Dictionary collection of String and Value
''' using a directly settable key.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' Created
''' </history>
Public Interface IKeyable

    ''' <summary>Gets the unique Key for an instance of this class.</summary>
    Property UniqueKey() As String

End Interface

''' <summary>
''' A <see cref="Collections.ObjectModel.KeyedCollection">collection</see> of
''' <see cref="IKeyable">Keyable</see> 
''' items keyed by the <see cref="IKeyable.UniqueKey">unique key.</see>
''' </summary>
Public Class KeyableCollection(Of TItem As IKeyable)
    Inherits Collections.ObjectModel.KeyedCollection(Of String, TItem)

    Protected Overrides Function GetKeyForItem(ByVal item As TItem) As String
        Return item.UniqueKey
    End Function

End Class

