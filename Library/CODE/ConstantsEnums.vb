#Region " TYPES "

''' <summary>
''' Determines how to access the device
''' </summary>
<Flags()>
Public Enum ResourceAccessLevels
    <System.ComponentModel.Description("Access Device only if New Value")> None = 0
    <System.ComponentModel.Description("Access Device")> Device = 1
    <System.ComponentModel.Description("Access Device and Read Back and Verify")> Verify = 2
    <System.ComponentModel.Description("Access Cached Value")> Cache = 4
End Enum

''' <summary>
''' Enumerates the format for writing boolean types.
''' </summary>
Public Enum BooleanDataFormat
    <ComponentModel.Description("Not Defined")> None
    <ComponentModel.Description("One (true) or Zero")> OneZero
    <ComponentModel.Description("On (true) or Off")> OnOff
    <ComponentModel.Description("True or False")> TrueFalse
    <ComponentModel.Description("In (true) or Out")> InOut
    <ComponentModel.Description("Acceptor (bypass) or Source")> AcceptorSource
    <ComponentModel.Description("Immediate (true) or End Binning Control")> ImmediateEnd
    <ComponentModel.Description("Grading (true) or Sorting Binning Mode")> GradingSorting
    <ComponentModel.Description("Front (true) or Rear Route Terminals")> FrontRear
    <ComponentModel.Description("Next (true) or Never Buffer Feed Control")> NextNever
    <ComponentModel.Description("Cable (true) or Ohms Guard Type")> CableOhms
End Enum

#End Region

#Region " SCPI TYPES "

''' <summary>
''' Enumerates the arm layer control sources.
''' </summary>
Public Enum ArmSource
    <ComponentModel.Description("Not Defined ()")> None
    <ComponentModel.Description("Bus (BUS)")> Bus
    <ComponentModel.Description("External (EXT)")> External
    <ComponentModel.Description("Hold operation (HOLD)")> Hold
    <ComponentModel.Description("Immediate (IMM)")> Immediate
    <ComponentModel.Description("Manual (MAN)")> Manual
    <ComponentModel.Description("Timer (TIM)")> Timer
    ''' <summary>
    ''' Event detection for the arm layer is satisfied when either a positive-going or 
    ''' a negative-going pulse (via the SOT line of the Digital I/O) is received.
    ''' </summary>
    <ComponentModel.Description("Start Test Pulsed High or Low (BSTES)")> StartTestBoth
    ''' <summary>
    ''' Event detection for the arm layer is satisfied when a positive-going pulse 
    ''' (via the SOT line of the Digital I/O) is received
    ''' </summary>
    <ComponentModel.Description("Start Test Pulsed High (PSTES)")> StartTestHigh
    ''' <summary>
    ''' Event detection for the arm layer is satisfied when a negative-going pulse 
    ''' (via the SOT line of the Digital I/O) is received.
    ''' </summary>
    <ComponentModel.Description("Start Test Pulsed High (NSTES)")> StartTestLow
    <ComponentModel.Description("Trigger Link (TLIN)")> TriggerLink
End Enum

''' <summary>
''' Enumerates the source of readings.
''' </summary>
Public Enum FeedSource
    <ComponentModel.Description("Not Defined ()")> None
    <ComponentModel.Description("Sense (SENS)")> Sense
    <ComponentModel.Description("Calculate 1 (CALC)")> Calculate1
    <ComponentModel.Description("Calculate 2 (CALC2)")> Calculate2
    <ComponentModel.Description("Current (CURR)")> Current
    <ComponentModel.Description("Voltage (VOLT)")> Voltage
    <ComponentModel.Description("Resistance (RES)")> Resistance
End Enum

''' <summary>Specifies the output off mode.</summary>
Public Enum OutputOffMode
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Guard (GUAR)")> Guard
    <ComponentModel.Description("High Impedance (HIMP)")> HighImpedance
    <ComponentModel.Description("Normal (NORM)")> Normal
    <ComponentModel.Description("Zero (ZERO)")> Zero
End Enum

''' <summary>
''' Enumerates reading elements the instrument is capable of.
''' </summary>
<System.Flags()>
Public Enum ReadingElements
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Reading (READ)")> Reading = 1
    <ComponentModel.Description("Time Stamp (TST)")> Timestamp = 2 * Reading
    <ComponentModel.Description("Units (UNIT)")> Units = 2 * Timestamp
    <ComponentModel.Description("Reading Number (RNUM)")> ReadingNumber = 2 * Units
    <ComponentModel.Description("Source (SOUR)")> Source = 2 * ReadingNumber
    <ComponentModel.Description("Compliance (COMP)")> Compliance = 2 * Source
    <ComponentModel.Description("Average Voltage (AVOL)")> AverageVoltage = 2 * Compliance
    <ComponentModel.Description("Voltage (VOLT)")> Voltage = 2 * AverageVoltage
    <ComponentModel.Description("Current (CURR)")> Current = 2 * Voltage
    <ComponentModel.Description("Resistance (RES)")> Resistance = 2 * Current
    <ComponentModel.Description("Time (TIME)")> Time = 2 * Resistance
    <ComponentModel.Description("Status (STAT)")> Status = 2 * Time
    <ComponentModel.Description("Channel (CHAN)")> Channel = 2 * Status
    <ComponentModel.Description("Limits (LIM)")> Limits = 2 * Channel
End Enum

''' <summary>Specifies the sense function modes.</summary>
<System.Flags()>
Public Enum SenseFunctionModes
    <ComponentModel.Description("None")> None = 0
    <ComponentModel.Description("Voltage ('VOLT')")> Voltage = 1
    <ComponentModel.Description("Current ('CURR')")> Current = 2 * SenseFunctionModes.Voltage
    <ComponentModel.Description("DC Voltage ('VOLT:DC')")> VoltageDC = 2 * SenseFunctionModes.Current
    <ComponentModel.Description("DC Current ('CURR:DC')")> CurrentDC = 2 * SenseFunctionModes.VoltageDC
    <ComponentModel.Description("AC Voltage ('VOLT:AC')")> VoltageAC = 2 * SenseFunctionModes.CurrentDC
    <ComponentModel.Description("AC Current ('CURR:AC')")> CurrentAC = 2 * SenseFunctionModes.VoltageAC
    <ComponentModel.Description("Resistance ('RES')")> Resistance = 2 * SenseFunctionModes.CurrentAC
    <ComponentModel.Description("Four-Wire Resistance ('FRES')")> FourWireResistance = 2 * SenseFunctionModes.Resistance
    <ComponentModel.Description("Temperature ('TEMP')")> Temperature = 2 * SenseFunctionModes.FourWireResistance
    <ComponentModel.Description("Frequency ('FREQ')")> Frequency = 2 * SenseFunctionModes.Temperature
    <ComponentModel.Description("Period ('PER')")> Period = 2 * SenseFunctionModes.Frequency
    <ComponentModel.Description("Continuity ('CONT')")> Continuity = 2 * SenseFunctionModes.Period
    <System.ComponentModel.Description("Timestamp element (TIME)")> TimestampElement = 2 * SenseFunctionModes.Continuity
    <System.ComponentModel.Description("Status Element (STAT)")> StatusElement = 2 * SenseFunctionModes.TimestampElement
    <System.ComponentModel.Description("Memory (MEM)")> Memory = 2 * SenseFunctionModes.StatusElement
End Enum

''' <summary>Specifies the source function modes.</summary>
Public Enum SourceFunctionMode
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Voltage (VOLT)")> Voltage
    <ComponentModel.Description("Current (CURR)")> Current
    <ComponentModel.Description("Memory (MEM)")> Memory
    <ComponentModel.Description("DC Voltage (VOLT:DC)")> VoltageDC
    <ComponentModel.Description("DC Current (CURR:DC)")> CurrentDC
    <ComponentModel.Description("AC Voltage (VOLT:AC)")> VoltageAC
    <ComponentModel.Description("AC Current (CURR:AC)")> CurrentAC
End Enum

''' <summary>Specifies the source sweep modes.</summary>
Public Enum SweepMode
    <ComponentModel.Description("None")> None
    <ComponentModel.Description("Fixed (FIX)")> Fixed
    <ComponentModel.Description("Sweep (SWE)")> Sweep
    <ComponentModel.Description("List (LIST)")> List
End Enum

''' <summary>
''' Enumerates the arm ot trigger events.
''' </summary>
Public Enum TriggerEvent
    <ComponentModel.Description("None (NONE)")> None
    <ComponentModel.Description("Source (SOUR)")> Source
    <ComponentModel.Description("Delay (DEL)")> Delay
    <ComponentModel.Description("Sense (SENS)")> Sense
End Enum

''' <summary>
''' Enumerates the trigger layer control sources.
''' </summary>
Public Enum TriggerSource
    <ComponentModel.Description("Not Defined ()")> None
    <ComponentModel.Description("Bus (BUS)")> Bus
    <ComponentModel.Description("External (Ext)")> External
    <ComponentModel.Description("Immediate (IMM)")> Immediate
    <ComponentModel.Description("Trigger Link (TLIN)")> TriggerLink
End Enum

#End Region

#Region " IEEE 488.2 EVENT FLAGS "

''' <summary>Gets or sets the status byte bits of the service request register.</summary>
''' <remarks>
''' Enumerates the Status Byte Register Bits.
''' Use STB? or status.request_event to read this register.
''' Use *SRE or status.request_enable to enable these services.
''' This attribute is used to read the status byte, which is returned as a
''' numeric value. The binary equivalent of the returned value indicates which
''' register bits are set.
''' </remarks>
<System.Flags()>
Public Enum ServiceRequests
    <ComponentModel.Description("None")> None = 0
    ''' <summary>
    ''' Bit B0, Measurement Summary Bit (MSB). Set summary bit indicates
    ''' that an enabled measurement event has occurred.
    ''' </summary>
    <ComponentModel.Description("Measurement Event (MSB)")> MeasurementEvent = &H1
    ''' <summary>
    ''' Bit B1, System Summary Bit (SSB). Set summary bit indicates
    ''' that an enabled system event has occurred.
    ''' </summary>
    <ComponentModel.Description("System Event (SSB)")> SystemEvent = &H2
    ''' <summary>
    ''' Bit B2, Error Available (EAV). Set summary bit indicates that
    ''' an error or status message is present in the Error Queue.
    ''' </summary>
    <ComponentModel.Description("Error Available (EAV)")> ErrorAvailable = &H4
    ''' <summary>
    ''' Bit B3, Questionable Summary Bit (QSB). Set summary bit indicates
    ''' that an enabled questionable event has occurred.
    ''' </summary>
    <ComponentModel.Description("Questionable Event (QSB)")> QuestionableEvent = &H8
    ''' <summary>
    ''' Bit B4 (16), Message Available (MAV). Set summary bit indicates that
    ''' a response message is present in the Output Queue.
    ''' </summary>
    <ComponentModel.Description("Message Available (MAV)")> MessageAvailable = &H10
    ''' <summary>Bit B5, Event Summary Bit (ESB). Set summary bit indicates 
    ''' that an enabled standard event has occurred.
    ''' </summary>
    <ComponentModel.Description("Standard Event (ESB)")> StandardEvent = &H20 ' (32) ESB
    ''' <summary>
    ''' Bit B6 (64), Request Service (RQS)/Master Summary Status (MSS).
    ''' Set bit indicates that an enabled summary bit of the Status Byte Register
    ''' is set. Depending on how it is used, Bit B6 of the Status Byte Register
    ''' is either the Request for Service (RQS) bit or the Master Summary Status
    ''' (MSS) bit: When using the GPIB serial poll sequence of the unit to obtain
    ''' the status byte (serial poll byte), B6 is the RQS bit. When using
    ''' status.condition or the *STB? common command to read the status byte,
    ''' B6 is the MSS bit.
    ''' </summary>
    <ComponentModel.Description("Request Service (RQS)/Master Summary Status (MSS)")> RequestingService = &H40
    ''' <summary>
    ''' Bit B7 (128), Operation Summary (OSB). Set summary bit indicates that
    ''' an enabled operation event has occurred.
    ''' </summary>
    <ComponentModel.Description("Operation Event (OSB)")> OperationEvent = &H80
    ''' <summary>
    ''' Includes all bits.
    ''' </summary>
    <ComponentModel.Description("All")> All = &HFF ' 255
    ''' <summary>
    ''' Unknown value due to, for example, error trying to get value from the
    ''' instrument.
    ''' </summary>
    <ComponentModel.Description("Unknown")> Unknown = &H100
End Enum

''' <summary>
''' Enumerates the status byte flags of the standard event register.
''' </summary>
''' <remarks>
''' Enumerates the Standard Event Status Register Bits.
''' Read this information using ESR? or status.standard.event.
''' Use *ESE or status.standard.enable or event status enable 
''' to enable this register.
''' These values are used when reading or writing to the
''' standard event registers. Reading a status register returns a value.
''' The binary equivalent of the returned value indicates which register bits
''' are set. The least significant bit of the binary number is bit 0, and
''' the most significant bit is bit 15. For example, assume value 9 is
''' returned for the enable register. The binary equivalent is
''' 0000000000001001. This value indicates that bit 0 (OPC) and bit 3 (DDE)
''' are set.
''' </remarks>
<System.Flags()>
Public Enum StandardEvents
    <ComponentModel.Description("None")> None = 0
    ''' <summary>
    ''' Bit B0, Operation Complete (OPC). Set bit indicates that all
    ''' pending selected device operations are completed and the unit is ready to
    ''' accept new commands. The bit is set in response to an *OPC command.
    ''' The ICL function opc() can be used in place of the *OPC command.
    ''' </summary>
    <ComponentModel.Description("Operation Complete (OPC)")> OperationComplete = 1
    ''' <summary>
    ''' Bit B1, Request Control (RQC). Set bit indicates that....
    ''' </summary>
    <ComponentModel.Description("Request Control (RQC)")> RequestControl = &H2
    ''' <summary>
    ''' Bit B2, Query Error (QYE). Set bit indicates that you attempted
    ''' to read data from an empty Output Queue.
    ''' </summary>
    <ComponentModel.Description("Query Error (QYE)")> QueryError = &H4
    ''' <summary>
    ''' Bit B3, Device-Dependent Error (DDE). Set bit indicates that an
    ''' instrument operation did not execute properly due to some internal
    ''' condition.
    ''' </summary>
    <ComponentModel.Description("Device Dependent Error (DDE)")> DeviceDependentError = &H8
    ''' <summary>
    ''' Bit B4 (16), Execution Error (EXE). Set bit indicates that the unit
    ''' detected an error while trying to execute a command. 
    ''' This is used by Quatech to report No Contact.
    ''' </summary>
    <ComponentModel.Description("Execution Error (EXE)")> ExecutionError = &H10
    ''' <summary>
    ''' Bit B5 (32), Command Error (CME). Set bit indicates that a
    ''' command error has occurred. Command errors include:<p>
    ''' IEEE-488.2 syntax error � unit received a message that does not follow
    ''' the defined syntax of the IEEE-488.2 standard.  </p><p>
    ''' Semantic error � unit received a command that was misspelled or received
    ''' an optional IEEE-488.2 command that is not implemented.  </p><p>
    ''' The instrument received a Group Execute Trigger (GET) inside a program
    ''' message.  </p>
    ''' </summary>
    <ComponentModel.Description("Command Error (CME)")> CommandError = &H20
    ''' <summary>
    ''' Bit B6 (64), User Request (URQ). Set bit indicates that the LOCAL
    ''' key on the SourceMeter front panel was pressed.
    ''' </summary>
    <ComponentModel.Description("User Request (URQ)")> UserRequest = &H40
    ''' <summary>
    ''' Bit B7 (128), Power ON (PON). Set bit indicates that the instrument
    ''' has been turned off and turned back on since the last time this register
    ''' has been read.
    ''' </summary>
    <ComponentModel.Description("Power Toggled (PON)")> PowerToggled = &H80
    ''' <summary>
    ''' Unknown value due to, for example, error trying to get value from the
    ''' instrument.
    ''' </summary>
    <ComponentModel.Description("Unknown")> Unknown = &H100
    ''' <summary>Includes all bits.
    ''' </summary>
    <ComponentModel.Description("All")> All = &HFF ' 255
End Enum

#End Region
