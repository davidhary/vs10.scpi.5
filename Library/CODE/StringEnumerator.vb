Imports System
Imports System.Collections.Generic
Imports System.Reflection
Imports System.ComponentModel

''' <summary>
''' Utility class can be used to treat <c>string</c> arrays as <c>enum</c>'s.  Use this class
''' to parse a string to an enum or serialize an enum to a string.  Unlike direct use of an
''' enum, this class allows the enum values to be decorated with <c>Description</c> attributes
''' which can be used when serializing the enum.
''' Modified to user with SCPI enumerations.
''' TO_DO: Extend SCPI enumerations to include the SCPI command stream where necessary in a 
''' way that can be used for parsing the enumerated value from the returned value.  For example,
''' parse the sense function mode from the returned value such as "CURRENT:DC".
''' </summary>
''' <typeparam name="T"></typeparam>
''' <history date="02/03/2007" by="David" revision="1.0.2590.x">
''' Converted to VB. 
''' By Rudy RIHANI, Code Net. http://www.CodeProject.com/CSharp/StringEmulator.asp
''' </history>
Public Class StringEnumerator(Of T)

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="StringEnumerator"/> class.
    ''' Permits calling 'shared' methods as members to meet FX COP rule 'DoNotDeclareStaticMembersOnGenericTypes'.
    ''' </summary>
    Public Sub New()
        MyBase.New()
    End Sub

#End Region

    ''' <summary>
    ''' Parses the specified value string to enum type.  If the string can not be parsed then
    ''' and exception is thrown.  This method will first attempt to parse the string by matching it
    ''' directly to the string representation of the enum value.  If a match is not found, then it will
    ''' attempt to match the enum against each DescriptionAttribute applied to that enum (if any).
    ''' </summary>
    ''' <param name="value">The enum string.</param>
    ''' <returns>An enum value of type T</returns>
    ''' <exception cref="InvalidCastException"></exception>
    Public Function Parse(ByVal value As String) As T
        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If String.Compare(fi.Name, value, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                Return CType(fi.GetValue(Nothing), T)
            Else
                Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
                For Each attr As DescriptionAttribute In fieldAttributes
                    If String.Compare(attr.Description, value, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                        Return CType(fi.GetValue(Nothing), T)
                    End If
                Next attr
            End If
        Next fi
        Throw New InvalidCastException(String.Format(Globalization.CultureInfo.CurrentCulture, 
            "Can't convert {0} to {1}", value, enumType.ToString()))
    End Function

    ''' <summary>
    ''' Parses the specified value string to enum type.  If the string can not be parsed then
    ''' and exception is thrown.  This method will first attempt to parse the string by matching it
    ''' directly to the string representation of the enum value.  If a match is not found, then it will
    ''' attempt to match the enum against each DescriptionAttribute applied to that enum (if any).
    ''' </summary>
    ''' <param name="value">The enum string.</param>
    ''' <returns>An enum value of type T</returns>
    ''' <exception cref="InvalidCastException"></exception>
    Public Function ParseContained(ByVal value As String) As T
        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If String.Compare(fi.Name, value, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                Return CType(fi.GetValue(Nothing), T)
            Else
                Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
                For Each attr As DescriptionAttribute In fieldAttributes
                    If attr.Description.Contains(value) Then
                        Return CType(fi.GetValue(Nothing), T)
                    End If
                Next attr
            End If
        Next fi
        Throw New InvalidCastException(String.Format(Globalization.CultureInfo.CurrentCulture, 
            "Can't convert {0} to {1}", value, enumType.ToString()))
    End Function

    ''' <summary>
    ''' Parses the specified value string to enum type.  If the string can not be parsed then
    ''' <c>False</c> is returned. This method will first attempt to parse the string by matching it
    ''' directly to the string representation of the enum value.  If a match is not found, then it will
    ''' attempt to match the enum against each DescriptionAttribute applied to that enum (if any).
    ''' </summary>
    ''' <param name="value">The value to parse.</param>
    ''' <param name="enumerator">The enumerator type</param>
    ''' <returns>
    ''' 	<c>True</c> if the string value was successfully parsed.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#", Justification:="This is the normative use of Try Parse.")> 
    Public Function TryParse(ByVal value As String, ByRef enumerator As T) As Boolean
        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If String.Compare(fi.Name, value, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                enumerator = CType(fi.GetValue(Nothing), T)
                Return True
            Else
                Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
                For Each attr As DescriptionAttribute In fieldAttributes
                    If String.Compare(attr.Description, value, True, Globalization.CultureInfo.CurrentCulture) = 0 Then
                        enumerator = CType(fi.GetValue(Nothing), T)
                        Return True
                    End If
                Next attr
            End If
        Next fi
        Return False
    End Function

    ''' <summary>
    ''' Uses the enumeration DescriptionAttribute to generate the string translation of the enum value.  If
    ''' no such attribute has been applied then the method will simply call T.ToString() on the enum.
    ''' </summary>
    ''' <param name="enumValue">The enum value.</param>
    Public Shadows Function ToString(ByVal enumValue As T) As String
        Dim enumType As Type = GetType(T)
        Dim fi As FieldInfo = enumType.GetField(enumValue.ToString())

        'Get the Description attribute that has been applied to this enum
        Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
        If fieldAttributes.Length > 0 Then
            Dim descAttr As DescriptionAttribute = TryCast(fieldAttributes(0), DescriptionAttribute)
            If Not descAttr Is Nothing Then
                Return descAttr.Description
            End If

        End If
        ' Enum does not have Description attribute so we return default string representation.
        Return enumValue.ToString()
    End Function

    ''' <summary>
    ''' Returns an array of strings that represent the values of the enumerator.  If any of the 
    ''' enum values have a <see cref="System.ComponentModel.DescriptionAttribute">Description Attribute</see> 
    ''' applied to them, then the value of the description
    ''' is used in the List element.
    ''' </summary>
    Public Function ToArray() As String()
        Return toList().ToArray()
    End Function

    ''' <summary>
    ''' Returns a collection of strings that represent the values of the enumerator.  If any of the 
    ''' enum values have <see cref="System.ComponentModel.DescriptionAttribute">Description Attribute</see> 
    ''' applied to them, then the value of the description
    ''' is used in the List element.
    ''' </summary>
    Public Function ToReadOnlyCollection() As ObjectModel.ReadOnlyCollection(Of String)
        Return New ObjectModel.ReadOnlyCollection(Of String)(toList())
    End Function

    ''' <summary>
    ''' Returns a list of strings that represent the values of the enumerator.  If any of the 
    ''' enum values have <see cref="System.ComponentModel.DescriptionAttribute">Description Attribute</see> 
    ''' applied to them, then the value of the description
    ''' is used in the List element.
    ''' </summary>
    Private Shared Function toList() As List(Of String)
        Dim enumValues As List(Of String) = New List(Of String)()
        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If fi.IsSpecialName = False Then
                'Get the Description attribute that has been applied to this enum
                Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
                If fieldAttributes.Length > 0 Then
                    Dim descAttr As DescriptionAttribute = TryCast(fieldAttributes(0), DescriptionAttribute)
                    If Not descAttr Is Nothing Then
                        enumValues.Add(descAttr.Description)
                    End If
                Else
                    'Enum does not have Description attribute so we return default string representation.
                    Dim enumValue As T = CType(fi.GetValue(Nothing), T)
                    enumValues.Add(enumValue.ToString())
                End If
            End If
        Next fi

        Return enumValues

    End Function

    ''' <summary>
    ''' Returns a dictionary that maps enums to their descriptions.
    ''' </summary>
    Public Function ToDictionary() As Dictionary(Of T, String)
        Dim enumDictionary As Dictionary(Of T, String) = New Dictionary(Of T, String)()

        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If fi.IsSpecialName = False Then
                Dim enumValue As T = CType(fi.GetValue(Nothing), T)

                'Get the Description attribute that has been applied to this enum
                Dim fieldAttributes As Object() = fi.GetCustomAttributes(GetType(DescriptionAttribute), False)
                If fieldAttributes.Length > 0 Then
                    Dim descAttr As DescriptionAttribute = TryCast(fieldAttributes(0), DescriptionAttribute)
                    If Not descAttr Is Nothing Then
                        enumDictionary.Add(enumValue, descAttr.Description)
                    End If
                Else
                    'Enum does not have Description attribute so we return default string representation.
                    enumDictionary.Add(enumValue, enumValue.ToString())
                End If
            End If
        Next fi

        Return enumDictionary
    End Function

    ''' <summary>
    ''' Returns a list that contains all of the enum types.  This list can be enumerated
    ''' against to get each enum value.
    ''' </summary>
    Public Function GetKeys() As T()
        Dim keys As List(Of T) = New List(Of T)()
        Dim enumType As Type = GetType(T)
        For Each fi As FieldInfo In enumType.GetFields()
            If fi.IsSpecialName = False Then
                Dim enumValue As T = CType(fi.GetValue(Nothing), T)
                keys.Add(enumValue)
            End If
        Next fi
        Return keys.ToArray()
    End Function

End Class
