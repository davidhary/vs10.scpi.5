﻿''' <summary>Handles scPI function not defined exceptions.</summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/08/05" by="David" revision="1.00.1834.x">
''' Created
''' </history>
<Serializable()>
Public Class ScpiFunctionNotDefinedException
    Inherits FunctionNotDefinedException

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ScpiFunctionNotDefinedException"/> class.
    ''' </summary>
    Public Sub New()
        Me.New("Scpi function not defined")
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BaseException" /> class.
    ''' </summary>
    ''' <param name="message">Specifies the exception message.</param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BaseException" /> class.
    ''' </summary>
    ''' <param name="message">Specifies the exception message.</param>
    ''' <param name="innerException">Sets a reference to the InnerException.</param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    Private Const _functionModeMessage As String = "Failed selecting SCPI function {0}."
    ''' <summary>
    ''' Initializes a new instance of the <see cref="FunctionNotDefinedException"/> class.
    ''' </summary>
    ''' <param name="functionMode">Specifies the function.</param>
    Public Sub New(ByVal functionMode As SourceFunctionMode)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, _functionModeMessage, functionMode))
    End Sub

    ''' <param name="functionMode">Specifies the exception message.</param>
    Public Sub New(ByVal functionMode As SenseFunctionModes)
        MyBase.New(String.Format(Globalization.CultureInfo.CurrentCulture, _functionModeMessage, functionMode))
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized data.
    ''' </summary>
    ''' <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that
    ''' holds the serialized object data about the exception being thrown.</param>
    ''' <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that
    ''' contains contextual information about the source or destination.</param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class
